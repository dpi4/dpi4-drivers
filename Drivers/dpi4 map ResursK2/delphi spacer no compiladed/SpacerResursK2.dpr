library SpacerResursK2;


uses
  System.Classes,
  Winapi.Windows, System.SysUtils,

  SignalK2fn in 'SignalK2fn.pas',
  SignalK2tp in 'SignalK2tp.pas';

// ����� �������� �� ���������� �������

const
  SIGNALK2DLL = 'SignalK2.dll';

// �������� ������ ����������
procedure GetDllVersion(out Major, Minor: Word);
  stdcall; external SIGNALK2DLL;

// �������� ������������ ����������� ������������� ���������� �������� �������� �����������
function CheckSignalData(SignalData: PUserSignalData): Word;
  stdcall; external SIGNALK2DLL;

// �������� ����������� ������ � ���-������
function CheckPort(PortNumber: Word): Word;
  stdcall; external SIGNALK2DLL;

// �������� ��������� �������� ������
function GetErrorText(ErrorCode: Word; Text: PChar): Boolean;
  stdcall; external SIGNALK2DLL;

// ���������� ������ ������� ������������
function StopSignal(PortNumber: Word): Word;
  stdcall; external SIGNALK2DLL;

// ���������� ������ ������������ ��������/��������������
function StopCollapsesOvervoltages(PortNumber: Word): Word;
  stdcall; external SIGNALK2DLL;

// ��������� ������������ �������� ��������
function SetSignal(PortNumber: Word; SignalData: PUserSignalData;
  OutSignalData: POutSignalData; ServiceInfo: PServiceInfo): Word;
  stdcall; external SIGNALK2DLL;

// ������ ������ ���������� �������� �������� �����������
function GetOutSignalData(SignalData: PUserSignalData; OutSignalData: POutSignalData): Word;
  stdcall; external SIGNALK2DLL;

type

 TArr = array[0..512] of Double;  // ����������� ������
 TPArr = ^TArr;

var

 SignalData: TUserSignalData;
 Deviations: TCollapsesOvervoltages;
 Oscillations: TVoltageOscillations;
 ServiceInfo: TServiceInfo;
 ComPortNumber1: Word;
 OutSignalData: TOutSignalData;
 SignalRes: Word;


/////////////////////////////////////////
function F2(): Boolean ;
begin
    //������� ���������� ��������� �������
     //��������� ������� �������
    SignalData.Signal.Frequency.Value := 50;
    //��������� �������� ����������
    //������������ �������� ��� ����������� �� ����������� ������������
    //������� �� ������� ����������
    SignalData.Signal.Voltage.NominalID := CONST_NOMINAL_U_220V;
    //����� ������ �� ������� ����������
    SignalData.Signal.Voltage.Enable := True; //True=1
    //��������� ���������� �������� ������� � ������
    SignalData.Signal.Voltage.Faza_A.IncludeMainFreq := True; //True
    //������� �������� ���������� �������� �������
    SignalData.Signal.Voltage.Faza_A.MainFreqVoltCur.Koef := 220;
    //������� ���� ���������� �������� �������
    SignalData.Signal.Voltage.Faza_A.MainFreqVoltCur.Angle := 0;

    SignalData.Signal.Voltage.Faza_B.IncludeMainFreq := True; //True
    SignalData.Signal.Voltage.Faza_B.MainFreqVoltCur.Koef := 230;
    SignalData.Signal.Voltage.Faza_B.MainFreqVoltCur.Angle := -120;

    SignalData.Signal.Voltage.Faza_C.IncludeMainFreq := True ;//True
    SignalData.Signal.Voltage.Faza_C.MainFreqVoltCur.Koef := 210;
    SignalData.Signal.Voltage.Faza_C.MainFreqVoltCur.Angle := 120;
    //������ ������� ������������ � ���� ���������
    SignalData.Signal.Voltage.Faza_A.Harmonics[2].Koef := 1.5;
    SignalData.Signal.Voltage.Faza_A.Harmonics[2].Angle := 10;
    SignalData.Signal.Voltage.Faza_B.Harmonics[2].Koef := 2.5;
    SignalData.Signal.Voltage.Faza_B.Harmonics[2].Angle := 15;
    SignalData.Signal.Voltage.Faza_C.Harmonics[2].Koef := 3.5;
    SignalData.Signal.Voltage.Faza_C.Harmonics[2].Angle := 20;
    SignalData.Signal.Voltage.Faza_A.Harmonics[40].Koef := 3;
    SignalData.Signal.Voltage.Faza_A.Harmonics[40].Angle := 10;
    SignalData.Signal.Voltage.Faza_B.Harmonics[40].Koef := 2.5;
    SignalData.Signal.Voltage.Faza_B.Harmonics[40].Angle := 15;
    SignalData.Signal.Voltage.Faza_C.Harmonics[40].Koef := 2;
    SignalData.Signal.Voltage.Faza_C.Harmonics[40].Angle := 20;
    //��������� �������� ����
    //������������ �������� ��� ����������� �� ����������� ������������
    //������� �� ������� ����
    SignalData.Signal.Current.NominalID := CONST_NOMINAL_I_5A;

    SignalData.Signal.Current.Faza_A.IncludeMainFreq := True; //True
    SignalData.Signal.Current.Faza_A.MainFreqVoltCur.Koef := 5;
    SignalData.Signal.Current.Faza_A.MainFreqVoltCur.Angle := 30;
    SignalData.Signal.Current.Faza_B.IncludeMainFreq := True ;//True
    SignalData.Signal.Current.Faza_B.MainFreqVoltCur.Koef := 4;
    SignalData.Signal.Current.Faza_B.MainFreqVoltCur.Angle := -30;
    SignalData.Signal.Current.Faza_C.IncludeMainFreq := True; //True
    SignalData.Signal.Current.Faza_C.MainFreqVoltCur.Koef := 3 ;
    SignalData.Signal.Current.Faza_C.MainFreqVoltCur.Angle := 0;

    SignalData.Signal.Current.Faza_A.Harmonics[2].Koef := 5.5 ;
    SignalData.Signal.Current.Faza_A.Harmonics[2].Angle := 10  ;
    SignalData.Signal.Current.Faza_B.Harmonics[2].Koef := 4.5 ;
    SignalData.Signal.Current.Faza_B.Harmonics[2].Angle := 15;
    SignalData.Signal.Current.Faza_C.Harmonics[2].Koef := 4 ;
    SignalData.Signal.Current.Faza_C.Harmonics[2].Angle := 20  ;
    SignalData.Signal.Current.Faza_A.Harmonics[40].Koef := 2       ;
    SignalData.Signal.Current.Faza_A.Harmonics[40].Angle := 10    ;
    SignalData.Signal.Current.Faza_B.Harmonics[40].Koef := 2.5   ;
    SignalData.Signal.Current.Faza_B.Harmonics[40].Angle := 15  ;
    SignalData.Signal.Current.Faza_C.Harmonics[40].Koef := 3   ;
    SignalData.Signal.Current.Faza_C.Harmonics[40].Angle := 20;
    //����� ������ �� ������� ����
    SignalData.Signal.Current.Enable := True; //True;
     //:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=
    //������� ���������� ��������/��������������
    //����������� ��� ������������� ������������ ��������/��������������
    //:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=
    //��������� ����� ����������
{
    Deviations.Amount := 5        ;
    Deviations.Duration := 1.5   ;
    Deviations.RepeatPeriod := 2;
    Deviations.InitialFaza := 0;
    //��������� ���� �������� ����������
    Deviations.ReferenceVoltage := CONST_DEVIATIONS_REFERENCE_VOLTAGE_NOMINAL;
    //������� ���� ��������. ������ ��� �������� ����������������
    Deviations.Linking := CONST_DEVIATIONS_LINKING_PERIOD;
    //������� ������� �� ���� A
    Deviations.Faza_A.IsCollapse := True;
    Deviations.Faza_A.Value := 20;
    //������� �������������� �� ���� B
    Deviations.Faza_B.IsCollapse := False;
    Deviations.Faza_B.Value := 1.1;
    //�� ���� C ��� �� ��������, �� ��������������
    Deviations.Faza_C.IsCollapse := True ;
    Deviations.Faza_C.Value := -1;
    //������ ������ ���������� ���� CollapsOvervolt ��������� ������
    //� �������� ������� ��������� ���������� Deviations
    SignalData.CollapsOvervolt := 0; //VarPtr(Deviations)
    //:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=
    //������� ���������� ���������
    //����������� ��� ������������� ������������ ���������
    //:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:=:= //
     //������� ������� ���������
    Oscillations.Frequency := 1.2         ;
    //������� ������� ��������� �� ���� A
    //�� ��������� ����� ��������� �� �����
    Oscillations.Faza_A.Swing := 40      ;
    //������ ������ ���������� ���� VoltageOscillations ��������� ������
    //� �������� ������� ��������� ���������� Oscillations
    SignalData.VoltageOscillations := 0 ;//VarPtr(Oscillations)
 }

    SignalData.CollapsOvervolt.Amount := 5        ;
    SignalData.CollapsOvervolt.Duration := 1.5   ;
    SignalData.CollapsOvervolt.RepeatPeriod := 2;
    SignalData.CollapsOvervolt.InitialFaza := 0;
    SignalData.CollapsOvervolt.ReferenceVoltage := CONST_DEVIATIONS_REFERENCE_VOLTAGE_NOMINAL;
    SignalData.CollapsOvervolt.Linking := CONST_DEVIATIONS_LINKING_PERIOD;
    SignalData.CollapsOvervolt.Faza_A.IsCollapse := True;
    SignalData.CollapsOvervolt.Faza_A.Value := 20;
    SignalData.CollapsOvervolt.Faza_B.IsCollapse := False;
    SignalData.CollapsOvervolt.Faza_B.Value := 1.1;
    SignalData.CollapsOvervolt.Faza_C.IsCollapse := True ;
    SignalData.CollapsOvervolt.Faza_C.Value := -1;

//    SignalData.CollapsOvervolt := 0; //VarPtr(Deviations)
    SignalData.VoltageOscillations.Frequency := 1.2         ;
    SignalData.VoltageOscillations.Faza_A.Swing := 40      ;

//  SignalData.VoltageOscillations := 0 ;//VarPtr(Oscillations)

   result := true;
 end;
 /////////////////���������� ���������
function In_Signal(arr:TPArr;Count: Word): Boolean;
var i,j:integer;
begin
i:=0;j:=2;
    SignalData.Signal.Frequency.Value := arr^[i]; i:=i+1;//50
    SignalData.Signal.Voltage.NominalID := trunc(arr^[i]);i:=i+1;///CONST_NOMINAL_U_220V;
    SignalData.Signal.Voltage.Enable := (arr^[i]>0.0);i:=i+1;//True; //True=1
    SignalData.Signal.Voltage.Faza_A.IncludeMainFreq := (arr^[i]>0.0);i:=i+1; //True
    SignalData.Signal.Voltage.Faza_A.MainFreqVoltCur.Koef := arr^[i];i:=i+1;//220;
    SignalData.Signal.Voltage.Faza_A.MainFreqVoltCur.Angle := arr^[i];i:=i+1;//0;
    SignalData.Signal.Voltage.Faza_B.IncludeMainFreq := (arr^[i]>0.0);i:=i+1; //True
    SignalData.Signal.Voltage.Faza_B.MainFreqVoltCur.Koef := arr^[i];i:=i+1;//230;
    SignalData.Signal.Voltage.Faza_B.MainFreqVoltCur.Angle := arr^[i];i:=i+1;//-120;
    SignalData.Signal.Voltage.Faza_C.IncludeMainFreq := (arr^[i]>0.0);i:=i+1;//True
    SignalData.Signal.Voltage.Faza_C.MainFreqVoltCur.Koef := arr^[i];i:=i+1;//210;
    SignalData.Signal.Voltage.Faza_C.MainFreqVoltCur.Angle := arr^[i];i:=i+1;//120;

    SignalData.Signal.Voltage.Faza_A.Harmonics[j].Koef := arr^[i];i:=i+1;//1.5;
    SignalData.Signal.Voltage.Faza_A.Harmonics[j].Angle := arr^[i];i:=i+1;//10;
    SignalData.Signal.Voltage.Faza_B.Harmonics[j].Koef := arr^[i];i:=i+1;//2.5;
    SignalData.Signal.Voltage.Faza_B.Harmonics[j].Angle := arr^[i];i:=i+1;//15;
    SignalData.Signal.Voltage.Faza_C.Harmonics[j].Koef := arr^[i];i:=i+1;//3.5;
    SignalData.Signal.Voltage.Faza_C.Harmonics[j].Angle := arr^[i];i:=i+1;//20;
//2 ���������
    SignalData.Signal.Voltage.Faza_A.Harmonics[j].Koef := arr^[i];i:=i+1;//1.5;
    SignalData.Signal.Voltage.Faza_A.Harmonics[j].Angle := arr^[i];i:=i+1;//10;
    SignalData.Signal.Voltage.Faza_B.Harmonics[j].Koef := arr^[i];i:=i+1;//2.5;
    SignalData.Signal.Voltage.Faza_B.Harmonics[j].Angle := arr^[i];i:=i+1;//15;
    SignalData.Signal.Voltage.Faza_C.Harmonics[j].Koef := arr^[i];i:=i+1;//3.5;
    SignalData.Signal.Voltage.Faza_C.Harmonics[j].Angle := arr^[i];i:=i+1;//20;
                                   j:=j+1;
//3 ���������
    SignalData.Signal.Voltage.Faza_A.Harmonics[j].Koef := arr^[i];i:=i+1;//1.5;
    SignalData.Signal.Voltage.Faza_A.Harmonics[j].Angle := arr^[i];i:=i+1;//10;
    SignalData.Signal.Voltage.Faza_B.Harmonics[j].Koef := arr^[i];i:=i+1;//2.5;
    SignalData.Signal.Voltage.Faza_B.Harmonics[j].Angle := arr^[i];i:=i+1;//15;
    SignalData.Signal.Voltage.Faza_C.Harmonics[j].Koef := arr^[i];i:=i+1;//3.5;
    SignalData.Signal.Voltage.Faza_C.Harmonics[j].Angle := arr^[i];i:=i+1;//20;
                                   j:=j+1;
//4 ���������
    SignalData.Signal.Voltage.Faza_A.Harmonics[j].Koef := arr^[i];i:=i+1;//1.5;
    SignalData.Signal.Voltage.Faza_A.Harmonics[j].Angle := arr^[i];i:=i+1;//10;
    SignalData.Signal.Voltage.Faza_B.Harmonics[j].Koef := arr^[i];i:=i+1;//2.5;
    SignalData.Signal.Voltage.Faza_B.Harmonics[j].Angle := arr^[i];i:=i+1;//15;
    SignalData.Signal.Voltage.Faza_C.Harmonics[j].Koef := arr^[i];i:=i+1;//3.5;
    SignalData.Signal.Voltage.Faza_C.Harmonics[j].Angle := arr^[i];i:=i+1;//20;
                                   j:=j+1;

//5 ���������
    SignalData.Signal.Voltage.Faza_A.Harmonics[j].Koef := arr^[i];i:=i+1;//1.5;
    SignalData.Signal.Voltage.Faza_A.Harmonics[j].Angle := arr^[i];i:=i+1;//10;
    SignalData.Signal.Voltage.Faza_B.Harmonics[j].Koef := arr^[i];i:=i+1;//2.5;
    SignalData.Signal.Voltage.Faza_B.Harmonics[j].Angle := arr^[i];i:=i+1;//15;
    SignalData.Signal.Voltage.Faza_C.Harmonics[j].Koef := arr^[i];i:=i+1;//3.5;
    SignalData.Signal.Voltage.Faza_C.Harmonics[j].Angle := arr^[i];i:=i+1;//20;
                                   j:=j+1;

//2 ���������
    SignalData.Signal.Voltage.Faza_A.Harmonics[j].Koef := arr^[i];i:=i+1;//1.5;
    SignalData.Signal.Voltage.Faza_A.Harmonics[j].Angle := arr^[i];i:=i+1;//10;
    SignalData.Signal.Voltage.Faza_B.Harmonics[j].Koef := arr^[i];i:=i+1;//2.5;
    SignalData.Signal.Voltage.Faza_B.Harmonics[j].Angle := arr^[i];i:=i+1;//15;
    SignalData.Signal.Voltage.Faza_C.Harmonics[j].Koef := arr^[i];i:=i+1;//3.5;
    SignalData.Signal.Voltage.Faza_C.Harmonics[j].Angle := arr^[i];i:=i+1;//20;
                                   j:=j+1;

//6 ���������
    SignalData.Signal.Voltage.Faza_A.Harmonics[j].Koef := arr^[i];i:=i+1;//1.5;
    SignalData.Signal.Voltage.Faza_A.Harmonics[j].Angle := arr^[i];i:=i+1;//10;
    SignalData.Signal.Voltage.Faza_B.Harmonics[j].Koef := arr^[i];i:=i+1;//2.5;
    SignalData.Signal.Voltage.Faza_B.Harmonics[j].Angle := arr^[i];i:=i+1;//15;
    SignalData.Signal.Voltage.Faza_C.Harmonics[j].Koef := arr^[i];i:=i+1;//3.5;
    SignalData.Signal.Voltage.Faza_C.Harmonics[j].Angle := arr^[i];i:=i+1;//20;
                                   j:=j+1;

//7 ���������
    SignalData.Signal.Voltage.Faza_A.Harmonics[j].Koef := arr^[i];i:=i+1;//1.5;
    SignalData.Signal.Voltage.Faza_A.Harmonics[j].Angle := arr^[i];i:=i+1;//10;
    SignalData.Signal.Voltage.Faza_B.Harmonics[j].Koef := arr^[i];i:=i+1;//2.5;
    SignalData.Signal.Voltage.Faza_B.Harmonics[j].Angle := arr^[i];i:=i+1;//15;
    SignalData.Signal.Voltage.Faza_C.Harmonics[j].Koef := arr^[i];i:=i+1;//3.5;
    SignalData.Signal.Voltage.Faza_C.Harmonics[j].Angle := arr^[i];i:=i+1;//20;
                                   j:=j+1;

//8 ���������
    SignalData.Signal.Voltage.Faza_A.Harmonics[j].Koef := arr^[i];i:=i+1;//1.5;
    SignalData.Signal.Voltage.Faza_A.Harmonics[j].Angle := arr^[i];i:=i+1;//10;
    SignalData.Signal.Voltage.Faza_B.Harmonics[j].Koef := arr^[i];i:=i+1;//2.5;
    SignalData.Signal.Voltage.Faza_B.Harmonics[j].Angle := arr^[i];i:=i+1;//15;
    SignalData.Signal.Voltage.Faza_C.Harmonics[j].Koef := arr^[i];i:=i+1;//3.5;
    SignalData.Signal.Voltage.Faza_C.Harmonics[j].Angle := arr^[i];i:=i+1;//20;
                                   j:=j+1;

//9 ���������
    SignalData.Signal.Voltage.Faza_A.Harmonics[j].Koef := arr^[i];i:=i+1;//1.5;
    SignalData.Signal.Voltage.Faza_A.Harmonics[j].Angle := arr^[i];i:=i+1;//10;
    SignalData.Signal.Voltage.Faza_B.Harmonics[j].Koef := arr^[i];i:=i+1;//2.5;
    SignalData.Signal.Voltage.Faza_B.Harmonics[j].Angle := arr^[i];i:=i+1;//15;
    SignalData.Signal.Voltage.Faza_C.Harmonics[j].Koef := arr^[i];i:=i+1;//3.5;
    SignalData.Signal.Voltage.Faza_C.Harmonics[j].Angle := arr^[i];i:=i+1;//20;
                                   j:=j+1;

//10 ���������
    SignalData.Signal.Voltage.Faza_A.Harmonics[j].Koef := arr^[i];i:=i+1;//1.5;
    SignalData.Signal.Voltage.Faza_A.Harmonics[j].Angle := arr^[i];i:=i+1;//10;
    SignalData.Signal.Voltage.Faza_B.Harmonics[j].Koef := arr^[i];i:=i+1;//2.5;
    SignalData.Signal.Voltage.Faza_B.Harmonics[j].Angle := arr^[i];i:=i+1;//15;
    SignalData.Signal.Voltage.Faza_C.Harmonics[j].Koef := arr^[i];i:=i+1;//3.5;
    SignalData.Signal.Voltage.Faza_C.Harmonics[j].Angle := arr^[i];i:=i+1;//20;
                                   j:=j+1;

//11 ���������
    SignalData.Signal.Voltage.Faza_A.Harmonics[j].Koef := arr^[i];i:=i+1;//1.5;
    SignalData.Signal.Voltage.Faza_A.Harmonics[j].Angle := arr^[i];i:=i+1;//10;
    SignalData.Signal.Voltage.Faza_B.Harmonics[j].Koef := arr^[i];i:=i+1;//2.5;
    SignalData.Signal.Voltage.Faza_B.Harmonics[j].Angle := arr^[i];i:=i+1;//15;
    SignalData.Signal.Voltage.Faza_C.Harmonics[j].Koef := arr^[i];i:=i+1;//3.5;
    SignalData.Signal.Voltage.Faza_C.Harmonics[j].Angle := arr^[i];i:=i+1;//20;
                                   j:=j+1;

//12 ���������
    SignalData.Signal.Voltage.Faza_A.Harmonics[j].Koef := arr^[i];i:=i+1;//1.5;
    SignalData.Signal.Voltage.Faza_A.Harmonics[j].Angle := arr^[i];i:=i+1;//10;
    SignalData.Signal.Voltage.Faza_B.Harmonics[j].Koef := arr^[i];i:=i+1;//2.5;
    SignalData.Signal.Voltage.Faza_B.Harmonics[j].Angle := arr^[i];i:=i+1;//15;
    SignalData.Signal.Voltage.Faza_C.Harmonics[j].Koef := arr^[i];i:=i+1;//3.5;
    SignalData.Signal.Voltage.Faza_C.Harmonics[j].Angle := arr^[i];i:=i+1;//20;
                                   j:=j+1;

//13 ���������
    SignalData.Signal.Voltage.Faza_A.Harmonics[j].Koef := arr^[i];i:=i+1;//1.5;
    SignalData.Signal.Voltage.Faza_A.Harmonics[j].Angle := arr^[i];i:=i+1;//10;
    SignalData.Signal.Voltage.Faza_B.Harmonics[j].Koef := arr^[i];i:=i+1;//2.5;
    SignalData.Signal.Voltage.Faza_B.Harmonics[j].Angle := arr^[i];i:=i+1;//15;
    SignalData.Signal.Voltage.Faza_C.Harmonics[j].Koef := arr^[i];i:=i+1;//3.5;
    SignalData.Signal.Voltage.Faza_C.Harmonics[j].Angle := arr^[i];i:=i+1;//20;
                                   j:=j+1;

//14 ���������
    SignalData.Signal.Voltage.Faza_A.Harmonics[j].Koef := arr^[i];i:=i+1;//1.5;
    SignalData.Signal.Voltage.Faza_A.Harmonics[j].Angle := arr^[i];i:=i+1;//10;
    SignalData.Signal.Voltage.Faza_B.Harmonics[j].Koef := arr^[i];i:=i+1;//2.5;
    SignalData.Signal.Voltage.Faza_B.Harmonics[j].Angle := arr^[i];i:=i+1;//15;
    SignalData.Signal.Voltage.Faza_C.Harmonics[j].Koef := arr^[i];i:=i+1;//3.5;
    SignalData.Signal.Voltage.Faza_C.Harmonics[j].Angle := arr^[i];i:=i+1;//20;
                                   j:=j+1;

//15 ���������
    SignalData.Signal.Voltage.Faza_A.Harmonics[j].Koef := arr^[i];i:=i+1;//1.5;
    SignalData.Signal.Voltage.Faza_A.Harmonics[j].Angle := arr^[i];i:=i+1;//10;
    SignalData.Signal.Voltage.Faza_B.Harmonics[j].Koef := arr^[i];i:=i+1;//2.5;
    SignalData.Signal.Voltage.Faza_B.Harmonics[j].Angle := arr^[i];i:=i+1;//15;
    SignalData.Signal.Voltage.Faza_C.Harmonics[j].Koef := arr^[i];i:=i+1;//3.5;
    SignalData.Signal.Voltage.Faza_C.Harmonics[j].Angle := arr^[i];i:=i+1;//20;
                                   j:=j+1;

//16 ���������
    SignalData.Signal.Voltage.Faza_A.Harmonics[j].Koef := arr^[i];i:=i+1;//1.5;
    SignalData.Signal.Voltage.Faza_A.Harmonics[j].Angle := arr^[i];i:=i+1;//10;
    SignalData.Signal.Voltage.Faza_B.Harmonics[j].Koef := arr^[i];i:=i+1;//2.5;
    SignalData.Signal.Voltage.Faza_B.Harmonics[j].Angle := arr^[i];i:=i+1;//15;
    SignalData.Signal.Voltage.Faza_C.Harmonics[j].Koef := arr^[i];i:=i+1;//3.5;
    SignalData.Signal.Voltage.Faza_C.Harmonics[j].Angle := arr^[i];i:=i+1;//20;
                                   j:=j+1;

//17 ���������
    SignalData.Signal.Voltage.Faza_A.Harmonics[j].Koef := arr^[i];i:=i+1;//1.5;
    SignalData.Signal.Voltage.Faza_A.Harmonics[j].Angle := arr^[i];i:=i+1;//10;
    SignalData.Signal.Voltage.Faza_B.Harmonics[j].Koef := arr^[i];i:=i+1;//2.5;
    SignalData.Signal.Voltage.Faza_B.Harmonics[j].Angle := arr^[i];i:=i+1;//15;
    SignalData.Signal.Voltage.Faza_C.Harmonics[j].Koef := arr^[i];i:=i+1;//3.5;
    SignalData.Signal.Voltage.Faza_C.Harmonics[j].Angle := arr^[i];i:=i+1;//20;
                                   j:=j+1;

//18 ���������
    SignalData.Signal.Voltage.Faza_A.Harmonics[j].Koef := arr^[i];i:=i+1;//1.5;
    SignalData.Signal.Voltage.Faza_A.Harmonics[j].Angle := arr^[i];i:=i+1;//10;
    SignalData.Signal.Voltage.Faza_B.Harmonics[j].Koef := arr^[i];i:=i+1;//2.5;
    SignalData.Signal.Voltage.Faza_B.Harmonics[j].Angle := arr^[i];i:=i+1;//15;
    SignalData.Signal.Voltage.Faza_C.Harmonics[j].Koef := arr^[i];i:=i+1;//3.5;
    SignalData.Signal.Voltage.Faza_C.Harmonics[j].Angle := arr^[i];i:=i+1;//20;
                                   j:=j+1;

//19 ���������
    SignalData.Signal.Voltage.Faza_A.Harmonics[j].Koef := arr^[i];i:=i+1;//1.5;
    SignalData.Signal.Voltage.Faza_A.Harmonics[j].Angle := arr^[i];i:=i+1;//10;
    SignalData.Signal.Voltage.Faza_B.Harmonics[j].Koef := arr^[i];i:=i+1;//2.5;
    SignalData.Signal.Voltage.Faza_B.Harmonics[j].Angle := arr^[i];i:=i+1;//15;
    SignalData.Signal.Voltage.Faza_C.Harmonics[j].Koef := arr^[i];i:=i+1;//3.5;
    SignalData.Signal.Voltage.Faza_C.Harmonics[j].Angle := arr^[i];i:=i+1;//20;
                                   j:=j+1;

//20 ���������
    SignalData.Signal.Voltage.Faza_A.Harmonics[j].Koef := arr^[i];i:=i+1;//1.5;
    SignalData.Signal.Voltage.Faza_A.Harmonics[j].Angle := arr^[i];i:=i+1;//10;
    SignalData.Signal.Voltage.Faza_B.Harmonics[j].Koef := arr^[i];i:=i+1;//2.5;
    SignalData.Signal.Voltage.Faza_B.Harmonics[j].Angle := arr^[i];i:=i+1;//15;
    SignalData.Signal.Voltage.Faza_C.Harmonics[j].Koef := arr^[i];i:=i+1;//3.5;
    SignalData.Signal.Voltage.Faza_C.Harmonics[j].Angle := arr^[i];i:=i+1;//20;
                                   j:=j+1;

//21 ���������
    SignalData.Signal.Voltage.Faza_A.Harmonics[j].Koef := arr^[i];i:=i+1;//1.5;
    SignalData.Signal.Voltage.Faza_A.Harmonics[j].Angle := arr^[i];i:=i+1;//10;
    SignalData.Signal.Voltage.Faza_B.Harmonics[j].Koef := arr^[i];i:=i+1;//2.5;
    SignalData.Signal.Voltage.Faza_B.Harmonics[j].Angle := arr^[i];i:=i+1;//15;
    SignalData.Signal.Voltage.Faza_C.Harmonics[j].Koef := arr^[i];i:=i+1;//3.5;
    SignalData.Signal.Voltage.Faza_C.Harmonics[j].Angle := arr^[i];i:=i+1;//20;
                                   j:=j+1;

//22 ���������
    SignalData.Signal.Voltage.Faza_A.Harmonics[j].Koef := arr^[i];i:=i+1;//1.5;
    SignalData.Signal.Voltage.Faza_A.Harmonics[j].Angle := arr^[i];i:=i+1;//10;
    SignalData.Signal.Voltage.Faza_B.Harmonics[j].Koef := arr^[i];i:=i+1;//2.5;
    SignalData.Signal.Voltage.Faza_B.Harmonics[j].Angle := arr^[i];i:=i+1;//15;
    SignalData.Signal.Voltage.Faza_C.Harmonics[j].Koef := arr^[i];i:=i+1;//3.5;
    SignalData.Signal.Voltage.Faza_C.Harmonics[j].Angle := arr^[i];i:=i+1;//20;
                                   j:=j+1;

//23 ���������
    SignalData.Signal.Voltage.Faza_A.Harmonics[j].Koef := arr^[i];i:=i+1;//1.5;
    SignalData.Signal.Voltage.Faza_A.Harmonics[j].Angle := arr^[i];i:=i+1;//10;
    SignalData.Signal.Voltage.Faza_B.Harmonics[j].Koef := arr^[i];i:=i+1;//2.5;
    SignalData.Signal.Voltage.Faza_B.Harmonics[j].Angle := arr^[i];i:=i+1;//15;
    SignalData.Signal.Voltage.Faza_C.Harmonics[j].Koef := arr^[i];i:=i+1;//3.5;
    SignalData.Signal.Voltage.Faza_C.Harmonics[j].Angle := arr^[i];i:=i+1;//20;
                                   j:=j+1;

//24 ���������
    SignalData.Signal.Voltage.Faza_A.Harmonics[j].Koef := arr^[i];i:=i+1;//1.5;
    SignalData.Signal.Voltage.Faza_A.Harmonics[j].Angle := arr^[i];i:=i+1;//10;
    SignalData.Signal.Voltage.Faza_B.Harmonics[j].Koef := arr^[i];i:=i+1;//2.5;
    SignalData.Signal.Voltage.Faza_B.Harmonics[j].Angle := arr^[i];i:=i+1;//15;
    SignalData.Signal.Voltage.Faza_C.Harmonics[j].Koef := arr^[i];i:=i+1;//3.5;
    SignalData.Signal.Voltage.Faza_C.Harmonics[j].Angle := arr^[i];i:=i+1;//20;
                                   j:=j+1;

//25 ���������
    SignalData.Signal.Voltage.Faza_A.Harmonics[j].Koef := arr^[i];i:=i+1;//1.5;
    SignalData.Signal.Voltage.Faza_A.Harmonics[j].Angle := arr^[i];i:=i+1;//10;
    SignalData.Signal.Voltage.Faza_B.Harmonics[j].Koef := arr^[i];i:=i+1;//2.5;
    SignalData.Signal.Voltage.Faza_B.Harmonics[j].Angle := arr^[i];i:=i+1;//15;
    SignalData.Signal.Voltage.Faza_C.Harmonics[j].Koef := arr^[i];i:=i+1;//3.5;
    SignalData.Signal.Voltage.Faza_C.Harmonics[j].Angle := arr^[i];i:=i+1;//20;
                                   j:=j+1;

//26 ���������
    SignalData.Signal.Voltage.Faza_A.Harmonics[j].Koef := arr^[i];i:=i+1;//1.5;
    SignalData.Signal.Voltage.Faza_A.Harmonics[j].Angle := arr^[i];i:=i+1;//10;
    SignalData.Signal.Voltage.Faza_B.Harmonics[j].Koef := arr^[i];i:=i+1;//2.5;
    SignalData.Signal.Voltage.Faza_B.Harmonics[j].Angle := arr^[i];i:=i+1;//15;
    SignalData.Signal.Voltage.Faza_C.Harmonics[j].Koef := arr^[i];i:=i+1;//3.5;
    SignalData.Signal.Voltage.Faza_C.Harmonics[j].Angle := arr^[i];i:=i+1;//20;
                                   j:=j+1;

//27 ���������
    SignalData.Signal.Voltage.Faza_A.Harmonics[j].Koef := arr^[i];i:=i+1;//1.5;
    SignalData.Signal.Voltage.Faza_A.Harmonics[j].Angle := arr^[i];i:=i+1;//10;
    SignalData.Signal.Voltage.Faza_B.Harmonics[j].Koef := arr^[i];i:=i+1;//2.5;
    SignalData.Signal.Voltage.Faza_B.Harmonics[j].Angle := arr^[i];i:=i+1;//15;
    SignalData.Signal.Voltage.Faza_C.Harmonics[j].Koef := arr^[i];i:=i+1;//3.5;
    SignalData.Signal.Voltage.Faza_C.Harmonics[j].Angle := arr^[i];i:=i+1;//20;
                                   j:=j+1;

//28 ���������
    SignalData.Signal.Voltage.Faza_A.Harmonics[j].Koef := arr^[i];i:=i+1;//1.5;
    SignalData.Signal.Voltage.Faza_A.Harmonics[j].Angle := arr^[i];i:=i+1;//10;
    SignalData.Signal.Voltage.Faza_B.Harmonics[j].Koef := arr^[i];i:=i+1;//2.5;
    SignalData.Signal.Voltage.Faza_B.Harmonics[j].Angle := arr^[i];i:=i+1;//15;
    SignalData.Signal.Voltage.Faza_C.Harmonics[j].Koef := arr^[i];i:=i+1;//3.5;
    SignalData.Signal.Voltage.Faza_C.Harmonics[j].Angle := arr^[i];i:=i+1;//20;
                                   j:=j+1;

//29 ���������
    SignalData.Signal.Voltage.Faza_A.Harmonics[j].Koef := arr^[i];i:=i+1;//1.5;
    SignalData.Signal.Voltage.Faza_A.Harmonics[j].Angle := arr^[i];i:=i+1;//10;
    SignalData.Signal.Voltage.Faza_B.Harmonics[j].Koef := arr^[i];i:=i+1;//2.5;
    SignalData.Signal.Voltage.Faza_B.Harmonics[j].Angle := arr^[i];i:=i+1;//15;
    SignalData.Signal.Voltage.Faza_C.Harmonics[j].Koef := arr^[i];i:=i+1;//3.5;
    SignalData.Signal.Voltage.Faza_C.Harmonics[j].Angle := arr^[i];i:=i+1;//20;
                                   j:=j+1;

//30 ���������
    SignalData.Signal.Voltage.Faza_A.Harmonics[j].Koef := arr^[i];i:=i+1;//1.5;
    SignalData.Signal.Voltage.Faza_A.Harmonics[j].Angle := arr^[i];i:=i+1;//10;
    SignalData.Signal.Voltage.Faza_B.Harmonics[j].Koef := arr^[i];i:=i+1;//2.5;
    SignalData.Signal.Voltage.Faza_B.Harmonics[j].Angle := arr^[i];i:=i+1;//15;
    SignalData.Signal.Voltage.Faza_C.Harmonics[j].Koef := arr^[i];i:=i+1;//3.5;
    SignalData.Signal.Voltage.Faza_C.Harmonics[j].Angle := arr^[i];i:=i+1;//20;
                                   j:=j+1;

//31 ���������
    SignalData.Signal.Voltage.Faza_A.Harmonics[j].Koef := arr^[i];i:=i+1;//1.5;
    SignalData.Signal.Voltage.Faza_A.Harmonics[j].Angle := arr^[i];i:=i+1;//10;
    SignalData.Signal.Voltage.Faza_B.Harmonics[j].Koef := arr^[i];i:=i+1;//2.5;
    SignalData.Signal.Voltage.Faza_B.Harmonics[j].Angle := arr^[i];i:=i+1;//15;
    SignalData.Signal.Voltage.Faza_C.Harmonics[j].Koef := arr^[i];i:=i+1;//3.5;
    SignalData.Signal.Voltage.Faza_C.Harmonics[j].Angle := arr^[i];i:=i+1;//20;
                                   j:=j+1;

//32 ���������
    SignalData.Signal.Voltage.Faza_A.Harmonics[j].Koef := arr^[i];i:=i+1;//1.5;
    SignalData.Signal.Voltage.Faza_A.Harmonics[j].Angle := arr^[i];i:=i+1;//10;
    SignalData.Signal.Voltage.Faza_B.Harmonics[j].Koef := arr^[i];i:=i+1;//2.5;
    SignalData.Signal.Voltage.Faza_B.Harmonics[j].Angle := arr^[i];i:=i+1;//15;
    SignalData.Signal.Voltage.Faza_C.Harmonics[j].Koef := arr^[i];i:=i+1;//3.5;
    SignalData.Signal.Voltage.Faza_C.Harmonics[j].Angle := arr^[i];i:=i+1;//20;
                                   j:=j+1;

//33 ���������
    SignalData.Signal.Voltage.Faza_A.Harmonics[j].Koef := arr^[i];i:=i+1;//1.5;
    SignalData.Signal.Voltage.Faza_A.Harmonics[j].Angle := arr^[i];i:=i+1;//10;
    SignalData.Signal.Voltage.Faza_B.Harmonics[j].Koef := arr^[i];i:=i+1;//2.5;
    SignalData.Signal.Voltage.Faza_B.Harmonics[j].Angle := arr^[i];i:=i+1;//15;
    SignalData.Signal.Voltage.Faza_C.Harmonics[j].Koef := arr^[i];i:=i+1;//3.5;
    SignalData.Signal.Voltage.Faza_C.Harmonics[j].Angle := arr^[i];i:=i+1;//20;
                                   j:=j+1;

//34 ���������
    SignalData.Signal.Voltage.Faza_A.Harmonics[j].Koef := arr^[i];i:=i+1;//1.5;
    SignalData.Signal.Voltage.Faza_A.Harmonics[j].Angle := arr^[i];i:=i+1;//10;
    SignalData.Signal.Voltage.Faza_B.Harmonics[j].Koef := arr^[i];i:=i+1;//2.5;
    SignalData.Signal.Voltage.Faza_B.Harmonics[j].Angle := arr^[i];i:=i+1;//15;
    SignalData.Signal.Voltage.Faza_C.Harmonics[j].Koef := arr^[i];i:=i+1;//3.5;
    SignalData.Signal.Voltage.Faza_C.Harmonics[j].Angle := arr^[i];i:=i+1;//20;
                                   j:=j+1;

//35 ���������
    SignalData.Signal.Voltage.Faza_A.Harmonics[j].Koef := arr^[i];i:=i+1;//1.5;
    SignalData.Signal.Voltage.Faza_A.Harmonics[j].Angle := arr^[i];i:=i+1;//10;
    SignalData.Signal.Voltage.Faza_B.Harmonics[j].Koef := arr^[i];i:=i+1;//2.5;
    SignalData.Signal.Voltage.Faza_B.Harmonics[j].Angle := arr^[i];i:=i+1;//15;
    SignalData.Signal.Voltage.Faza_C.Harmonics[j].Koef := arr^[i];i:=i+1;//3.5;
    SignalData.Signal.Voltage.Faza_C.Harmonics[j].Angle := arr^[i];i:=i+1;//20;
                                   j:=j+1;

//36 ���������
    SignalData.Signal.Voltage.Faza_A.Harmonics[j].Koef := arr^[i];i:=i+1;//1.5;
    SignalData.Signal.Voltage.Faza_A.Harmonics[j].Angle := arr^[i];i:=i+1;//10;
    SignalData.Signal.Voltage.Faza_B.Harmonics[j].Koef := arr^[i];i:=i+1;//2.5;
    SignalData.Signal.Voltage.Faza_B.Harmonics[j].Angle := arr^[i];i:=i+1;//15;
    SignalData.Signal.Voltage.Faza_C.Harmonics[j].Koef := arr^[i];i:=i+1;//3.5;
    SignalData.Signal.Voltage.Faza_C.Harmonics[j].Angle := arr^[i];i:=i+1;//20;
                                   j:=j+1;

//37 ���������
    SignalData.Signal.Voltage.Faza_A.Harmonics[j].Koef := arr^[i];i:=i+1;//1.5;
    SignalData.Signal.Voltage.Faza_A.Harmonics[j].Angle := arr^[i];i:=i+1;//10;
    SignalData.Signal.Voltage.Faza_B.Harmonics[j].Koef := arr^[i];i:=i+1;//2.5;
    SignalData.Signal.Voltage.Faza_B.Harmonics[j].Angle := arr^[i];i:=i+1;//15;
    SignalData.Signal.Voltage.Faza_C.Harmonics[j].Koef := arr^[i];i:=i+1;//3.5;
    SignalData.Signal.Voltage.Faza_C.Harmonics[j].Angle := arr^[i];i:=i+1;//20;
                                   j:=j+1;

//38 ���������
    SignalData.Signal.Voltage.Faza_A.Harmonics[j].Koef := arr^[i];i:=i+1;//1.5;
    SignalData.Signal.Voltage.Faza_A.Harmonics[j].Angle := arr^[i];i:=i+1;//10;
    SignalData.Signal.Voltage.Faza_B.Harmonics[j].Koef := arr^[i];i:=i+1;//2.5;
    SignalData.Signal.Voltage.Faza_B.Harmonics[j].Angle := arr^[i];i:=i+1;//15;
    SignalData.Signal.Voltage.Faza_C.Harmonics[j].Koef := arr^[i];i:=i+1;//3.5;
    SignalData.Signal.Voltage.Faza_C.Harmonics[j].Angle := arr^[i];i:=i+1;//20;
                                   j:=j+1;

//39 ���������
    SignalData.Signal.Voltage.Faza_A.Harmonics[j].Koef := arr^[i];i:=i+1;//1.5;
    SignalData.Signal.Voltage.Faza_A.Harmonics[j].Angle := arr^[i];i:=i+1;//10;
    SignalData.Signal.Voltage.Faza_B.Harmonics[j].Koef := arr^[i];i:=i+1;//2.5;
    SignalData.Signal.Voltage.Faza_B.Harmonics[j].Angle := arr^[i];i:=i+1;//15;
    SignalData.Signal.Voltage.Faza_C.Harmonics[j].Koef := arr^[i];i:=i+1;//3.5;
    SignalData.Signal.Voltage.Faza_C.Harmonics[j].Angle := arr^[i];i:=i+1;//20;
                                   j:=j+1;

//40 ���������
    SignalData.Signal.Voltage.Faza_A.Harmonics[j].Koef := arr^[i];i:=i+1;//1.5;
    SignalData.Signal.Voltage.Faza_A.Harmonics[j].Angle := arr^[i];i:=i+1;//10;
    SignalData.Signal.Voltage.Faza_B.Harmonics[j].Koef := arr^[i];i:=i+1;//2.5;
    SignalData.Signal.Voltage.Faza_B.Harmonics[j].Angle := arr^[i];i:=i+1;//15;
    SignalData.Signal.Voltage.Faza_C.Harmonics[j].Koef := arr^[i];i:=i+1;//3.5;
    SignalData.Signal.Voltage.Faza_C.Harmonics[j].Angle := arr^[i];i:=i+1;//20;
                                  // j:=j+1;
///������� ����
 SignalData.Signal.Current.NominalID := trunc(arr^[i]);i:=i+1;//CONST_NOMINAL_I_5A
// ���� ��� �� �������
 SignalData.Signal.Current.Enable:=(arr^[i]>0.0); ;i:=i+1;

    SignalData.Signal.Current.Faza_A.IncludeMainFreq := (arr^[i]>0.0); ;i:=i+1;// 1 'True
    SignalData.Signal.Current.Faza_A.MainFreqVoltCur.Koef := arr^[i];i:=i+1;//5
    SignalData.Signal.Current.Faza_A.MainFreqVoltCur.Angle := arr^[i];i:=i+1;//30
    SignalData.Signal.Current.Faza_B.IncludeMainFreq := (arr^[i]>0.0);i:=i+1;//1 'True
    SignalData.Signal.Current.Faza_B.MainFreqVoltCur.Koef := arr^[i];i:=i+1;//4
    SignalData.Signal.Current.Faza_B.MainFreqVoltCur.Angle := arr^[i];i:=i+1;//-30
    SignalData.Signal.Current.Faza_C.IncludeMainFreq := (arr^[i]>0.0);i:=i+1;//1 'True
    SignalData.Signal.Current.Faza_C.MainFreqVoltCur.Koef := arr^[i];i:=i+1;//3
    SignalData.Signal.Current.Faza_C.MainFreqVoltCur.Angle := arr^[i];i:=i+1;//0
//��������� ����
                     j:=2;
//2���������
    SignalData.Signal.Current.Faza_A.Harmonics[j].Koef := arr^[i];i:=i+1;//5.5
    SignalData.Signal.Current.Faza_A.Harmonics[j].Angle := arr^[i];i:=i+1;//10
    SignalData.Signal.Current.Faza_B.Harmonics[j].Koef := arr^[i];i:=i+1;//4.5
    SignalData.Signal.Current.Faza_B.Harmonics[j].Angle := arr^[i];i:=i+1;//15
    SignalData.Signal.Current.Faza_C.Harmonics[j].Koef := arr^[i];i:=i+1;//4
    SignalData.Signal.Current.Faza_C.Harmonics[j].Angle := arr^[i];i:=i+1;//20
                                j:=j+1;
//3
    SignalData.Signal.Current.Faza_A.Harmonics[j].Koef := arr^[i];i:=i+1;//5.5
    SignalData.Signal.Current.Faza_A.Harmonics[j].Angle := arr^[i];i:=i+1;//10
    SignalData.Signal.Current.Faza_B.Harmonics[j].Koef := arr^[i];i:=i+1;//4.5
    SignalData.Signal.Current.Faza_B.Harmonics[j].Angle := arr^[i];i:=i+1;//15
    SignalData.Signal.Current.Faza_C.Harmonics[j].Koef := arr^[i];i:=i+1;//4
    SignalData.Signal.Current.Faza_C.Harmonics[j].Angle := arr^[i];i:=i+1;//20
                                j:=j+1;

//4
    SignalData.Signal.Current.Faza_A.Harmonics[j].Koef := arr^[i];i:=i+1;//5.5
    SignalData.Signal.Current.Faza_A.Harmonics[j].Angle := arr^[i];i:=i+1;//10
    SignalData.Signal.Current.Faza_B.Harmonics[j].Koef := arr^[i];i:=i+1;//4.5
    SignalData.Signal.Current.Faza_B.Harmonics[j].Angle := arr^[i];i:=i+1;//15
    SignalData.Signal.Current.Faza_C.Harmonics[j].Koef := arr^[i];i:=i+1;//4
    SignalData.Signal.Current.Faza_C.Harmonics[j].Angle := arr^[i];i:=i+1;//20
                                j:=j+1;

//5
    SignalData.Signal.Current.Faza_A.Harmonics[j].Koef := arr^[i];i:=i+1;//5.5
    SignalData.Signal.Current.Faza_A.Harmonics[j].Angle := arr^[i];i:=i+1;//10
    SignalData.Signal.Current.Faza_B.Harmonics[j].Koef := arr^[i];i:=i+1;//4.5
    SignalData.Signal.Current.Faza_B.Harmonics[j].Angle := arr^[i];i:=i+1;//15
    SignalData.Signal.Current.Faza_C.Harmonics[j].Koef := arr^[i];i:=i+1;//4
    SignalData.Signal.Current.Faza_C.Harmonics[j].Angle := arr^[i];i:=i+1;//20
                                j:=j+1;

//6
    SignalData.Signal.Current.Faza_A.Harmonics[j].Koef := arr^[i];i:=i+1;//5.5
    SignalData.Signal.Current.Faza_A.Harmonics[j].Angle := arr^[i];i:=i+1;//10
    SignalData.Signal.Current.Faza_B.Harmonics[j].Koef := arr^[i];i:=i+1;//4.5
    SignalData.Signal.Current.Faza_B.Harmonics[j].Angle := arr^[i];i:=i+1;//15
    SignalData.Signal.Current.Faza_C.Harmonics[j].Koef := arr^[i];i:=i+1;//4
    SignalData.Signal.Current.Faza_C.Harmonics[j].Angle := arr^[i];i:=i+1;//20
                                j:=j+1;

//7
    SignalData.Signal.Current.Faza_A.Harmonics[j].Koef := arr^[i];i:=i+1;//5.5
    SignalData.Signal.Current.Faza_A.Harmonics[j].Angle := arr^[i];i:=i+1;//10
    SignalData.Signal.Current.Faza_B.Harmonics[j].Koef := arr^[i];i:=i+1;//4.5
    SignalData.Signal.Current.Faza_B.Harmonics[j].Angle := arr^[i];i:=i+1;//15
    SignalData.Signal.Current.Faza_C.Harmonics[j].Koef := arr^[i];i:=i+1;//4
    SignalData.Signal.Current.Faza_C.Harmonics[j].Angle := arr^[i];i:=i+1;//20
                                j:=j+1;

//8
    SignalData.Signal.Current.Faza_A.Harmonics[j].Koef := arr^[i];i:=i+1;//5.5
    SignalData.Signal.Current.Faza_A.Harmonics[j].Angle := arr^[i];i:=i+1;//10
    SignalData.Signal.Current.Faza_B.Harmonics[j].Koef := arr^[i];i:=i+1;//4.5
    SignalData.Signal.Current.Faza_B.Harmonics[j].Angle := arr^[i];i:=i+1;//15
    SignalData.Signal.Current.Faza_C.Harmonics[j].Koef := arr^[i];i:=i+1;//4
    SignalData.Signal.Current.Faza_C.Harmonics[j].Angle := arr^[i];i:=i+1;//20
                                j:=j+1;

//9
    SignalData.Signal.Current.Faza_A.Harmonics[j].Koef := arr^[i];i:=i+1;//5.5
    SignalData.Signal.Current.Faza_A.Harmonics[j].Angle := arr^[i];i:=i+1;//10
    SignalData.Signal.Current.Faza_B.Harmonics[j].Koef := arr^[i];i:=i+1;//4.5
    SignalData.Signal.Current.Faza_B.Harmonics[j].Angle := arr^[i];i:=i+1;//15
    SignalData.Signal.Current.Faza_C.Harmonics[j].Koef := arr^[i];i:=i+1;//4
    SignalData.Signal.Current.Faza_C.Harmonics[j].Angle := arr^[i];i:=i+1;//20
                                j:=j+1;

//10
    SignalData.Signal.Current.Faza_A.Harmonics[j].Koef := arr^[i];i:=i+1;//5.5
    SignalData.Signal.Current.Faza_A.Harmonics[j].Angle := arr^[i];i:=i+1;//10
    SignalData.Signal.Current.Faza_B.Harmonics[j].Koef := arr^[i];i:=i+1;//4.5
    SignalData.Signal.Current.Faza_B.Harmonics[j].Angle := arr^[i];i:=i+1;//15
    SignalData.Signal.Current.Faza_C.Harmonics[j].Koef := arr^[i];i:=i+1;//4
    SignalData.Signal.Current.Faza_C.Harmonics[j].Angle := arr^[i];i:=i+1;//20
                                j:=j+1;

//11
    SignalData.Signal.Current.Faza_A.Harmonics[j].Koef := arr^[i];i:=i+1;//5.5
    SignalData.Signal.Current.Faza_A.Harmonics[j].Angle := arr^[i];i:=i+1;//10
    SignalData.Signal.Current.Faza_B.Harmonics[j].Koef := arr^[i];i:=i+1;//4.5
    SignalData.Signal.Current.Faza_B.Harmonics[j].Angle := arr^[i];i:=i+1;//15
    SignalData.Signal.Current.Faza_C.Harmonics[j].Koef := arr^[i];i:=i+1;//4
    SignalData.Signal.Current.Faza_C.Harmonics[j].Angle := arr^[i];i:=i+1;//20
                                j:=j+1;

//12
    SignalData.Signal.Current.Faza_A.Harmonics[j].Koef := arr^[i];i:=i+1;//5.5
    SignalData.Signal.Current.Faza_A.Harmonics[j].Angle := arr^[i];i:=i+1;//10
    SignalData.Signal.Current.Faza_B.Harmonics[j].Koef := arr^[i];i:=i+1;//4.5
    SignalData.Signal.Current.Faza_B.Harmonics[j].Angle := arr^[i];i:=i+1;//15
    SignalData.Signal.Current.Faza_C.Harmonics[j].Koef := arr^[i];i:=i+1;//4
    SignalData.Signal.Current.Faza_C.Harmonics[j].Angle := arr^[i];i:=i+1;//20
                                j:=j+1;

//13
    SignalData.Signal.Current.Faza_A.Harmonics[j].Koef := arr^[i];i:=i+1;//5.5
    SignalData.Signal.Current.Faza_A.Harmonics[j].Angle := arr^[i];i:=i+1;//10
    SignalData.Signal.Current.Faza_B.Harmonics[j].Koef := arr^[i];i:=i+1;//4.5
    SignalData.Signal.Current.Faza_B.Harmonics[j].Angle := arr^[i];i:=i+1;//15
    SignalData.Signal.Current.Faza_C.Harmonics[j].Koef := arr^[i];i:=i+1;//4
    SignalData.Signal.Current.Faza_C.Harmonics[j].Angle := arr^[i];i:=i+1;//20
                                j:=j+1;

//14
    SignalData.Signal.Current.Faza_A.Harmonics[j].Koef := arr^[i];i:=i+1;//5.5
    SignalData.Signal.Current.Faza_A.Harmonics[j].Angle := arr^[i];i:=i+1;//10
    SignalData.Signal.Current.Faza_B.Harmonics[j].Koef := arr^[i];i:=i+1;//4.5
    SignalData.Signal.Current.Faza_B.Harmonics[j].Angle := arr^[i];i:=i+1;//15
    SignalData.Signal.Current.Faza_C.Harmonics[j].Koef := arr^[i];i:=i+1;//4
    SignalData.Signal.Current.Faza_C.Harmonics[j].Angle := arr^[i];i:=i+1;//20
                                j:=j+1;

//15
    SignalData.Signal.Current.Faza_A.Harmonics[j].Koef := arr^[i];i:=i+1;//5.5
    SignalData.Signal.Current.Faza_A.Harmonics[j].Angle := arr^[i];i:=i+1;//10
    SignalData.Signal.Current.Faza_B.Harmonics[j].Koef := arr^[i];i:=i+1;//4.5
    SignalData.Signal.Current.Faza_B.Harmonics[j].Angle := arr^[i];i:=i+1;//15
    SignalData.Signal.Current.Faza_C.Harmonics[j].Koef := arr^[i];i:=i+1;//4
    SignalData.Signal.Current.Faza_C.Harmonics[j].Angle := arr^[i];i:=i+1;//20
                                j:=j+1;

//16
    SignalData.Signal.Current.Faza_A.Harmonics[j].Koef := arr^[i];i:=i+1;//5.5
    SignalData.Signal.Current.Faza_A.Harmonics[j].Angle := arr^[i];i:=i+1;//10
    SignalData.Signal.Current.Faza_B.Harmonics[j].Koef := arr^[i];i:=i+1;//4.5
    SignalData.Signal.Current.Faza_B.Harmonics[j].Angle := arr^[i];i:=i+1;//15
    SignalData.Signal.Current.Faza_C.Harmonics[j].Koef := arr^[i];i:=i+1;//4
    SignalData.Signal.Current.Faza_C.Harmonics[j].Angle := arr^[i];i:=i+1;//20
                                j:=j+1;

//17
    SignalData.Signal.Current.Faza_A.Harmonics[j].Koef := arr^[i];i:=i+1;//5.5
    SignalData.Signal.Current.Faza_A.Harmonics[j].Angle := arr^[i];i:=i+1;//10
    SignalData.Signal.Current.Faza_B.Harmonics[j].Koef := arr^[i];i:=i+1;//4.5
    SignalData.Signal.Current.Faza_B.Harmonics[j].Angle := arr^[i];i:=i+1;//15
    SignalData.Signal.Current.Faza_C.Harmonics[j].Koef := arr^[i];i:=i+1;//4
    SignalData.Signal.Current.Faza_C.Harmonics[j].Angle := arr^[i];i:=i+1;//20
                                j:=j+1;

//18
    SignalData.Signal.Current.Faza_A.Harmonics[j].Koef := arr^[i];i:=i+1;//5.5
    SignalData.Signal.Current.Faza_A.Harmonics[j].Angle := arr^[i];i:=i+1;//10
    SignalData.Signal.Current.Faza_B.Harmonics[j].Koef := arr^[i];i:=i+1;//4.5
    SignalData.Signal.Current.Faza_B.Harmonics[j].Angle := arr^[i];i:=i+1;//15
    SignalData.Signal.Current.Faza_C.Harmonics[j].Koef := arr^[i];i:=i+1;//4
    SignalData.Signal.Current.Faza_C.Harmonics[j].Angle := arr^[i];i:=i+1;//20
                                j:=j+1;

//19
    SignalData.Signal.Current.Faza_A.Harmonics[j].Koef := arr^[i];i:=i+1;//5.5
    SignalData.Signal.Current.Faza_A.Harmonics[j].Angle := arr^[i];i:=i+1;//10
    SignalData.Signal.Current.Faza_B.Harmonics[j].Koef := arr^[i];i:=i+1;//4.5
    SignalData.Signal.Current.Faza_B.Harmonics[j].Angle := arr^[i];i:=i+1;//15
    SignalData.Signal.Current.Faza_C.Harmonics[j].Koef := arr^[i];i:=i+1;//4
    SignalData.Signal.Current.Faza_C.Harmonics[j].Angle := arr^[i];i:=i+1;//20
                                j:=j+1;

//20
    SignalData.Signal.Current.Faza_A.Harmonics[j].Koef := arr^[i];i:=i+1;//5.5
    SignalData.Signal.Current.Faza_A.Harmonics[j].Angle := arr^[i];i:=i+1;//10
    SignalData.Signal.Current.Faza_B.Harmonics[j].Koef := arr^[i];i:=i+1;//4.5
    SignalData.Signal.Current.Faza_B.Harmonics[j].Angle := arr^[i];i:=i+1;//15
    SignalData.Signal.Current.Faza_C.Harmonics[j].Koef := arr^[i];i:=i+1;//4
    SignalData.Signal.Current.Faza_C.Harmonics[j].Angle := arr^[i];i:=i+1;//20
                                j:=j+1;

//21
    SignalData.Signal.Current.Faza_A.Harmonics[j].Koef := arr^[i];i:=i+1;//5.5
    SignalData.Signal.Current.Faza_A.Harmonics[j].Angle := arr^[i];i:=i+1;//10
    SignalData.Signal.Current.Faza_B.Harmonics[j].Koef := arr^[i];i:=i+1;//4.5
    SignalData.Signal.Current.Faza_B.Harmonics[j].Angle := arr^[i];i:=i+1;//15
    SignalData.Signal.Current.Faza_C.Harmonics[j].Koef := arr^[i];i:=i+1;//4
    SignalData.Signal.Current.Faza_C.Harmonics[j].Angle := arr^[i];i:=i+1;//20
                                j:=j+1;

//22
    SignalData.Signal.Current.Faza_A.Harmonics[j].Koef := arr^[i];i:=i+1;//5.5
    SignalData.Signal.Current.Faza_A.Harmonics[j].Angle := arr^[i];i:=i+1;//10
    SignalData.Signal.Current.Faza_B.Harmonics[j].Koef := arr^[i];i:=i+1;//4.5
    SignalData.Signal.Current.Faza_B.Harmonics[j].Angle := arr^[i];i:=i+1;//15
    SignalData.Signal.Current.Faza_C.Harmonics[j].Koef := arr^[i];i:=i+1;//4
    SignalData.Signal.Current.Faza_C.Harmonics[j].Angle := arr^[i];i:=i+1;//20
                                j:=j+1;

//23
    SignalData.Signal.Current.Faza_A.Harmonics[j].Koef := arr^[i];i:=i+1;//5.5
    SignalData.Signal.Current.Faza_A.Harmonics[j].Angle := arr^[i];i:=i+1;//10
    SignalData.Signal.Current.Faza_B.Harmonics[j].Koef := arr^[i];i:=i+1;//4.5
    SignalData.Signal.Current.Faza_B.Harmonics[j].Angle := arr^[i];i:=i+1;//15
    SignalData.Signal.Current.Faza_C.Harmonics[j].Koef := arr^[i];i:=i+1;//4
    SignalData.Signal.Current.Faza_C.Harmonics[j].Angle := arr^[i];i:=i+1;//20
                                j:=j+1;

//24
    SignalData.Signal.Current.Faza_A.Harmonics[j].Koef := arr^[i];i:=i+1;//5.5
    SignalData.Signal.Current.Faza_A.Harmonics[j].Angle := arr^[i];i:=i+1;//10
    SignalData.Signal.Current.Faza_B.Harmonics[j].Koef := arr^[i];i:=i+1;//4.5
    SignalData.Signal.Current.Faza_B.Harmonics[j].Angle := arr^[i];i:=i+1;//15
    SignalData.Signal.Current.Faza_C.Harmonics[j].Koef := arr^[i];i:=i+1;//4
    SignalData.Signal.Current.Faza_C.Harmonics[j].Angle := arr^[i];i:=i+1;//20
                                j:=j+1;

//25
    SignalData.Signal.Current.Faza_A.Harmonics[j].Koef := arr^[i];i:=i+1;//5.5
    SignalData.Signal.Current.Faza_A.Harmonics[j].Angle := arr^[i];i:=i+1;//10
    SignalData.Signal.Current.Faza_B.Harmonics[j].Koef := arr^[i];i:=i+1;//4.5
    SignalData.Signal.Current.Faza_B.Harmonics[j].Angle := arr^[i];i:=i+1;//15
    SignalData.Signal.Current.Faza_C.Harmonics[j].Koef := arr^[i];i:=i+1;//4
    SignalData.Signal.Current.Faza_C.Harmonics[j].Angle := arr^[i];i:=i+1;//20
                                j:=j+1;

//26
    SignalData.Signal.Current.Faza_A.Harmonics[j].Koef := arr^[i];i:=i+1;//5.5
    SignalData.Signal.Current.Faza_A.Harmonics[j].Angle := arr^[i];i:=i+1;//10
    SignalData.Signal.Current.Faza_B.Harmonics[j].Koef := arr^[i];i:=i+1;//4.5
    SignalData.Signal.Current.Faza_B.Harmonics[j].Angle := arr^[i];i:=i+1;//15
    SignalData.Signal.Current.Faza_C.Harmonics[j].Koef := arr^[i];i:=i+1;//4
    SignalData.Signal.Current.Faza_C.Harmonics[j].Angle := arr^[i];i:=i+1;//20
                                j:=j+1;

//27
    SignalData.Signal.Current.Faza_A.Harmonics[j].Koef := arr^[i];i:=i+1;//5.5
    SignalData.Signal.Current.Faza_A.Harmonics[j].Angle := arr^[i];i:=i+1;//10
    SignalData.Signal.Current.Faza_B.Harmonics[j].Koef := arr^[i];i:=i+1;//4.5
    SignalData.Signal.Current.Faza_B.Harmonics[j].Angle := arr^[i];i:=i+1;//15
    SignalData.Signal.Current.Faza_C.Harmonics[j].Koef := arr^[i];i:=i+1;//4
    SignalData.Signal.Current.Faza_C.Harmonics[j].Angle := arr^[i];i:=i+1;//20
                                j:=j+1;

//28
    SignalData.Signal.Current.Faza_A.Harmonics[j].Koef := arr^[i];i:=i+1;//5.5
    SignalData.Signal.Current.Faza_A.Harmonics[j].Angle := arr^[i];i:=i+1;//10
    SignalData.Signal.Current.Faza_B.Harmonics[j].Koef := arr^[i];i:=i+1;//4.5
    SignalData.Signal.Current.Faza_B.Harmonics[j].Angle := arr^[i];i:=i+1;//15
    SignalData.Signal.Current.Faza_C.Harmonics[j].Koef := arr^[i];i:=i+1;//4
    SignalData.Signal.Current.Faza_C.Harmonics[j].Angle := arr^[i];i:=i+1;//20
                                j:=j+1;

//29
    SignalData.Signal.Current.Faza_A.Harmonics[j].Koef := arr^[i];i:=i+1;//5.5
    SignalData.Signal.Current.Faza_A.Harmonics[j].Angle := arr^[i];i:=i+1;//10
    SignalData.Signal.Current.Faza_B.Harmonics[j].Koef := arr^[i];i:=i+1;//4.5
    SignalData.Signal.Current.Faza_B.Harmonics[j].Angle := arr^[i];i:=i+1;//15
    SignalData.Signal.Current.Faza_C.Harmonics[j].Koef := arr^[i];i:=i+1;//4
    SignalData.Signal.Current.Faza_C.Harmonics[j].Angle := arr^[i];i:=i+1;//20
                                j:=j+1;

//30
    SignalData.Signal.Current.Faza_A.Harmonics[j].Koef := arr^[i];i:=i+1;//5.5
    SignalData.Signal.Current.Faza_A.Harmonics[j].Angle := arr^[i];i:=i+1;//10
    SignalData.Signal.Current.Faza_B.Harmonics[j].Koef := arr^[i];i:=i+1;//4.5
    SignalData.Signal.Current.Faza_B.Harmonics[j].Angle := arr^[i];i:=i+1;//15
    SignalData.Signal.Current.Faza_C.Harmonics[j].Koef := arr^[i];i:=i+1;//4
    SignalData.Signal.Current.Faza_C.Harmonics[j].Angle := arr^[i];i:=i+1;//20
                                j:=j+1;

//31
    SignalData.Signal.Current.Faza_A.Harmonics[j].Koef := arr^[i];i:=i+1;//5.5
    SignalData.Signal.Current.Faza_A.Harmonics[j].Angle := arr^[i];i:=i+1;//10
    SignalData.Signal.Current.Faza_B.Harmonics[j].Koef := arr^[i];i:=i+1;//4.5
    SignalData.Signal.Current.Faza_B.Harmonics[j].Angle := arr^[i];i:=i+1;//15
    SignalData.Signal.Current.Faza_C.Harmonics[j].Koef := arr^[i];i:=i+1;//4
    SignalData.Signal.Current.Faza_C.Harmonics[j].Angle := arr^[i];i:=i+1;//20
                                j:=j+1;

//32
    SignalData.Signal.Current.Faza_A.Harmonics[j].Koef := arr^[i];i:=i+1;//5.5
    SignalData.Signal.Current.Faza_A.Harmonics[j].Angle := arr^[i];i:=i+1;//10
    SignalData.Signal.Current.Faza_B.Harmonics[j].Koef := arr^[i];i:=i+1;//4.5
    SignalData.Signal.Current.Faza_B.Harmonics[j].Angle := arr^[i];i:=i+1;//15
    SignalData.Signal.Current.Faza_C.Harmonics[j].Koef := arr^[i];i:=i+1;//4
    SignalData.Signal.Current.Faza_C.Harmonics[j].Angle := arr^[i];i:=i+1;//20
                                j:=j+1;

//33
    SignalData.Signal.Current.Faza_A.Harmonics[j].Koef := arr^[i];i:=i+1;//5.5
    SignalData.Signal.Current.Faza_A.Harmonics[j].Angle := arr^[i];i:=i+1;//10
    SignalData.Signal.Current.Faza_B.Harmonics[j].Koef := arr^[i];i:=i+1;//4.5
    SignalData.Signal.Current.Faza_B.Harmonics[j].Angle := arr^[i];i:=i+1;//15
    SignalData.Signal.Current.Faza_C.Harmonics[j].Koef := arr^[i];i:=i+1;//4
    SignalData.Signal.Current.Faza_C.Harmonics[j].Angle := arr^[i];i:=i+1;//20
                                j:=j+1;

//34
    SignalData.Signal.Current.Faza_A.Harmonics[j].Koef := arr^[i];i:=i+1;//5.5
    SignalData.Signal.Current.Faza_A.Harmonics[j].Angle := arr^[i];i:=i+1;//10
    SignalData.Signal.Current.Faza_B.Harmonics[j].Koef := arr^[i];i:=i+1;//4.5
    SignalData.Signal.Current.Faza_B.Harmonics[j].Angle := arr^[i];i:=i+1;//15
    SignalData.Signal.Current.Faza_C.Harmonics[j].Koef := arr^[i];i:=i+1;//4
    SignalData.Signal.Current.Faza_C.Harmonics[j].Angle := arr^[i];i:=i+1;//20
                                j:=j+1;

//35
    SignalData.Signal.Current.Faza_A.Harmonics[j].Koef := arr^[i];i:=i+1;//5.5
    SignalData.Signal.Current.Faza_A.Harmonics[j].Angle := arr^[i];i:=i+1;//10
    SignalData.Signal.Current.Faza_B.Harmonics[j].Koef := arr^[i];i:=i+1;//4.5
    SignalData.Signal.Current.Faza_B.Harmonics[j].Angle := arr^[i];i:=i+1;//15
    SignalData.Signal.Current.Faza_C.Harmonics[j].Koef := arr^[i];i:=i+1;//4
    SignalData.Signal.Current.Faza_C.Harmonics[j].Angle := arr^[i];i:=i+1;//20
                                j:=j+1;

//36
    SignalData.Signal.Current.Faza_A.Harmonics[j].Koef := arr^[i];i:=i+1;//5.5
    SignalData.Signal.Current.Faza_A.Harmonics[j].Angle := arr^[i];i:=i+1;//10
    SignalData.Signal.Current.Faza_B.Harmonics[j].Koef := arr^[i];i:=i+1;//4.5
    SignalData.Signal.Current.Faza_B.Harmonics[j].Angle := arr^[i];i:=i+1;//15
    SignalData.Signal.Current.Faza_C.Harmonics[j].Koef := arr^[i];i:=i+1;//4
    SignalData.Signal.Current.Faza_C.Harmonics[j].Angle := arr^[i];i:=i+1;//20
                                j:=j+1;

//37
    SignalData.Signal.Current.Faza_A.Harmonics[j].Koef := arr^[i];i:=i+1;//5.5
    SignalData.Signal.Current.Faza_A.Harmonics[j].Angle := arr^[i];i:=i+1;//10
    SignalData.Signal.Current.Faza_B.Harmonics[j].Koef := arr^[i];i:=i+1;//4.5
    SignalData.Signal.Current.Faza_B.Harmonics[j].Angle := arr^[i];i:=i+1;//15
    SignalData.Signal.Current.Faza_C.Harmonics[j].Koef := arr^[i];i:=i+1;//4
    SignalData.Signal.Current.Faza_C.Harmonics[j].Angle := arr^[i];i:=i+1;//20
                                j:=j+1;

//38
    SignalData.Signal.Current.Faza_A.Harmonics[j].Koef := arr^[i];i:=i+1;//5.5
    SignalData.Signal.Current.Faza_A.Harmonics[j].Angle := arr^[i];i:=i+1;//10
    SignalData.Signal.Current.Faza_B.Harmonics[j].Koef := arr^[i];i:=i+1;//4.5
    SignalData.Signal.Current.Faza_B.Harmonics[j].Angle := arr^[i];i:=i+1;//15
    SignalData.Signal.Current.Faza_C.Harmonics[j].Koef := arr^[i];i:=i+1;//4
    SignalData.Signal.Current.Faza_C.Harmonics[j].Angle := arr^[i];i:=i+1;//20
                                j:=j+1;

//39
    SignalData.Signal.Current.Faza_A.Harmonics[j].Koef := arr^[i];i:=i+1;//5.5
    SignalData.Signal.Current.Faza_A.Harmonics[j].Angle := arr^[i];i:=i+1;//10
    SignalData.Signal.Current.Faza_B.Harmonics[j].Koef := arr^[i];i:=i+1;//4.5
    SignalData.Signal.Current.Faza_B.Harmonics[j].Angle := arr^[i];i:=i+1;//15
    SignalData.Signal.Current.Faza_C.Harmonics[j].Koef := arr^[i];i:=i+1;//4
    SignalData.Signal.Current.Faza_C.Harmonics[j].Angle := arr^[i];i:=i+1;//20
                                j:=j+1;

//40 ���������
    SignalData.Signal.Current.Faza_A.Harmonics[j].Koef := arr^[i];i:=i+1;//5.5
    SignalData.Signal.Current.Faza_A.Harmonics[j].Angle := arr^[i];i:=i+1;//10
    SignalData.Signal.Current.Faza_B.Harmonics[j].Koef := arr^[i];i:=i+1;//4.5
    SignalData.Signal.Current.Faza_B.Harmonics[j].Angle := arr^[i];i:=i+1;//15
    SignalData.Signal.Current.Faza_C.Harmonics[j].Koef := arr^[i];i:=i+1;//4
    SignalData.Signal.Current.Faza_C.Harmonics[j].Angle := arr^[i];i:=i+1;//20

    SignalData.CollapsOvervolt.Amount := trunc(arr^[i]);i:=i+1;//5        ;
    SignalData.CollapsOvervolt.Duration := arr^[i];i:=i+1;//1.5   ;
    SignalData.CollapsOvervolt.RepeatPeriod := arr^[i];i:=i+1;//2;
    SignalData.CollapsOvervolt.InitialFaza := arr^[i];i:=i+1;//0;
    SignalData.CollapsOvervolt.ReferenceVoltage := trunc(arr^[i]);i:=i+1;//CONST_DEVIATIONS_REFERENCE_VOLTAGE_NOMINAL;
    SignalData.CollapsOvervolt.Linking := trunc(arr^[i]);i:=i+1;//CONST_DEVIATIONS_LINKING_PERIOD;
    SignalData.CollapsOvervolt.Faza_A.IsCollapse := (arr^[i]>0.0);i:=i+1;//True;
    SignalData.CollapsOvervolt.Faza_A.Value := arr^[i];i:=i+1;//20;
    SignalData.CollapsOvervolt.Faza_B.IsCollapse := (arr^[i]>0.0);i:=i+1;//False;
    SignalData.CollapsOvervolt.Faza_B.Value := arr^[i];i:=i+1;//1.1;
    SignalData.CollapsOvervolt.Faza_C.IsCollapse := (arr^[i]>0.0);i:=i+1;//True ;
    SignalData.CollapsOvervolt.Faza_C.Value := arr^[i];i:=i+1;//-1;

//    SignalData.CollapsOvervolt := 0; //VarPtr(Deviations)
    SignalData.VoltageOscillations.Frequency := arr^[i];i:=i+1;//         ;
    SignalData.VoltageOscillations.Faza_A.Swing := arr^[i];i:=i+1;//      ;
    SignalData.VoltageOscillations.Faza_B.Swing := arr^[i];i:=i+1;//
    SignalData.VoltageOscillations.Faza_C.Swing := arr^[i];i:=i+1;//

    result:= true;

end;
 ////////////////////////////
// ��� ������� ����� ���������� ������� ����������
///////////////////////////////
//����� �������� ������� � ���� outtext.txt;  ����� - ����� ���������
 function LV_Arr_Test(arr:TPArr;Count: Word):Word;  stdcall;
var i: integer;
    d: Double;
t:text;
S: String;
      begin
      d:=0;
S:=GetCurrentDir();
S:= S+ '\output.txt';
      assign(t,S);  rewrite(t);
      for i := 0 to Count-1 do
          begin
        d :=  d+arr^[i];//d + arr^[i];
        writeln(t,i:5);
        writeln(t,arr^[i]:6:3);
        end;
   close(t);
        result := trunc(d);
      end;
//�������� ������� �� ��������� ����� ���������� �������; ����� - ��.������
function LV_CheckSignalData_Default(): Word;  stdcall;
     begin
     if (F2()=true) then
  result := CheckSignalData(@SignalData)
  else
result := 3000;
    end;
// �������� ������ ����������; ����� - ��.������
function LV_GetDllVersion_Major(): Word;      stdcall;
var Ver1,Ver2 : Word;
begin
  GetDllVersion(Ver1,Ver2);
  result:= Ver1;
end;
//�������� ������ ����������; ����� - ��.������
function LV_GetDllVersion_Minor(): Word;       stdcall;
var Ver1,Ver2 : Word;
begin
  GetDllVersion(Ver1,Ver2);
  result:= Ver2;
end;
// ��������� ������������ �������� ��������; ����� - ��.������
function LV_SetSignal(PortNumber: Word; arr:TPArr; Count: Word): Word;  stdcall;
begin
ComPortNumber1:=PortNumber;
if (In_Signal(arr,Count))   then
result := SetSignal(PortNumber, @SignalData, nil, @ServiceInfo)
else
result := 3000;
end;
// ���������� ������ ������������ ��������/��������������; ����� - ��.������
function LV_StopCollapsesOvervoltages(PortNumber: Word): Word;stdcall;
begin
ComPortNumber1 :=   PortNumber;
result:= StopCollapsesOvervoltages(PortNumber);
end;
// ���������� ������ ������� ������������; ����� - ��.������
function LV_StopSignal(PortNumber: Word): Word;  stdcall;
begin
ComPortNumber1 :=   PortNumber;
result:= StopSignal(PortNumber);
end;
// �������� ����������� ������ � ���-������; ����� - ��.������
function LV_CheckPort(PortNumber: Word): Word;   stdcall;
begin
ComPortNumber1 :=   PortNumber;
result:= CheckPort(PortNumber);
end;
// �������� ������������ ����������� �������������
///���������� �������� �������� �����������; ����� - ��.������
function LV_CheckSignalData(arr:TPArr;Count: Word): Word;  stdcall;
begin
if (In_Signal(arr,Count))   then
result := CheckSignalData(@SignalData)
else
result := 3000;
end;

exports
  LV_Arr_Test,LV_CheckSignalData_Default,
  LV_GetDllVersion_Major,
  LV_GetDllVersion_Minor,LV_SetSignal,LV_StopCollapsesOvervoltages,LV_StopSignal, LV_CheckPort,
  LV_CheckSignalData;


begin
//����� ������� ������ ���������� ������������������� ����������
//��������� ����������, � � ���������� ������������ �� ��� ������ ������
//������� SetSignal:
 ServiceInfo := CONST_SERVICEINFO_INITIALVALUE; ComPortNumber1:=1;
end.

