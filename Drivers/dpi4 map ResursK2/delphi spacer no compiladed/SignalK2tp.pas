{ *********************************************************************** }
{                                                                         }
{    ���������� ���������� ������������ SignalK2.dll                      }
{    ������ � ��������� ������������ ����� ������ � ��������              }
{                                                                         }
{ *********************************************************************** }

unit SignalK2tp;

interface

//==============================================================================
// ��������� ����������
//==============================================================================
type
  PServiceInfo = ^TServiceInfo;
  TServiceInfo = Cardinal;

const
  CONST_SERVICEINFO_INITIALVALUE = 0;     // �������� ���������� ���� ��� �������������� ������ �������
  CONST_SERVICEINFO_UNASSIGNED = nil;     // ��������� ��� ����������� �������������������� ��������� ������

//==============================================================================
// ��������� ��� ���������� �������
//==============================================================================
const
  { �������� }
  CONST_NOMINAL_U_57V   = 1;                          // �������� �������������� ��� �����. ���������� 57,7�
  CONST_NOMINAL_U_220V  = 2;                          // �������� �������������� ��� �����. ���������� 220�
  CONST_NOMINAL_I_1A    = 3;                          // �������� �������������� ��� �����. ���� 1�
  CONST_NOMINAL_I_5A    = 4;                          // �������� �������������� ��� �����. ���� 5�
  CONST_NOMINAL_FREQ    = 50;                         // ����������� �������� �������

  { ������� }
  CONST_FREQ_MIN = 45;                                // ����������� �������� �������
  CONST_FREQ_MAX = 65;                                // ������������ �������� �������

  { ��������� }
  CONST_U_MAINFREQVOLT_ANGLE_A_VALUE = 0;             // �������� �������� ���� ���������� �������� ������� ���� �
  CONST_U_HARMONIC_COEFFICIENT_MIN = 0;               // ����������� �������� ������������ ��������� ����������
  CONST_U_HARMONIC_COEFFICIENT_MAX = 80;              // ������������ �������� ������������ ��������� ����������
  CONST_I_HARMONIC_COEFFICIENT_MIN = 0;               // ����������� �������� ������������ ��������� ����
  CONST_I_HARMONIC_COEFFICIENT_MAX = 100;             // ������������ �������� ������������ ��������� ����

  { ������������� ������������ }
  CONST_KALIB_KOEF_VALUE_MIN = -100;                  // ����������� �������� �������������� ������������
  CONST_KALIB_KOEF_VALUE_MAX = 100;                   // ������������ �������� �������������� ������������

  { �������/�������������� }
  CONST_COLLAPSE_VALUE_NO    = 0;                     // ������� ����, ��� ������� ���
  CONST_COLLAPSE_VALUE_MIN   = 0;                     // ����������� �������� ������� �������
  CONST_COLLAPSE_VALUE_MAX   = 100;                   // ������������ �������� ������� �������
  CONST_OVERVOLTAGE_KOEF_NO  = 0;                     // ������� ����, ��� �������������� ���
  CONST_OVERVOLTAGE_KOEF_MIN = 1;                     // ����������� �������� ������������ ��������������
  CONST_OVERVOLTAGE_KOEF_MAX = 2;                     // ������������ �������� ������������ ��������������

  { ������������ ��������/�������������� }
  CONST_DEVIATIONS_AMOUNT_MIN = 1;                    // ����������� ���-�� ��������/��������������
  CONST_DEVIATIONS_AMOUNT_MAX = High(Word) div 2;     // ������������ ���-�� ��������/��������������
  CONST_DEVIATIONS_DURATION_MIN = 0;                  // ����������� �������� ������������ ��������/��������.
  CONST_DEVIATIONS_DURATION_MAX = 600;                // ������������ �������� ������������ ��������/��������.
  CONST_DEVIATIONS_REPEAT_PERIOD_MIN = 0;             // ����������� �������� ������� ���������� ��������/��������.
  CONST_DEVIATIONS_REPEAT_PERIOD_MAX = 600;           // ������������ �������� ������� ���������� ��������/��������.
  CONST_DEVIATIONS_REFERENCE_VOLTAGE_NOMINAL = 1;     // ������� ���������� - �����������
  CONST_DEVIATIONS_REFERENCE_VOLTAGE_EFFECTIVE = 2;   // ������� ���������� - ����������� �� ����
  CONST_DEVIATIONS_LINKING_NONE = 1;                  // �������� - ���
  CONST_DEVIATIONS_LINKING_HALFPERIOD = 2;            // �������� - � �����������
  CONST_DEVIATIONS_LINKING_PERIOD = 3;                // �������� - � �������

  { ������������ ��������� }
  CONST_OSCILLATIONS_FREQUENCY_MIN = 1 / 120;         // ����������� ������� ���������, ��
  CONST_OSCILLATIONS_FREQUENCY_MAX = 24;              // ������������ ������� ���������, ��
  CONST_OSCILLATIONS_SWING_MIN = 0;                   // ����������� ������ ��������� ����������, %
  CONST_OSCILLATIONS_SWING_MAX = 80;                  // ������������ ������ ��������� ����������, %

//==============================================================================
// ��������� ������
//==============================================================================
//------------------------------------------------------------------------------
// ����� ����� ������
//------------------------------------------------------------------------------
const
  ER_NO = 0;                                 // ��� ������
  ER_EXPORT_FUNC_EXCEPTION = 10;             // ������ ������������� ���������� �� ����� ���������� �������. �������

//------------------------------------------------------------------------------
// ������� ����������� COM-�����
//------------------------------------------------------------------------------
const
  ER_PORT_BUSY       = 51;                   // ���� �����
  ER_PORT_NO         = 52;                   // ��� ������ �����
  ER_PORT_BAD_HANDLE = 53;                   // ���������������� ��������� �� ����
  ER_PORT_SETTINGS   = 54;                   // �� ������� ��������� ��������� �����
  ER_PORT_DISCONNECT = 55;                   // ���������� ����������� �� �����

//------------------------------------------------------------------------------
// ������, ����������� ��� ���������� ������� ������ � ������������
//------------------------------------------------------------------------------
const
  { ���� ������ ��� ������ �� ���������� }
  ER_INTERFACE_NO_ANSWER = 101;              // ��� ������ �� �����������
  ER_INTERFACE_WRITE     = 102;              // ������ ������ � ���� / ������� �� ��� ������
  ER_INTERFACE_READ      = 103;              // ������ ������ �� ����� / ��������� �� ��� ������

  { ������ ��� ������� ������� � �������������� �� ����������� }
  ER_COM_RECEIVE_CONFIRM_NO         = 104;   // ���������� ����������� ���������� ��������� ������� (������ �� 8 ����)
  ER_COM_RECEIVE_CONFIRM_SYNC_BYTES = 105;   // ������������ � ������ ������������� ��� ���������. ���������
  ER_COM_RECEIVE_CONFIRM_COMM_CODE  = 106;   // ������������ � ���� ������� ��� ���������. ���������
  ER_COM_EXECUTE_CONFIRM_NO         = 107;   // ���������� ����������� ���������� ���������� ������� (������ �� 8 ����)
  ER_COM_EXECUTE_CONFIRM_SYNC_BYTES = 108;   // ������������ � ������ ������������� ��� ���������. ����������
  ER_COM_EXECUTE_CONFIRM_COMM_CODE  = 109;   // ������������ � ���� ������� ��� ���������. ����������

//------------------------------------------------------------------------------
// ������ �������������� ����������� ������
//------------------------------------------------------------------------------
const
  { ������ ��� ���������� ���������� �� ��������� � ��������� ������� }
  ER_SIGNAL_FULL_POINTER_NO = 151;           // ��� ��������� �� ��������� ������ ���������� �������� ��������, ����������� �����������
  ER_SIGNAL_USER_POINTER_NO = 152;           // ��� ��������� �� ��������� ���������� �������� ��������, ����������� �������������

  { ������ �� ������������� ������������� }
  ER_KALIB_KOEF_U_A = 201;                   // �������� ������������� ����������� ���������� ���� A (0..1%)
  ER_KALIB_KOEF_U_B = 202;                   // �������� ������������� ����������� ���������� ���� B (0..1%)
  ER_KALIB_KOEF_U_C = 203;                   // �������� ������������� ����������� ���������� ���� C (0..1%)
  ER_KALIB_KOEF_I_A = 204;                   // �������� ������������� ����������� ���� ���� A (0..1%)
  ER_KALIB_KOEF_I_B = 205;                   // �������� ������������� ����������� ���� ���� B (0..1%)
  ER_KALIB_KOEF_I_C = 206;                   // �������� ������������� ����������� ���� ���� C (0..1%)
  ER_KALIB_KOEF_F   = 207;                   // �������� ������������� ����������� ������� (0..1%)

  { ������ �� ��������� }
  ER_NOMINAL_U = 301;                        // �������� �������� ����������
  ER_NOMINAL_I = 302;                        // �������� �������� ����

  { ������� }
  ER_FREQ = 410;                             // �������� �������� ������� (�.�. � 45..65 ��)

  { ������ �� ���������� �������� ���������� � ���� }
  ER_VOLTAGE_MAINFREQ_ANGLE_A = 501;         // ������� ���� ���������� �������� ������� ���� A <> 0
  ER_VOLTAGE_HARMONIC_A = 601;               // �������� �������� ������������ ��������� ���������� ���� � (0..80 %)
  ER_VOLTAGE_HARMONIC_B = 701;               // �������� �������� ������������ ��������� ���������� ���� � (0..80 %)
  ER_VOLTAGE_HARMONIC_C = 801;               // �������� �������� ������������ ��������� ���������� ���� � (0..80 %)
  ER_CURRENT_HARMONIC_A = 901;               // �������� �������� ������������ ��������� ���� ���� � (0..100 %)
  ER_CURRENT_HARMONIC_B = 1001;              // �������� �������� ������������ ��������� ���� ���� � (0..100 %)
  ER_CURRENT_HARMONIC_C = 1101;              // �������� �������� ������������ ��������� ���� ���� � (0..100 %)

  { ������ ������ �������������� ���������� }
  ER_RELATIONS_DEVIATIONS_OSCILLATIONS_TOGETHER = 1801; // ������ ������������ ������ � �������/�������������� � ���������

  { ������ �� ��������/��������������� }
  ER_DEVIATIONS_AMOUNT            = 1901;    // ���-�� ��������/��������. ����� �� ���������� ��������
  ER_DEVIATIONS_DURATION          = 1902;    // ������������ ��./���. ����� �� ���������� ��������
  ER_DEVIATIONS_REP_PERIOD        = 1903;    // ������� ���������� ��./���. ����� �� ���. ����.
  ER_DEVIATIONS_REFERENCE_VOLTAGE = 1904;    // �������� ������� ����������
  ER_DEVIATIONS_LINKING           = 1905;    // �������� ��������
  ER_DEVIATIONS_NO_VOLTAGE        = 1906;    // ������� ������ �������/�������������� ��� ����������� ����������
  ER_DEVIATIONS_FIRST_HARMONICS   = 1907;    // ��������� ��������/�������������� �������� ������ ��� ��������� ���������� �������� ������� ���� ��� � ������
  ER_COLLAPSE_FAZA_A_VALUE        = 2001;    // ����� �� ������� ����������� ��������� ������� ������� �� ���� �
  ER_OVERVOLT_FAZA_A_VALUE        = 2002;    // ����� �� ������� ����������� ��������� ����. ��������. �� ���� �
  ER_COLLAPSE_FAZA_B_VALUE        = 2003;    // ����� �� ������� ����������� ��������� ������� ������� �� ���� �
  ER_OVERVOLT_FAZA_B_VALUE        = 2004;    // ����� �� ������� ����������� ��������� ����. ��������. �� ���� �
  ER_COLLAPSE_FAZA_C_VALUE        = 2005;    // ����� �� ������� ����������� ��������� ������� ������� �� ���� �
  ER_OVERVOLT_FAZA_C_VALUE        = 2006;    // ����� �� ������� ����������� ��������� ����. ��������. �� ���� �

  { ������ �� ���������� }
  ER_OSCILLATIONS_FREQUENCY       = 2101;    // ����� �� ������� ����������� ��������� ������� ���������� ���������
  ER_OSCILLATIONS_NO_VOLTAGE      = 2102;    // ������� ������ ��������� ��� ����������� ����������
  ER_OSCILLATIONS_FIRST_HARMONICS = 2103;    // ��������� ��������� �������� ������ ��� ��������� ���������� �������� ������� ���� ��� � ������
  ER_OSCILLATIONS_FAZA_A_SWING    = 2201;    // ����� �� ������� ����������� ��������� ������� ��������� �� ���� �
  ER_OSCILLATIONS_FAZA_B_SWING    = 2202;    // ����� �� ������� ����������� ��������� ������� ��������� �� ���� �
  ER_OSCILLATIONS_FAZA_C_SWING    = 2203;    // ����� �� ������� ����������� ��������� ������� ��������� �� ���� �

//------------------------------------------------------------------------------
// ������ ��� ������������ �������� ���������� ��������
//------------------------------------------------------------------------------
const
  { ���� ������ ���������� ��������� ������� ���������� �������� }
  ER_AMPLITUDE_U_A   = 2301;                 // ���������� ��������� ���������� ���� �
  ER_AMPLITUDE_U_B   = 2302;                 // ���������� ��������� ���������� ���� �
  ER_AMPLITUDE_U_C   = 2303;                 // ���������� ��������� ���������� ���� �
  ER_AMPLITUDE_U_AB  = 2304;                 // ���������� ��������� ���������� ��� �,�
  ER_AMPLITUDE_U_BC  = 2305;                 // ���������� ��������� ���������� ��� �,�
  ER_AMPLITUDE_U_AC  = 2306;                 // ���������� ��������� ���������� ��� �,�
  ER_AMPLITUDE_U_ABC = 2307;                 // ���������� ��������� ���������� ��� �,�,�
  ER_AMPLITUDE_I_A   = 2401;                 // ���������� ��������� ���� ���� �
  ER_AMPLITUDE_I_B   = 2402;                 // ���������� ��������� ���� ���� �
  ER_AMPLITUDE_I_C   = 2403;                 // ���������� ��������� ���� ���� �
  ER_AMPLITUDE_I_AB  = 2404;                 // ���������� ��������� ���� ��� �,�
  ER_AMPLITUDE_I_BC  = 2405;                 // ���������� ��������� ���� ��� �,�
  ER_AMPLITUDE_I_AC  = 2406;                 // ���������� ��������� ���� ��� �,�
  ER_AMPLITUDE_I_ABC = 2407;                 // ���������� ��������� ���� ��� �,�,�
  ER_AMPLITUDE_DEVIATIONS_A   = 2501;        // ���������� ��������� ���������� ���� � ��� ��������/���������������
  ER_AMPLITUDE_DEVIATIONS_B   = 2502;        // ���������� ��������� ���������� ���� � ��� ��������/���������������
  ER_AMPLITUDE_DEVIATIONS_C   = 2503;        // ���������� ��������� ���������� ���� � ��� ��������/���������������
  ER_AMPLITUDE_DEVIATIONS_AB  = 2504;        // ���������� ��������� ���������� ��� �,� ��� ��������/���������������
  ER_AMPLITUDE_DEVIATIONS_BC  = 2505;        // ���������� ��������� ���������� ��� �,� ��� ��������/���������������
  ER_AMPLITUDE_DEVIATIONS_AC  = 2506;        // ���������� ��������� ���������� ��� �,� ��� ��������/���������������
  ER_AMPLITUDE_DEVIATIONS_ABC = 2507;        // ���������� ��������� ���������� ��� �,�,� ��� ��������/���������������
  ER_AMPLITUDE_OSCILLATIONS_A   = 2601;      // ���������� ��������� ���������� ���� � ��� ����������
  ER_AMPLITUDE_OSCILLATIONS_B   = 2602;      // ���������� ��������� ���������� ���� � ��� ����������
  ER_AMPLITUDE_OSCILLATIONS_C   = 2603;      // ���������� ��������� ���������� ���� � ��� ����������
  ER_AMPLITUDE_OSCILLATIONS_AB  = 2604;      // ���������� ��������� ���������� ��� �,� ��� ����������
  ER_AMPLITUDE_OSCILLATIONS_BC  = 2605;      // ���������� ��������� ���������� ��� �,� ��� ����������
  ER_AMPLITUDE_OSCILLATIONS_AC  = 2606;      // ���������� ��������� ���������� ��� �,� ��� ����������
  ER_AMPLITUDE_OSCILLATIONS_ABC = 2607;      // ���������� ��������� ���������� ��� �,�,� ��� ����������
  
  { ������ �� ���������� ��� ������������ �������� �����. �������� }
  ER_COMPUTATION_DISCR_VALUES_U            = 2701;  // ������ ��� ����. ������� �����. �������� ����������
  ER_COMPUTATION_DISCR_VALUES_I            = 2702;  // ������ ��� ����. ������� �����. �������� ����
  ER_COMPUTATION_DISCR_VALUES_DEVIATIONS   = 2703;  // ������ ��� ����. ������� �����. �������� ���������� �������/��������������
  ER_COMPUTATION_DISCR_VALUES_OSCILLATIONS = 2704;  // ������ ��� ����. ������� �����. �������� ��������� ����������

//------------------------------------------------------------------------------
// ������ ��� ������� ������ ���������� ��������, ����������� ������������
//------------------------------------------------------------------------------
const
  ER_CALC_ANGLES       = 2801;               // ��� ������� �����
  ER_CALC_CURRENT      = 2802;               // ��� ������� ����
  ER_CALC_DEVIATIONS   = 2803;               // ��� ������� ��������/��������������
  ER_CALC_FREQUENCY    = 2804;               // ��� ������� �������
  ER_CALC_ENERGY       = 2805;               // ��� ������� �������
  ER_CALC_OSCILLATIONS = 2806;               // ��� ������� ���������
  ER_CALC_POWER        = 2807;               // ��� ������� ��������
  ER_CALC_VOLTAGE      = 2808;               // ��� ������� ����������

//==============================================================================
// ���� ������
//==============================================================================

//------------------------------------------------------------------------------
// ������ �� ����� ���������
//------------------------------------------------------------------------------
type
  { ������ �� ����� ��������� }
  TOneHarmonic = record
    Koef: Single;                   // �����������
    Angle: Single;                  // ����
  end;

  THarmonicsArray = array[2..40] of TOneHarmonic;
  THarmonicsValues = array[2..40] of Single;

//------------------------------------------------------------------------------
// ������ ��� ������� ������� � �����������
//------------------------------------------------------------------------------
type
  { ���������� �� ����� ���� ����������/���� }
  TVoltageCurrentOneFaza = record
    KalibKoef: Single;                     // ������������� ����������� �� ���� ����������/����
    IncludeMainFreq: Boolean;              // ���� ��������� ����������/���� �������� ������� ����
    MainFreqVoltCur: TOneHarmonic;         // ������ ����������/��� �������� �������
    Harmonics: THarmonicsArray;            // ��������� ����
  end;

  { ������������ ���������� �� ����������/���� }
  TVoltageCurrent = record
    Enable: Boolean;                // ���./����. ����� ����������/����
    NominalID: Byte;                // ������������� �������� (����������: [1 (57,7�), 2 (220�)],
                                    //                         ���: [3 (1�), 4 (5�)] )
    Faza_A: TVoltageCurrentOneFaza; // ���������� �� ���� �
    Faza_B: TVoltageCurrentOneFaza; // ���������� �� ���� �
    Faza_C: TVoltageCurrentOneFaza; // ���������� �� ���� �
  end;

  { ������������ ���������� �� ������� }
  TFrequency = record
    KalibKoef: Single;                // ������������� �����������
    Value: Single;                    // �������� �������
  end;

  { ��� ������������ ����������, ����������� ��� �������� ������������ ������� }
  PSignal = ^TSignal;
  TSignal = record
    Voltage: TVoltageCurrent;       // ����������
    Current: TVoltageCurrent;       // ���
    Frequency: TFrequency;          // �������
  end;

  { ���������� �� ��������/��������������� ����� ���� }
  TOneFazaCollapseOvervoltage = record
    IsCollapse: Boolean;              // True - ������, False - ��������������
    Value: Single;                    // ��������
  end;

  { ���������� �� ��������/��������������� ���� ��� }
  PCollapsesOvervoltages = ^TCollapsesOvervoltages;
  TCollapsesOvervoltages = record
    Amount: Word;                           // ����������
    Duration: Single;                       // ������������
    RepeatPeriod: Single;                   // ������ ����������
    InitialFaza: Single;                    // ��������� ����
    ReferenceVoltage: Byte;                 // ������� ����������
    Linking: Byte;                          // ��������
    Faza_A: TOneFazaCollapseOvervoltage;    // �������/�������������� ���� �
    Faza_B: TOneFazaCollapseOvervoltage;    // �������/�������������� ���� �
    Faza_C: TOneFazaCollapseOvervoltage;    // �������/�������������� ���� �
  end;

  { ���������� �� ���������� ����� ���� }
  TOneFazaOscillations = record
    Swing: Single;                          // ������ ����������
  end;

  { ���������� �� ���������� ���������� }
  PVoltageOscillations = ^TVoltageOscillations;
  TVoltageOscillations = record
    Frequency: Single;                    // ������� ���������, ��
    Faza_A: TOneFazaOscillations;         // ��������� ���� �
    Faza_B: TOneFazaOscillations;         // ��������� ���� �
    Faza_C: TOneFazaOscillations;         // ��������� ���� �
  end;

  { ��� ��������� ������ ��� ������ ������������
   (��������� ������� � ��������/��������������) }
  PUserSignalData = ^TUserSignalData;
  TUserSignalData = record
    Signal: TSignal;                            // ��������� �������
    CollapsOvervolt: PCollapsesOvervoltages;    // �������/��������������
    VoltageOscillations: PVoltageOscillations;  // ��������� ����������
  end;

//------------------------------------------------------------------------------
// ������ ������ �� �������
//------------------------------------------------------------------------------
type
  { ��������� ������ �� ������� }
  TCalcFrequency = record
    KalibKoef: Single;              // ������������� �����������
    Value: Single;                  // �������� �������, ��
    Delta: Single;                  // ���������� ������� �� ������������ ��������, ��
  end;

  { ��������� ������������� ������������ (��� ����������/����) }
  TCalcKalibKoeffs = record
    Faza_A: Single;                 // ���� � (����������/����)
    Faza_B: Single;                 // ���� � (����������/����)
    Faza_C: Single;                 // ���� � (����������/����)
  end;

  { ��������� ������ �� ����� ���� }
  TCalcAnglesFaza = THarmonicsValues;

  { ��������� ���� �/� ����������� ���������� � ����� }
  TCalcAngles = record
    U_A_B: TCalcAnglesFaza;       // ���� �/� n-�� �������������� ������������� ���������� ��� � � �
    U_B_C: TCalcAnglesFaza;       // ���� �/� n-�� �������������� ������������� ���������� ��� � � �
    U_C_A: TCalcAnglesFaza;       // ���� �/� n-�� �������������� ������������� ���������� ��� � � �
    U_AB_BC: TCalcAnglesFaza;     // ���� �/� n-�� �������������� ������������� ���������� ��� �� � ��
    U_BC_CA: TCalcAnglesFaza;     // ���� �/� n-�� �������������� ������������� ���������� ��� �� � ��
    U_CA_AB: TCalcAnglesFaza;     // ���� �/� n-�� �������������� ������������� ���������� ��� �� � ��
    I_A_B: TCalcAnglesFaza;       // ���� �/� n-�� �������������� ������������� ����� ��� � � �
    I_B_C: TCalcAnglesFaza;       // ���� �/� n-�� �������������� ������������� ����� ��� � � �
    I_C_A: TCalcAnglesFaza;       // ���� �/� n-�� �������������� ������������� ����� ��� � � �
  end;

  { ��������� ������ �� ����� ���� ���������� }
  TCalcVoltageFaza = record
    MainFreqVolt: Single;           // ����������� �������� ���������� �������� �������, �
    DeltaMainFreqVolt: Single;      // ���������� ���������� �������� ������� �� ������������ �������� ����������, %
    AngleMainFreqVolt: Single;      // ������� ���� ���������� �������� �������, �
    AngleBetweenFazes: Single;      // ���� ����� ������������ �������� �������, � (�-�, �-�, �-�, ��-��, ��-��, ��-��)
    EffectiveValue: Single;         // ����������� �������� ���������� ����, �
    AsinusoidalKoefVolt: Single;    // ����������� ������������������ ����������, %
    Harmonics: THarmonicsArray;     // �������������� n-�� ������������� ������������ ������� (������������) ����������
  end;

  { ��������� ������ �� ����������� ���������� }
  TCalcVoltageAsymmetry = record
    U0_f: Single;                   // ���������� ������� ������������������ ������� ������ ����������, �
    U1_f: Single;                   // ���������� ������ ������������������ ������� ������ ����������, �
    U2_f: Single;                   // ���������� �������� ������������������ ������� ������ ����������, �
    U0_mf: Single;                  // ���������� ������� ������������������ ������� ����������� ����������, �
    U1_mf: Single;                  // ���������� ������ ������������������ ������� ����������� ����������, �
    U2_mf: Single;                  // ���������� �������� ������������������ ������� ����������� ����������, �
    K_0: Single;                    // ����������� ����������� ���������� �� ������� ������������������, %
    K_2: Single;                    // ����������� ����������� ���������� �� �������� ������������������, %
  end;

  { ��������� ������ �� ����� ���������� }
  TCalcVoltage = record
    NominalID: Byte;                        // ������������� ��������
    Faza_A: TCalcVoltageFaza;               // ���������� �� ���� �
    Faza_B: TCalcVoltageFaza;               // ���������� �� ���� �
    Faza_C: TCalcVoltageFaza;               // ���������� �� ���� �
    Faza_AB: TCalcVoltageFaza;              // ���������� �� ���� ��
    Faza_BC: TCalcVoltageFaza;              // ���������� �� ���� ��
    Faza_CA: TCalcVoltageFaza;              // ���������� �� ���� ��
    Asymmetry: TCalcVoltageAsymmetry;       // ����������� ����������
    KalibKoeffs: TCalcKalibKoeffs           // ������������� ������������
  end;

  { ��������� ������ �� ����� ���� ���� }
  TCalcCurrentFaza = record
    MainFreqCur: Single;                      // ����������� �������� ���� �������� �������, �
    DeltaMainFreqCur: Single;                 // ���������� ���� �������� ������� �� �������� ����, %
    AngleBetweenCurVolt: Single;              // �������� �������� ���� ����� ����� � �����������, �
    AngleMainFreqCur: Single;                 // ������� ���� ���� �������� �������, �
    AngleBetweenFazes: Single;                // ���� ����� ������ �������� �������, � (�-�, �-�, �-�)
    EffectiveValue: Single;                   // ����������� �������� ���� ����, �
    AsinusoidalKoefCur: Single;               // ����������� ��������� ���������������� ����, %
    Harmonics: THarmonicsArray;               // �������������� n-�� ������������� ������������ ������� ����
  end;

  { ��������� ����������� ���� }
  TCalcCurrentAsymmetry = record
    I0: Single;                               // �������� ���� ���� ������� ������������������, �
    I0_angle: Single;                         // �������� �������� ���� ���� ������� ������������������, �
    I1: Single;                               // �������� ���� ���� ������ ������������������, �
    I1_angle: Single;                         // �������� �������� ���� ���� ������ ������������������, �
    I2: Single;                               // �������� ���� ���� �������� ������������������, �
    I2_angle: Single;                         // �������� �������� ���� ���� �������� ������������������, �
  end;

  { ��������� ������ �� ����� ���� }
  TCalcCurrent = record
    NominalID: Byte;                        // ������������� ��������
    Faza_A: TCalcCurrentFaza;               // ���������� �� ���� �
    Faza_B: TCalcCurrentFaza;               // ���������� �� ���� �
    Faza_C: TCalcCurrentFaza;               // ���������� �� ���� �
    NeutralCurrent: Single;                 // ������ ��� ��������
    Asymmetry: TCalcCurrentAsymmetry;       // ����������� ����
    KalibKoeffs: TCalcKalibKoeffs           // ������������� ������������
  end;

  { ��������� ������ �� ������ ���� �������� �������� }
  TCalcPowerOneType = record
    MainFreqPow: Single;                    // �������� �������� �������
    HarmonicsPow: THarmonicsValues;         // �������� �������� (2..50)
    FullSignalPow_1: Single;                // �������� ����� ������� (��� 1)
    FullSignalPow_2: Single;                // �������� ����� ������� (��� 2)
  end;

  { ��������� ������ �� ����� ���� �������� }
  TCalcPowerFaza = record
    KoefPower: Single;                      // ����������� ��������
    ActivePower: TCalcPowerOneType;         // �������� �������� (�����������: ��)
    ReactivePower: TCalcPowerOneType;       // ���������� �������� (�����������: ���)
    FullPower: TCalcPowerOneType;           // ������ �������� (�����������: ��)
  end;

  { ��������� ������ �� ������������ �������� }
  TCalcPowerSummary = record
    MainFreq: Single;                         // ��������� �������� �������� �������
    FullSignal_1: Single;                     // ��������� �������� ����� ������� (��� 1)
    FullSignal_2: Single;                     // ��������� �������� ����� ������� (��� 2)
  end;

  { ��������� ������ �� ����������� �������� }
  TCalcPowerAsymmetry = record
    P0: Single;                               // �������� �������� ������� ������������������, ��
    P1: Single;                               // �������� �������� ������ ������������������, ��
    P2: Single;                               // �������� �������� �������� ������������������, ��
    S0: Single;                               // ������ �������� ������� ������������������, ��
    S1: Single;                               // ������ �������� ������ ������������������, ��
    S2: Single;                               // ������ �������� �������� ������������������, ��
    Q0: Single;                               // ���������� �������� ������� ������������������, ���
    Q1: Single;                               // ���������� �������� ������ ������������������, ���
    Q2: Single;                               // ���������� �������� �������� ������������������, ���
  end;

  { ��������� ������ �� ���� �������� }
  TCalcPower = record
    Faza_A: TCalcPowerFaza;                 // ���������� �� ���� �
    Faza_B: TCalcPowerFaza;                 // ���������� �� ���� �
    Faza_C: TCalcPowerFaza;                 // ���������� �� ���� �
    SumActivePower: TCalcPowerSummary;      // ��������� �������� �������
    SumReactivePower: TCalcPowerSummary;    // ��������� ���������� �������
    SumFullPower: TCalcPowerSummary;        // ��������� ������ �������
    Asymmetry: TCalcPowerAsymmetry;         // ����������� ��������
  end;

  { ��������� ������ �� ������� }
  TCalcEnergy = record
    EnergyOneSecond: Single;                // ������� �� 1 ���
  end;

  { ��������� ������ ������� }
  TCalcSignalData = record
    Frequency: TCalcFrequency;              // �������
    Voltage: TCalcVoltage;                  // ����������
    Current: TCalcCurrent;                  // ���
    Angles: TCalcAngles;                    // ����
    Power: TCalcPower;                      // ��������
    Energy: TCalcEnergy;                    // �������
  end;

  { ��������� ������ �� ��������/��������������� ����� ���� }
  TCalcOneFazaCollapseOvervoltage = record
    IsCollapse: Boolean;                      // True - ������, False - ��������������
    Value: Single;                            // �������� ������� ������� � ������������ ��������������
    Voltage: Single;                          // ���������� ���������� (������) ��� ������������ ���������� (��������������)
  end;

  { ��������� ������ �� ��������/��������������� ���� ��� }
  PCalcCollapsesOvervoltages = ^TCalcCollapsesOvervoltages;
  TCalcCollapsesOvervoltages = record
    Amount: Word;                               // ����������
    Duration: Single;                           // ������������
    RepeatPeriod: Single;                       // ������ ����������
    InitialFaza: Single;                        // ��������� ����
    ReferenceVoltage: Byte;                     // ������� ����������
    Linking: Byte;                              // ��������
    Faza_A: TCalcOneFazaCollapseOvervoltage;    // �������/�������������� ���� �
    Faza_B: TCalcOneFazaCollapseOvervoltage;    // �������/�������������� ���� �
    Faza_C: TCalcOneFazaCollapseOvervoltage;    // �������/�������������� ���� �
    Faza_AB: TCalcOneFazaCollapseOvervoltage;   // �������/�������������� ���� �B
    Faza_BC: TCalcOneFazaCollapseOvervoltage;   // �������/�������������� ���� �C
    Faza_CA: TCalcOneFazaCollapseOvervoltage;   // �������/�������������� ���� �A
  end;

  { ��������� ������ �� ���������� ����� ���� }
  TCalcOneFazaOscillations = record
    Swing: Single;                                   // ������ ��������� ����������
    Flicker: Single;                                 // ���� �������
  end;

  { ��������� ������ �� ���������� }
  PCalcVoltageOscillations = ^TCalcVoltageOscillations;
  TCalcVoltageOscillations = record
    Frequency: Single;                             // ������� ���������, ��
    Count: Single;                                 // ����� ��������� ���������� �� 1 ������
    Period: Single;                                // ������ ���������
    DeltaTime: Single;                             // �������� ������� ����� ����������� ����������
    Faza_A: TCalcOneFazaOscillations;              // ��������� ���� �
    Faza_B: TCalcOneFazaOscillations;              // ��������� ���� �
    Faza_C: TCalcOneFazaOscillations;              // ��������� ���� �
    Faza_AB: TCalcOneFazaOscillations;             // ��������� ���� �B
    Faza_BC: TCalcOneFazaOscillations;             // ��������� ���� �C
    Faza_CA: TCalcOneFazaOscillations;             // ��������� ���� �A
  end;

  { ������ ���������� � �������, ���������� ������������ }
  POutSignalData = ^TOutSignalData;
  TOutSignalData = record
    Signal: TCalcSignalData;                       // ������ ��������� �������
    IsGetCollOver: Boolean;                        // ���� �� ������ �� ��������/���������������
    CollapsOvervolt: TCalcCollapsesOvervoltages;   // �������/��������������
    IsGetOscillations: Boolean;                    // ���� �� ������ �� ����������
    VoltageOscillations: TCalcVoltageOscillations; // ���������
  end;

implementation

end.
