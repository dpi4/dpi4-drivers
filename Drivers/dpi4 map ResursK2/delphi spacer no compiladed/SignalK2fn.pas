{ *********************************************************************** }
{                                                                         }
{    ���������� ���������� ������������ SignalK2.dll                      }
{    ������ � ��������� ��������������� ���������� �������                }
{                                                                         }
{ *********************************************************************** }

unit SignalK2fn;

interface

uses SignalK2tp;

const
  SIGNALK2DLL = 'SignalK2.dll';

// �������� ������ ����������
procedure GetDllVersion(out Major, Minor: Word);
  stdcall; external SIGNALK2DLL;

// �������� ������������ ����������� ������������� ���������� �������� �������� �����������
function CheckSignalData(SignalData: PUserSignalData): Word;
  stdcall; external SIGNALK2DLL;

// �������� ����������� ������ � ���-������
function CheckPort(PortNumber: Word): Word;
  stdcall; external SIGNALK2DLL;

// �������� ��������� �������� ������
function GetErrorText(ErrorCode: Word; Text: PChar): Boolean;
  stdcall; external SIGNALK2DLL;

// ���������� ������ ������� ������������
function StopSignal(PortNumber: Word): Word;
  stdcall; external SIGNALK2DLL;

// ���������� ������ ������������ ��������/��������������
function StopCollapsesOvervoltages(PortNumber: Word): Word;
  stdcall; external SIGNALK2DLL;

// ��������� ������������ �������� ��������
function SetSignal(PortNumber: Word; SignalData: PUserSignalData;
  OutSignalData: POutSignalData; ServiceInfo: PServiceInfo): Word;
  stdcall; external SIGNALK2DLL;

// ������ ������ ���������� �������� �������� �����������
function GetOutSignalData(SignalData: PUserSignalData; OutSignalData: POutSignalData): Word;
  stdcall; external SIGNALK2DLL;

implementation

end.
