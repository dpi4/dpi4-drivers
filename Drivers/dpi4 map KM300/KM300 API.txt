#Управление KM300

DriverSet KM300 TIMEOUT=200
DriverSet KM300 VERSION=OLD				#Старая (OLD) или новая (NEW) версия прибора
DriverSet KM300 SIMULATION=TRUE			#Режим симуляции (без отправки команд в прибор)
DriverSet KM300 CHECK=TRUE				#Проверка ответа от прибора
DriverSet KM300 RESET						#Режим сброса
DriverSet KM300 TEST						#Тест обмена
DriverSet KM300 MEASURE					#Включить режим измерения

#настройки компаратора
DriverSet KM300 COMP_RANGE=0,1			#Предел компаратора			
DriverSet KM300 COMP_U=U1  				#Вход U1 или U2
DriverSet KM300 COMP_DU=TRUE       		#Выбор режима dU     
DriverSet KM300 COMP_AUTO=TRUE  			#Ручной/автомат.        
DriverSet KM300 COMP_DELTAU=TRUE			#Выбор режима дельтаU        
DriverSet KM300 COMP_OVER=TRUE			#Перегрузка	

DriverSet KM300 SwitchOff=TRUE			#Сброс в ноль перед установкой нового значения
DriverSet KM300 OUTPUT=Normal				#Установка выхода Normal, CT или VT
DriverSet KM300 RANGE=Auto				#Установка пределов автоматическая или вручную. Auto в пределах одного из выходов Normal, CT или VT

DriverSet KM300 VDC=10 adjust				#Установка постоянного напряжения 10 В и переход в режим регулировки
DriverSet KM300 VAC=5 1000				#Установка переменного напряжения 5 В частотой 1000 Гц
DriverSet KM300 IDC=0,1					#Установка постоянного тока 0,1 А
DriverSet KM300 IAC=0,1 50				#Установка переменного тока 0,1 А частотой 50 Гц

DriverGet KM300 mem_1=AMPL				#Чтение установленного значения амплитуды напряжения или тока калибратора
DriverGet KM300 mem_1=FREQ				#Чтение установленного значения частоты напряжения или тока калибратора
DriverGet KM300 mem_1=RANGE				#Чтение установленного значения предела
DriverGet KM300 mem_1=READ				#Чтение измеренного значения постоянного напряжения компаратора
