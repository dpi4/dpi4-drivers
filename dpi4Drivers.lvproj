﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="15008000">
	<Item Name="My Computer" Type="My Computer">
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="dpi4_LavLIB_Useful" Type="Folder">
			<Item Name="Convert Panel to Screen Coordinate.vi" Type="VI" URL="../../dpi4LIB/dpi4_lavlib_useful/!Builds/dpi4LavLIB_Useful.llb/Convert Panel to Screen Coordinate.vi"/>
			<Item Name="Create folder if not exist.vi" Type="VI" URL="../../dpi4LIB/dpi4_lavlib_useful/!Builds/dpi4LavLIB_Useful.llb/Create folder if not exist.vi"/>
			<Item Name="CSV load.vi" Type="VI" URL="../../dpi4LIB/dpi4_lavlib_useful/!Builds/dpi4LavLIB_Useful.llb/CSV load.vi"/>
			<Item Name="Excel export ArrayOfstr.vi" Type="VI" URL="../../dpi4LIB/dpi4_lavlib_useful/!Builds/dpi4LavLIB_Useful.llb/Excel export ArrayOfstr.vi"/>
			<Item Name="Excel open.vi" Type="VI" URL="../../dpi4LIB/dpi4_lavlib_useful/!Builds/dpi4LavLIB_Useful.llb/Excel open.vi"/>
			<Item Name="File Dialog.vi" Type="VI" URL="../../dpi4LIB/dpi4_lavlib_useful/!Builds/dpi4LavLIB_Useful.llb/File Dialog.vi"/>
			<Item Name="Get All Controls Ref From VI.vi" Type="VI" URL="../../dpi4LIB/dpi4_lavlib_useful/!Builds/dpi4LavLIB_Useful.llb/Get All Controls Ref From VI.vi"/>
			<Item Name="Get All inserted Controls Ref From Control.vi" Type="VI" URL="../../dpi4LIB/dpi4_lavlib_useful/!Builds/dpi4LavLIB_Useful.llb/Get All inserted Controls Ref From Control.vi"/>
			<Item Name="Get EXE name.vi" Type="VI" URL="../../dpi4LIB/dpi4_lavlib_useful/!Builds/dpi4LavLIB_Useful.llb/Get EXE name.vi"/>
			<Item Name="Get Exe Ver.vi" Type="VI" URL="../../dpi4LIB/dpi4_lavlib_useful/!Builds/dpi4LavLIB_Useful.llb/Get Exe Ver.vi"/>
			<Item Name="Get monitor resolution.vi" Type="VI" URL="../../dpi4LIB/dpi4_lavlib_useful/!Builds/dpi4LavLIB_Useful.llb/Get monitor resolution.vi"/>
			<Item Name="INI_SaveLoad.vi" Type="VI" URL="../../dpi4LIB/dpi4_lavlib_useful/!Builds/dpi4LavLIB_Useful.llb/INI_SaveLoad.vi"/>
			<Item Name="IS_EXE.vi" Type="VI" URL="../../dpi4LIB/dpi4_lavlib_useful/!Builds/dpi4LavLIB_Useful.llb/IS_EXE.vi"/>
			<Item Name="Make Top VI Frontmost.vi" Type="VI" URL="../../dpi4LIB/dpi4_lavlib_useful/!Builds/dpi4LavLIB_Useful.llb/Make Top VI Frontmost.vi"/>
			<Item Name="Make VI Frontmost.vi" Type="VI" URL="../../dpi4LIB/dpi4_lavlib_useful/!Builds/dpi4LavLIB_Useful.llb/Make VI Frontmost.vi"/>
			<Item Name="MCL or Tbl 2 Excel.vi" Type="VI" URL="../../dpi4LIB/dpi4_lavlib_useful/!Builds/dpi4LavLIB_Useful.llb/MCL or Tbl 2 Excel.vi"/>
			<Item Name="Postfix2expV2_point.vi" Type="VI" URL="../../dpi4LIB/dpi4_lavlib_useful/!Builds/dpi4LavLIB_Useful.llb/Postfix2expV2_point.vi"/>
			<Item Name="Postfix_ENG_2_RUS.vi" Type="VI" URL="../../dpi4LIB/dpi4_lavlib_useful/!Builds/dpi4LavLIB_Useful.llb/Postfix_ENG_2_RUS.vi"/>
			<Item Name="RemoveBadSymbols (SubVI).vi" Type="VI" URL="../../dpi4LIB/dpi4_lavlib_useful/!Builds/dpi4LavLIB_Useful.llb/RemoveBadSymbols (SubVI).vi"/>
			<Item Name="RunDifferentFile.vi" Type="VI" URL="../../dpi4LIB/dpi4_lavlib_useful/!Builds/dpi4LavLIB_Useful.llb/RunDifferentFile.vi"/>
			<Item Name="RunVI.vi" Type="VI" URL="../../dpi4LIB/dpi4_lavlib_useful/!Builds/dpi4LavLIB_Useful.llb/RunVI.vi"/>
			<Item Name="SaveFileAsSTR.vi" Type="VI" URL="../../dpi4LIB/dpi4_lavlib_useful/!Builds/dpi4LavLIB_Useful.llb/SaveFileAsSTR.vi"/>
			<Item Name="Set VI screen coordinates.vi" Type="VI" URL="../../dpi4LIB/dpi4_lavlib_useful/!Builds/dpi4LavLIB_Useful.llb/Set VI screen coordinates.vi"/>
			<Item Name="Set Window Z-Position (hwnd).vi" Type="VI" URL="../../dpi4LIB/dpi4_lavlib_useful/!Builds/dpi4LavLIB_Useful.llb/Set Window Z-Position (hwnd).vi"/>
			<Item Name="Startup Add EXE to it.vi" Type="VI" URL="../../dpi4LIB/dpi4_lavlib_useful/!Builds/dpi4LavLIB_Useful.llb/Startup Add EXE to it.vi"/>
			<Item Name="Startup Delete EXE from it.vi" Type="VI" URL="../../dpi4LIB/dpi4_lavlib_useful/!Builds/dpi4LavLIB_Useful.llb/Startup Delete EXE from it.vi"/>
			<Item Name="Startup Is EXE in it.vi" Type="VI" URL="../../dpi4LIB/dpi4_lavlib_useful/!Builds/dpi4LavLIB_Useful.llb/Startup Is EXE in it.vi"/>
			<Item Name="StopOrExit (SubVI).vi" Type="VI" URL="../../dpi4LIB/dpi4_lavlib_useful/!Builds/dpi4LavLIB_Useful.llb/StopOrExit (SubVI).vi"/>
			<Item Name="STR_translit.vi" Type="VI" URL="../../dpi4LIB/dpi4_lavlib_useful/!Builds/dpi4LavLIB_Useful.llb/STR_translit.vi"/>
			<Item Name="dpi4_Format_Value.vi" Type="VI" URL="../../dpi4LIB/dpi4_lavlib_useful/!Builds/dpi4LavLIB_Useful.llb/dpi4_Format_Value.vi"/>
			<Item Name="Variable_SaveLoad.vi" Type="VI" URL="../../dpi4LIB/dpi4_lavlib_useful/!Builds/dpi4LavLIB_Useful.llb/Variable_SaveLoad.vi"/>
		</Item>
		<Item Name="dpi4_lavlib_translatelib" Type="Folder">
			<Item Name="1 UTrans_Example.vi" Type="VI" URL="../../dpi4LIB/dpi4_lavlib_translatelib/builds/dpi4Translate.llb/1 UTrans_Example.vi"/>
			<Item Name="Language.ctl" Type="VI" URL="../../dpi4LIB/dpi4_lavlib_translatelib/builds/dpi4Translate.llb/Language.ctl"/>
			<Item Name="UTrans_Core.vi" Type="VI" URL="../../dpi4LIB/dpi4_lavlib_translatelib/builds/dpi4Translate.llb/UTrans_Core.vi"/>
			<Item Name="UTrans_Core_Write_All_Controls.vi" Type="VI" URL="../../dpi4LIB/dpi4_lavlib_translatelib/builds/dpi4Translate.llb/UTrans_Core_Write_All_Controls.vi"/>
			<Item Name="UTrans_Get_Current_Lang.vi" Type="VI" URL="../../dpi4LIB/dpi4_lavlib_translatelib/builds/dpi4Translate.llb/UTrans_Get_Current_Lang.vi"/>
			<Item Name="UTrans_Set_Current_Lang.vi" Type="VI" URL="../../dpi4LIB/dpi4_lavlib_translatelib/builds/dpi4Translate.llb/UTrans_Set_Current_Lang.vi"/>
			<Item Name="UTranslate_Creat_Lable.vi" Type="VI" URL="../../dpi4LIB/dpi4_lavlib_translatelib/builds/dpi4Translate.llb/UTranslate_Creat_Lable.vi"/>
			<Item Name="UTranslate_FindControls.vi" Type="VI" URL="../../dpi4LIB/dpi4_lavlib_translatelib/builds/dpi4Translate.llb/UTranslate_FindControls.vi"/>
			<Item Name="UTranslate_GetAllStringsFrom_VI.vi" Type="VI" URL="../../dpi4LIB/dpi4_lavlib_translatelib/builds/dpi4Translate.llb/UTranslate_GetAllStringsFrom_VI.vi"/>
			<Item Name="UTranslate_ID_string.vi" Type="VI" URL="../../dpi4LIB/dpi4_lavlib_translatelib/builds/dpi4Translate.llb/UTranslate_ID_string.vi"/>
			<Item Name="UTranslate_SetControl.vi" Type="VI" URL="../../dpi4LIB/dpi4_lavlib_translatelib/builds/dpi4Translate.llb/UTranslate_SetControl.vi"/>
			<Item Name="UTranslate_SetMenu.vi" Type="VI" URL="../../dpi4LIB/dpi4_lavlib_translatelib/builds/dpi4Translate.llb/UTranslate_SetMenu.vi"/>
			<Item Name="UTranslate_SetTextDecor.vi" Type="VI" URL="../../dpi4LIB/dpi4_lavlib_translatelib/builds/dpi4Translate.llb/UTranslate_SetTextDecor.vi"/>
			<Item Name="UTranslate_SetTitleBar.vi" Type="VI" URL="../../dpi4LIB/dpi4_lavlib_translatelib/builds/dpi4Translate.llb/UTranslate_SetTitleBar.vi"/>
		</Item>
		<Item Name="Drivers" Type="Folder">
			<Item Name="B7_xx" Type="Folder">
				<Item Name="B7_53.vi" Type="VI" URL="../Drivers/dpi4 map B7_53/B7_53.vi"/>
				<Item Name="B7_54.vi" Type="VI" URL="../Drivers/dpi4 map B7_54/B7_54.vi"/>
				<Item Name="B7_64.vi" Type="VI" URL="../Drivers/dpi4 map B7_64/B7_64.vi"/>
				<Item Name="B7_64M.vi" Type="VI" URL="../Drivers/dpi4 map B7_64M/B7_64M.vi"/>
				<Item Name="B7_65.vi" Type="VI" URL="../Drivers/dpi4 map B7_65/B7_65.vi"/>
				<Item Name="B7_68.vi" Type="VI" URL="../Drivers/dpi4 map B7_68/B7_68.vi"/>
				<Item Name="B7_72.vi" Type="VI" URL="../Drivers/dpi4 map B7_72/B7_72.vi"/>
				<Item Name="B7_73.vi" Type="VI" URL="../Drivers/dpi4 map B7_73/B7_73.vi"/>
				<Item Name="B7_74.vi" Type="VI" URL="../Drivers/dpi4 map B7_74/B7_74.vi"/>
				<Item Name="B7_78.vi" Type="VI" URL="../Drivers/dpi4 map B7_78/B7_78.vi"/>
				<Item Name="B7_80.vi" Type="VI" URL="../Drivers/dpi4 map B7_80/B7_80.vi"/>
				<Item Name="B7_82.vi" Type="VI" URL="../Drivers/dpi4 map B7_82/B7_82.vi"/>
				<Item Name="B7_84.vi" Type="VI" URL="../Drivers/dpi4 map B7_84/B7_84.vi"/>
				<Item Name="B7_89.vi" Type="VI" URL="../Drivers/dpi4 map B7_89/B7_89.vi"/>
			</Item>
			<Item Name="Fluke" Type="Folder">
				<Item Name="Fluke12x" Type="Folder">
					<Item Name="Fluke12x.vi" Type="VI" URL="../Drivers/dpi4 map Fluke12x/Fluke12x.vi"/>
					<Item Name="Fluke12x TEST.vi" Type="VI" URL="../Drivers/dpi4 map Fluke12x/Fluke12x TEST.vi"/>
					<Item Name="Fluke12x_CompareSetupNodes.vi" Type="VI" URL="../Drivers/dpi4 map Fluke12x/Fluke12x_CompareSetupNodes.vi"/>
				</Item>
				<Item Name="Fluke9100.vi" Type="VI" URL="../Drivers/dpi4 map Fluke9100/Fluke9100.vi"/>
				<Item Name="Fluke9500.vi" Type="VI" URL="../Drivers/dpi4 map Fluke9500/Fluke9500.vi"/>
				<Item Name="Fluke5790A.vi" Type="VI" URL="../Drivers/dpi4 map Fluke5790/Fluke5790A.vi"/>
				<Item Name="Fluke6100A.vi" Type="VI" URL="../Drivers/dpi4 map Fluke6100/Fluke6100A.vi"/>
				<Item Name="Fluke8508A.vi" Type="VI" URL="../Drivers/dpi4 map Fluke8508A/Fluke8508A.vi"/>
				<Item Name="Fluke8508A_UUT.vi" Type="VI" URL="../Drivers/dpi4 map Fluke8508A_UUT/Fluke8508A_UUT.vi"/>
				<Item Name="Fluke5000.vi" Type="VI" URL="../Drivers/dpi4 map Fluke5000/Fluke5000.vi"/>
				<Item Name="Fluke RPM4.vi" Type="VI" URL="../Drivers/dpi4 map FlukeRPM4/Fluke RPM4.vi"/>
				<Item Name="Fluke88XXA.vi" Type="VI" URL="../Drivers/dpi4 map Fluke884XA/Fluke88XXA.vi"/>
			</Item>
			<Item Name="RF" Type="Folder">
				<Item Name="AnritsuSG.vi" Type="VI" URL="../Drivers/dpi4 map AnritsuSG/AnritsuSG.vi"/>
				<Item Name="AnritsuSA.vi" Type="VI" URL="../Drivers/dpi4 map AnritsuSA/AnritsuSA.vi"/>
				<Item Name="MS2687B.vi" Type="VI" URL="../Drivers/dpi4 map AnritsuMS2687B/MS2687B.vi"/>
				<Item Name="MT8815.vi" Type="VI" URL="../Drivers/dpi4 map MT8815B/MT8815.vi"/>
				<Item Name="PG4_17.vi" Type="VI" URL="../Drivers/dpi4 map RG4_17/PG4_17.vi"/>
				<Item Name="G3_122.vi" Type="VI" URL="../Drivers/dpi4 map G3_122/G3_122.vi"/>
				<Item Name="G5_82.vi" Type="VI" URL="../Drivers/dpi4 map G5_82/G5_82.vi"/>
				<Item Name="RG4_04.vi" Type="VI" URL="../Drivers/dpi4 map RG4_04/RG4_04.vi"/>
				<Item Name="Stanford DS360.vi" Type="VI" URL="../Drivers/dpi4 map StanfordDS360/Stanford DS360.vi"/>
				<Item Name="G4_221.vi" Type="VI" URL="../Drivers/dpi4 map G3_221/G4_221.vi"/>
			</Item>
			<Item Name="N4" Type="Folder">
				<Item Name="N4_6.vi" Type="VI" URL="../Drivers/dpi4 map N4_6/N4_6.vi"/>
				<Item Name="N4_11.vi" Type="VI" URL="../Drivers/dpi4 map N4_11/N4_11.vi"/>
				<Item Name="N4_12.vi" Type="VI" URL="../Drivers/dpi4 map N4_12/N4_12.vi"/>
				<Item Name="N4_17.vi" Type="VI" URL="../Drivers/dpi4 map N4_17/N4_17.vi"/>
			</Item>
			<Item Name="MNIPI" Type="Folder">
				<Item Name="N4_101.vi" Type="VI" URL="../Drivers/dpi4 map N4_101/N4_101.vi"/>
				<Item Name="N4_201.vi" Type="VI" URL="../Drivers/dpi4 map N4_201/N4_201.vi"/>
				<Item Name="E7_20.vi" Type="VI" URL="../Drivers/dpi4 map E7_20/E7_20.vi"/>
				<Item Name="E7_28.vi" Type="VI" URL="../Drivers/dpi4 map E7_28/E7_28.vi"/>
				<Item Name="E7_30.vi" Type="VI" URL="../Drivers/dpi4 map E7_30/E7_30.vi"/>
			</Item>
			<Item Name="Pressure" Type="Folder">
				<Item Name="Betagauge.vi" Type="VI" URL="../Drivers/dpi4 map Betagauge/Betagauge.vi"/>
				<Item Name="XP2i.vi" Type="VI" URL="../Drivers/dpi4 map CrystalXP2/XP2i.vi"/>
				<Item Name="METRAN517.vi" Type="VI" URL="../Drivers/dpi4 map Metran517/METRAN517.vi"/>
			</Item>
			<Item Name="SoundMeter" Type="Folder">
				<Item Name="octava101a.vi" Type="VI" URL="../Drivers/dpi4 map OCTAVA101A/octava101a.vi"/>
			</Item>
			<Item Name="Power Quality Analyzer" Type="Folder">
				<Item Name="ENERGOMONITOR_3_3.vi" Type="VI" URL="../Drivers/dpi4 map Energomonitor_3_3/ENERGOMONITOR_3_3.vi"/>
				<Item Name="Fluke43B.vi" Type="VI" URL="../Drivers/dpi4 map Fluke43B/Fluke43B.vi"/>
				<Item Name="Fluke43X.vi" Type="VI" URL="../Drivers/dpi4 map Fluke43X/Fluke43X.vi"/>
				<Item Name="RESURS_K2.vi" Type="VI" URL="../Drivers/dpi4 map ResursK2/RESURS_K2.vi"/>
				<Item Name="УK1.vi" Type="VI" URL="../Drivers/dpi4 map UK1/УK1.vi"/>
			</Item>
			<Item Name="DMM" Type="Folder">
				<Item Name="InstekDMM.vi" Type="VI" URL="../Drivers/dpi4 map InstekDMM/InstekDMM.vi"/>
				<Item Name="AgilentDMM.vi" Type="VI" URL="../Drivers/dpi4 map AgilentDMM/AgilentDMM.vi"/>
				<Item Name="Agilent3458A.vi" Type="VI" URL="../Drivers/dpi4 map Agilent 3458A/Agilent3458A.vi"/>
				<Item Name="RigolDMM.vi" Type="VI" URL="../Drivers/dpi4 map RigolDMM/RigolDMM.vi"/>
				<Item Name="TektronixDMM.vi" Type="VI" URL="../Drivers/dpi4 map TektronixDMM/TektronixDMM.vi"/>
				<Item Name="Transmille80xx.vi" Type="VI" URL="../Drivers/dpi4 map Transmille8000/Transmille80xx.vi"/>
				<Item Name="AgilentU3400.vi" Type="VI" URL="../Drivers/dpi4 map AgilentU3400/AgilentU3400.vi"/>
				<Item Name="InstekDMM70000.vi" Type="VI" URL="../Drivers/dpi4 map InstekDMM7/InstekDMM70000.vi"/>
				<Item Name="Instek8212.vi" Type="VI" URL="../Drivers/dpi4 map Instek8212/Instek8212.vi"/>
				<Item Name="KeithleyDMM2000.vi" Type="VI" URL="../Drivers/dpi4 map KeithleyDMM2000/KeithleyDMM2000.vi"/>
				<Item Name="R&amp;S_HMC_DMM.vi" Type="VI" URL="../Drivers/dpi4 map R&amp;S HMC DMM/R&amp;S_HMC_DMM.vi"/>
			</Item>
			<Item Name="STORE" Type="Folder">
				<Item Name="RESISTANCESTORE.vi" Type="VI" URL="../Drivers/dpi4 map ResistanceStore/RESISTANCESTORE.vi"/>
				<Item Name="PRESSURESTORE.vi" Type="VI" URL="../Drivers/dpi4 map PressureStore/PRESSURESTORE.vi"/>
				<Item Name="ACOUSTICSTORE.vi" Type="VI" URL="../Drivers/dpi4 map Driver AcousticStore/ACOUSTICSTORE.vi"/>
				<Item Name="COILSTORE.vi" Type="VI" URL="../Drivers/dpi4 map COILSTORE/COILSTORE.vi"/>
			</Item>
			<Item Name="ZNP(KRASNODAR)" Type="Folder">
				<Item Name="KM300.vi" Type="VI" URL="../Drivers/dpi4 map KM300/KM300.vi"/>
				<Item Name="CO3001.vi" Type="VI" URL="../Drivers/dpi4 map CO3001/CO3001.vi"/>
				<Item Name="KM300C.vi" Type="VI" URL="../Drivers/dpi4 map KM300C/KM300C.vi"/>
			</Item>
			<Item Name="IKSU" Type="Folder">
				<Item Name="IKSU2000.vi" Type="VI" URL="../Drivers/dpi4 map IKSU2000/IKSU2000.vi"/>
			</Item>
			<Item Name="AKIP" Type="Folder">
				<Item Name="AKIP1202.vi" Type="VI" URL="../Drivers/dpi4 map AKIP_1202/AKIP1202.vi"/>
				<Item Name="AKIP1310.vi" Type="VI" URL="../Drivers/dpi4 map AKIP_1310/AKIP1310.vi"/>
				<Item Name="AKIP1320.vi" Type="VI" URL="../Drivers/dpi4 map AKIP_1320/AKIP1320.vi"/>
				<Item Name="AKIP2402.vi" Type="VI" URL="../Drivers/dpi4 map AKIP_2402/AKIP2402.vi"/>
			</Item>
			<Item Name="Instek" Type="Folder">
				<Item Name="IntekPSM.vi" Type="VI" URL="../Drivers/dpi4 map IntekPSM/IntekPSM.vi"/>
				<Item Name="InstekP(S_SS_ST_SH).vi" Type="VI" URL="../Drivers/dpi4 map InstekP(S_SS_ST_SH)/InstekP(S_SS_ST_SH).vi"/>
				<Item Name="InstekPCS71000.vi" Type="VI" URL="../Drivers/dpi4 map InstekPCS71000/InstekPCS71000.vi"/>
				<Item Name="InstekGPM8212.vi" Type="VI" URL="../Drivers/dpi4 map Instek_GPM_8212/InstekGPM8212.vi"/>
			</Item>
			<Item Name="B1_18.vi" Type="VI" URL="../Drivers/dpi4 map B1_18/B1_18.vi"/>
			<Item Name="B1_28.vi" Type="VI" URL="../Drivers/dpi4 map B1_28/B1_28.vi"/>
			<Item Name="TestDriver.vi" Type="VI" URL="../Drivers/dpi4 map TestDriver/TestDriver.vi"/>
			<Item Name="Transmille30xx.vi" Type="VI" URL="../Drivers/dpi4 map Transmille3000/Transmille30xx.vi"/>
			<Item Name="HP1600.vi" Type="VI" URL="../Drivers/dpi4 map HA1600/HP1600.vi"/>
			<Item Name="R&amp;S_CMU200.vi" Type="VI" URL="../Drivers/dpi4 map CMU200/R&amp;S_CMU200.vi"/>
			<Item Name="R&amp;S_SMBV100A.vi" Type="VI" URL="../Drivers/dpi4 map SMBV100A/R&amp;S_SMBV100A.vi"/>
			<Item Name="USWITCH.vi" Type="VI" URL="../Drivers/dpi4 map Uswitch/USWITCH.vi"/>
			<Item Name="PACE5000.vi" Type="VI" URL="../Drivers/dpi4 map PACE5000/PACE5000.vi"/>
			<Item Name="AR100A400.vi" Type="VI" URL="../Drivers/dpi4 map AR100A400/AR100A400.vi"/>
			<Item Name="AgilentN5181.vi" Type="VI" URL="../Drivers/dpi4 map AgilentN5181/AgilentN5181.vi"/>
			<Item Name="Agilent740X.vi" Type="VI" URL="../Drivers/dpi4 map Agilent_740XA/Agilent740X.vi"/>
			<Item Name="Ametek_MX15_Series.vi" Type="VI" URL="../Drivers/dpi4 map Ametek_MX15/Ametek_MX15_Series.vi"/>
			<Item Name="NEX1_NS265.vi" Type="VI" URL="../Drivers/dpi4 map NEX1_NS265/NEX1_NS265.vi"/>
			<Item Name="KeysightFuncGen.vi" Type="VI" URL="../Drivers/dpi4 map AgilentRF/KeysightFuncGen.vi"/>
			<Item Name="CE602.vi" Type="VI" URL="../Drivers/dpi4 map CE602/CE602.vi"/>
			<Item Name="EX124.vi" Type="VI" URL="../Drivers/dpi4 map EX124/EX124.vi"/>
			<Item Name="AgilentN67xx.vi" Type="VI" URL="../Drivers/dpi4 map AgilentN67xx/AgilentN67xx.vi"/>
			<Item Name="Keithley_2002.vi" Type="VI" URL="../Drivers/dpi4 map Keitley_2002/Keithley_2002.vi"/>
			<Item Name="Keithley_7001.vi" Type="VI" URL="../Drivers/dpi4 map Keitley_7001/Keithley_7001.vi"/>
			<Item Name="AgilentE363x.vi" Type="VI" URL="../Drivers/dpi4 map Agient E363x/AgilentE363x.vi"/>
			<Item Name="TDK_Lambda.vi" Type="VI" URL="../Drivers/dpi4 map TDK_Lambda/TDK_Lambda.vi"/>
		</Item>
		<Item Name="DriverLowLevel" Type="Folder">
			<Item Name="NI Modbus" Type="Folder">
				<Item Name="MB CRC-16.vi" Type="VI" URL="../DriverLowLevel/nimodbus121/NI Modbus.llb/MB CRC-16.vi"/>
				<Item Name="MB Decode Data.vi" Type="VI" URL="../DriverLowLevel/nimodbus121/NI Modbus.llb/MB Decode Data.vi"/>
				<Item Name="MB Embedded Modbus Demo Box Example (Legacy).vi" Type="VI" URL="../DriverLowLevel/nimodbus121/NI Modbus.llb/MB Embedded Modbus Demo Box Example (Legacy).vi"/>
				<Item Name="MB Embedded Modbus Demo Box Example.vi" Type="VI" URL="../DriverLowLevel/nimodbus121/NI Modbus.llb/MB Embedded Modbus Demo Box Example.vi"/>
				<Item Name="MB Ethernet Call Wait on Query.vi" Type="VI" URL="../DriverLowLevel/nimodbus121/NI Modbus.llb/MB Ethernet Call Wait on Query.vi"/>
				<Item Name="MB Ethernet Check Client.vi" Type="VI" URL="../DriverLowLevel/nimodbus121/NI Modbus.llb/MB Ethernet Check Client.vi"/>
				<Item Name="MB Ethernet Connection Function.ctl" Type="VI" URL="../DriverLowLevel/nimodbus121/NI Modbus.llb/MB Ethernet Connection Function.ctl"/>
				<Item Name="MB Ethernet Connection Manager.vi" Type="VI" URL="../DriverLowLevel/nimodbus121/NI Modbus.llb/MB Ethernet Connection Manager.vi"/>
				<Item Name="MB Ethernet Example Master (Legacy).vi" Type="VI" URL="../DriverLowLevel/nimodbus121/NI Modbus.llb/MB Ethernet Example Master (Legacy).vi"/>
				<Item Name="MB Ethernet Example Master.vi" Type="VI" URL="../DriverLowLevel/nimodbus121/NI Modbus.llb/MB Ethernet Example Master.vi"/>
				<Item Name="MB Ethernet Example Slave (Legacy).vi" Type="VI" URL="../DriverLowLevel/nimodbus121/NI Modbus.llb/MB Ethernet Example Slave (Legacy).vi"/>
				<Item Name="MB Ethernet Example Slave.vi" Type="VI" URL="../DriverLowLevel/nimodbus121/NI Modbus.llb/MB Ethernet Example Slave.vi"/>
				<Item Name="MB Ethernet Is Address Valid.vi" Type="VI" URL="../DriverLowLevel/nimodbus121/NI Modbus.llb/MB Ethernet Is Address Valid.vi"/>
				<Item Name="MB Ethernet Master Query (poly).vi" Type="VI" URL="../DriverLowLevel/nimodbus121/NI Modbus.llb/MB Ethernet Master Query (poly).vi"/>
				<Item Name="MB Ethernet Master Query Read Coils (poly).vi" Type="VI" URL="../DriverLowLevel/nimodbus121/NI Modbus.llb/MB Ethernet Master Query Read Coils (poly).vi"/>
				<Item Name="MB Ethernet Master Query Read Discrete Inputs (poly).vi" Type="VI" URL="../DriverLowLevel/nimodbus121/NI Modbus.llb/MB Ethernet Master Query Read Discrete Inputs (poly).vi"/>
				<Item Name="MB Ethernet Master Query Read Exception Status (poly).vi" Type="VI" URL="../DriverLowLevel/nimodbus121/NI Modbus.llb/MB Ethernet Master Query Read Exception Status (poly).vi"/>
				<Item Name="MB Ethernet Master Query Read Holding Registers (poly).vi" Type="VI" URL="../DriverLowLevel/nimodbus121/NI Modbus.llb/MB Ethernet Master Query Read Holding Registers (poly).vi"/>
				<Item Name="MB Ethernet Master Query Read Input Registers (poly).vi" Type="VI" URL="../DriverLowLevel/nimodbus121/NI Modbus.llb/MB Ethernet Master Query Read Input Registers (poly).vi"/>
				<Item Name="MB Ethernet Master Query Write Multiple Coils (poly).vi" Type="VI" URL="../DriverLowLevel/nimodbus121/NI Modbus.llb/MB Ethernet Master Query Write Multiple Coils (poly).vi"/>
				<Item Name="MB Ethernet Master Query Write Multiple Registers (poly).vi" Type="VI" URL="../DriverLowLevel/nimodbus121/NI Modbus.llb/MB Ethernet Master Query Write Multiple Registers (poly).vi"/>
				<Item Name="MB Ethernet Master Query Write Single Coil (poly).vi" Type="VI" URL="../DriverLowLevel/nimodbus121/NI Modbus.llb/MB Ethernet Master Query Write Single Coil (poly).vi"/>
				<Item Name="MB Ethernet Master Query Write Single Register (poly).vi" Type="VI" URL="../DriverLowLevel/nimodbus121/NI Modbus.llb/MB Ethernet Master Query Write Single Register (poly).vi"/>
				<Item Name="MB Ethernet Master Query.vi" Type="VI" URL="../DriverLowLevel/nimodbus121/NI Modbus.llb/MB Ethernet Master Query.vi"/>
				<Item Name="MB Ethernet Receive.vi" Type="VI" URL="../DriverLowLevel/nimodbus121/NI Modbus.llb/MB Ethernet Receive.vi"/>
				<Item Name="MB Ethernet RefCount.vi" Type="VI" URL="../DriverLowLevel/nimodbus121/NI Modbus.llb/MB Ethernet RefCount.vi"/>
				<Item Name="MB Ethernet Slave Communication.vi" Type="VI" URL="../DriverLowLevel/nimodbus121/NI Modbus.llb/MB Ethernet Slave Communication.vi"/>
				<Item Name="MB Ethernet Slave Demon - Single Port - Multiple Connections 2.vi" Type="VI" URL="../DriverLowLevel/nimodbus121/NI Modbus.llb/MB Ethernet Slave Demon - Single Port - Multiple Connections 2.vi"/>
				<Item Name="MB Ethernet Slave Demon - Single Port - Multiple Connections.vi" Type="VI" URL="../DriverLowLevel/nimodbus121/NI Modbus.llb/MB Ethernet Slave Demon - Single Port - Multiple Connections.vi"/>
				<Item Name="MB Ethernet Slave Demon - Single Port - Single Connection.vi" Type="VI" URL="../DriverLowLevel/nimodbus121/NI Modbus.llb/MB Ethernet Slave Demon - Single Port - Single Connection.vi"/>
				<Item Name="MB Ethernet Slave Demon.vi" Type="VI" URL="../DriverLowLevel/nimodbus121/NI Modbus.llb/MB Ethernet Slave Demon.vi"/>
				<Item Name="MB Ethernet String to Modbus Data Unit.vi" Type="VI" URL="../DriverLowLevel/nimodbus121/NI Modbus.llb/MB Ethernet String to Modbus Data Unit.vi"/>
				<Item Name="MB Ethernet Transmit.vi" Type="VI" URL="../DriverLowLevel/nimodbus121/NI Modbus.llb/MB Ethernet Transmit.vi"/>
				<Item Name="MB Ethernet Wait on Query.vi" Type="VI" URL="../DriverLowLevel/nimodbus121/NI Modbus.llb/MB Ethernet Wait on Query.vi"/>
				<Item Name="MB Exception Code to Error Message.vi" Type="VI" URL="../DriverLowLevel/nimodbus121/NI Modbus.llb/MB Exception Code to Error Message.vi"/>
				<Item Name="MB Globals.vi" Type="VI" URL="../DriverLowLevel/nimodbus121/NI Modbus.llb/MB Globals.vi"/>
				<Item Name="MB LRC-8.vi" Type="VI" URL="../DriverLowLevel/nimodbus121/NI Modbus.llb/MB LRC-8.vi"/>
				<Item Name="MB Modbus Command to Data Unit.vi" Type="VI" URL="../DriverLowLevel/nimodbus121/NI Modbus.llb/MB Modbus Command to Data Unit.vi"/>
				<Item Name="MB Modbus Command.ctl" Type="VI" URL="../DriverLowLevel/nimodbus121/NI Modbus.llb/MB Modbus Command.ctl"/>
				<Item Name="MB Modbus Data Unit.ctl" Type="VI" URL="../DriverLowLevel/nimodbus121/NI Modbus.llb/MB Modbus Data Unit.ctl"/>
				<Item Name="MB Modbus Demo Box Example (Legacy).vi" Type="VI" URL="../DriverLowLevel/nimodbus121/NI Modbus.llb/MB Modbus Demo Box Example (Legacy).vi"/>
				<Item Name="MB Modbus Demo Box Example.vi" Type="VI" URL="../DriverLowLevel/nimodbus121/NI Modbus.llb/MB Modbus Demo Box Example.vi"/>
				<Item Name="MB RefCount Function.ctl" Type="VI" URL="../DriverLowLevel/nimodbus121/NI Modbus.llb/MB RefCount Function.ctl"/>
				<Item Name="MB Registers Manager.vi" Type="VI" URL="../DriverLowLevel/nimodbus121/NI Modbus.llb/MB Registers Manager.vi"/>
				<Item Name="MB Serial Call Wait on Query.vi" Type="VI" URL="../DriverLowLevel/nimodbus121/NI Modbus.llb/MB Serial Call Wait on Query.vi"/>
				<Item Name="MB Serial Example Master (Legacy).vi" Type="VI" URL="../DriverLowLevel/nimodbus121/NI Modbus.llb/MB Serial Example Master (Legacy).vi"/>
				<Item Name="MB Serial Example Master.vi" Type="VI" URL="../DriverLowLevel/nimodbus121/NI Modbus.llb/MB Serial Example Master.vi"/>
				<Item Name="MB Serial Example Slave (Legacy).vi" Type="VI" URL="../DriverLowLevel/nimodbus121/NI Modbus.llb/MB Serial Example Slave (Legacy).vi"/>
				<Item Name="MB Serial Example Slave.vi" Type="VI" URL="../DriverLowLevel/nimodbus121/NI Modbus.llb/MB Serial Example Slave.vi"/>
				<Item Name="MB Serial Init.vi" Type="VI" URL="../DriverLowLevel/nimodbus121/NI Modbus.llb/MB Serial Init.vi"/>
				<Item Name="MB Serial Master Query (poly).vi" Type="VI" URL="../DriverLowLevel/nimodbus121/NI Modbus.llb/MB Serial Master Query (poly).vi"/>
				<Item Name="MB Serial Master Query Read Coils (poly).vi" Type="VI" URL="../DriverLowLevel/nimodbus121/NI Modbus.llb/MB Serial Master Query Read Coils (poly).vi"/>
				<Item Name="MB Serial Master Query Read Discrete Inputs (poly).vi" Type="VI" URL="../DriverLowLevel/nimodbus121/NI Modbus.llb/MB Serial Master Query Read Discrete Inputs (poly).vi"/>
				<Item Name="MB Serial Master Query Read Exception Status (poly).vi" Type="VI" URL="../DriverLowLevel/nimodbus121/NI Modbus.llb/MB Serial Master Query Read Exception Status (poly).vi"/>
				<Item Name="MB Serial Master Query Read Holding Registers (poly).vi" Type="VI" URL="../DriverLowLevel/nimodbus121/NI Modbus.llb/MB Serial Master Query Read Holding Registers (poly).vi"/>
				<Item Name="MB Serial Master Query Read Input Registers (poly).vi" Type="VI" URL="../DriverLowLevel/nimodbus121/NI Modbus.llb/MB Serial Master Query Read Input Registers (poly).vi"/>
				<Item Name="MB Serial Master Query Write Multiple Coils (poly).vi" Type="VI" URL="../DriverLowLevel/nimodbus121/NI Modbus.llb/MB Serial Master Query Write Multiple Coils (poly).vi"/>
				<Item Name="MB Serial Master Query Write Multiple Registers (poly).vi" Type="VI" URL="../DriverLowLevel/nimodbus121/NI Modbus.llb/MB Serial Master Query Write Multiple Registers (poly).vi"/>
				<Item Name="MB Serial Master Query Write Single Coil (poly).vi" Type="VI" URL="../DriverLowLevel/nimodbus121/NI Modbus.llb/MB Serial Master Query Write Single Coil (poly).vi"/>
				<Item Name="MB Serial Master Query Write Single Register (poly).vi" Type="VI" URL="../DriverLowLevel/nimodbus121/NI Modbus.llb/MB Serial Master Query Write Single Register (poly).vi"/>
				<Item Name="MB Serial Master Query.vi" Type="VI" URL="../DriverLowLevel/nimodbus121/NI Modbus.llb/MB Serial Master Query.vi"/>
				<Item Name="MB Serial Modbus Data Unit to String.vi" Type="VI" URL="../DriverLowLevel/nimodbus121/NI Modbus.llb/MB Serial Modbus Data Unit to String.vi"/>
				<Item Name="MB Serial Receive.vi" Type="VI" URL="../DriverLowLevel/nimodbus121/NI Modbus.llb/MB Serial Receive.vi"/>
				<Item Name="MB Serial RefCount.vi" Type="VI" URL="../DriverLowLevel/nimodbus121/NI Modbus.llb/MB Serial RefCount.vi"/>
				<Item Name="MB Serial Slave Communication.vi" Type="VI" URL="../DriverLowLevel/nimodbus121/NI Modbus.llb/MB Serial Slave Communication.vi"/>
				<Item Name="MB Serial Slave Demon.vi" Type="VI" URL="../DriverLowLevel/nimodbus121/NI Modbus.llb/MB Serial Slave Demon.vi"/>
				<Item Name="MB Serial Slave Init.vi" Type="VI" URL="../DriverLowLevel/nimodbus121/NI Modbus.llb/MB Serial Slave Init.vi"/>
				<Item Name="MB Serial String to Modbus Data Unit.vi" Type="VI" URL="../DriverLowLevel/nimodbus121/NI Modbus.llb/MB Serial String to Modbus Data Unit.vi"/>
				<Item Name="MB Serial Transmit.vi" Type="VI" URL="../DriverLowLevel/nimodbus121/NI Modbus.llb/MB Serial Transmit.vi"/>
				<Item Name="MB Serial Wait on Query.vi" Type="VI" URL="../DriverLowLevel/nimodbus121/NI Modbus.llb/MB Serial Wait on Query.vi"/>
				<Item Name="MB Slave Init (poly).vi" Type="VI" URL="../DriverLowLevel/nimodbus121/NI Modbus.llb/MB Slave Init (poly).vi"/>
				<Item Name="MB Slave Operations (poly).vi" Type="VI" URL="../DriverLowLevel/nimodbus121/NI Modbus.llb/MB Slave Operations (poly).vi"/>
				<Item Name="MB Slave Read All Coils (poly).vi" Type="VI" URL="../DriverLowLevel/nimodbus121/NI Modbus.llb/MB Slave Read All Coils (poly).vi"/>
				<Item Name="MB Slave Read All Discrete Inputs (poly).vi" Type="VI" URL="../DriverLowLevel/nimodbus121/NI Modbus.llb/MB Slave Read All Discrete Inputs (poly).vi"/>
				<Item Name="MB Slave Read All Holding Registers (poly).vi" Type="VI" URL="../DriverLowLevel/nimodbus121/NI Modbus.llb/MB Slave Read All Holding Registers (poly).vi"/>
				<Item Name="MB Slave Read All Input Registers (poly).vi" Type="VI" URL="../DriverLowLevel/nimodbus121/NI Modbus.llb/MB Slave Read All Input Registers (poly).vi"/>
				<Item Name="MB Slave Read Coils (poly).vi" Type="VI" URL="../DriverLowLevel/nimodbus121/NI Modbus.llb/MB Slave Read Coils (poly).vi"/>
				<Item Name="MB Slave Read Discrete Inputs (poly).vi" Type="VI" URL="../DriverLowLevel/nimodbus121/NI Modbus.llb/MB Slave Read Discrete Inputs (poly).vi"/>
				<Item Name="MB Slave Read Holding Registers (poly).vi" Type="VI" URL="../DriverLowLevel/nimodbus121/NI Modbus.llb/MB Slave Read Holding Registers (poly).vi"/>
				<Item Name="MB Slave Read Input Registers (poly).vi" Type="VI" URL="../DriverLowLevel/nimodbus121/NI Modbus.llb/MB Slave Read Input Registers (poly).vi"/>
				<Item Name="MB Slave Write Coils (poly).vi" Type="VI" URL="../DriverLowLevel/nimodbus121/NI Modbus.llb/MB Slave Write Coils (poly).vi"/>
				<Item Name="MB Slave Write Discrete Inputs (poly).vi" Type="VI" URL="../DriverLowLevel/nimodbus121/NI Modbus.llb/MB Slave Write Discrete Inputs (poly).vi"/>
				<Item Name="MB Slave Write Holding Registers (poly).vi" Type="VI" URL="../DriverLowLevel/nimodbus121/NI Modbus.llb/MB Slave Write Holding Registers (poly).vi"/>
				<Item Name="MB Slave Write Input Registers (poly).vi" Type="VI" URL="../DriverLowLevel/nimodbus121/NI Modbus.llb/MB Slave Write Input Registers (poly).vi"/>
				<Item Name="MB Update Registers.vi" Type="VI" URL="../DriverLowLevel/nimodbus121/NI Modbus.llb/MB Update Registers.vi"/>
			</Item>
			<Item Name="DrLL_User_Select_Interface2.vi" Type="VI" URL="../DriverLowLevel/DrLL_User_Select_Interface2.vi"/>
			<Item Name="DrLL_User_Select_Interface.vi" Type="VI" URL="../DriverLowLevel/DrLL_User_Select_Interface.vi"/>
			<Item Name="DrLL_ChangeSettings.vi" Type="VI" URL="../DriverLowLevel/DrLL_ChangeSettings.vi"/>
			<Item Name="DrLL_Clear.vi" Type="VI" URL="../DriverLowLevel/DrLL_Clear.vi"/>
			<Item Name="DrLL_ESR_check.vi" Type="VI" URL="../DriverLowLevel/DrLL_ESR_check.vi"/>
			<Item Name="DrLL_format_output_value.vi" Type="VI" URL="../DriverLowLevel/DrLL_format_output_value.vi"/>
			<Item Name="DrLL_FormatStringValue.vi" Type="VI" URL="../DriverLowLevel/DrLL_FormatStringValue.vi"/>
			<Item Name="DrLL_ID_Gen.vi" Type="VI" URL="../DriverLowLevel/DrLL_ID_Gen.vi"/>
			<Item Name="DrLL_Init.vi" Type="VI" URL="../DriverLowLevel/DrLL_Init.vi"/>
			<Item Name="DrLL_InterfaceSettings.ctl" Type="VI" URL="../DriverLowLevel/DrLL_InterfaceSettings.ctl"/>
			<Item Name="DrLL_InterfaceType.ctl" Type="VI" URL="../DriverLowLevel/DrLL_InterfaceType.ctl"/>
			<Item Name="DrLL_Load_Last_Setting.vi" Type="VI" URL="../DriverLowLevel/DrLL_Load_Last_Setting.vi"/>
			<Item Name="DrLL_ParameterSelect.vi" Type="VI" URL="../DriverLowLevel/DrLL_ParameterSelect.vi"/>
			<Item Name="DrLL_Read.vi" Type="VI" URL="../DriverLowLevel/DrLL_Read.vi"/>
			<Item Name="DrLL_Read_STB.vi" Type="VI" URL="../DriverLowLevel/DrLL_Read_STB.vi"/>
			<Item Name="DrLL_Save_Last_Setting.vi" Type="VI" URL="../DriverLowLevel/DrLL_Save_Last_Setting.vi"/>
			<Item Name="DrLL_SRQ_EN.vi" Type="VI" URL="../DriverLowLevel/DrLL_SRQ_EN.vi"/>
			<Item Name="DrLL_Wait_SRQ.vi" Type="VI" URL="../DriverLowLevel/DrLL_Wait_SRQ.vi"/>
			<Item Name="DrLL_Write.vi" Type="VI" URL="../DriverLowLevel/DrLL_Write.vi"/>
			<Item Name="DriverMode.ctl" Type="VI" URL="../DriverLowLevel/DriverMode.ctl"/>
			<Item Name="DrLL_Name.vi" Type="VI" URL="../DriverLowLevel/DrLL_Name.vi"/>
			<Item Name="DrLL_WriteSentenceWithDelay.vi" Type="VI" URL="../DriverLowLevel/DrLL_WriteSentenceWithDelay.vi"/>
			<Item Name="DrLL_InterfaceTypeFilter2.ctl" Type="VI" URL="../DriverLowLevel/DrLL_InterfaceTypeFilter2.ctl"/>
		</Item>
		<Item Name="ErrorCodesDrivers.txt" Type="Document" URL="../ErrorCodesDrivers.txt"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="Search and Replace Pattern.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Search and Replace Pattern.vi"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="Application Directory.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Application Directory.vi"/>
				<Item Name="System Exec.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/system.llb/System Exec.vi"/>
				<Item Name="MD5Checksum format message-digest.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/MD5Checksum.llb/MD5Checksum format message-digest.vi"/>
				<Item Name="MD5Checksum core.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/MD5Checksum.llb/MD5Checksum core.vi"/>
				<Item Name="MD5Checksum pad.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/MD5Checksum.llb/MD5Checksum pad.vi"/>
				<Item Name="subTimeDelay.vi" Type="VI" URL="/&lt;vilib&gt;/express/express execution control/TimeDelayBlock.llb/subTimeDelay.vi"/>
				<Item Name="General Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler.vi"/>
				<Item Name="Select Event Type.ctl" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/Select Event Type.ctl"/>
				<Item Name="Wait for RQS.vi" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/Wait for RQS.vi"/>
				<Item Name="VISA Find Search Mode.ctl" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Find Search Mode.ctl"/>
				<Item Name="VISA Open Access Mode.ctl" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Open Access Mode.ctl"/>
				<Item Name="Set Busy.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Busy.vi"/>
				<Item Name="Unset Busy.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Unset Busy.vi"/>
				<Item Name="LVBoundsTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVBoundsTypeDef.ctl"/>
				<Item Name="Get Text Rect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Get Text Rect.vi"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
				<Item Name="NI_FileType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/lvfile.llb/NI_FileType.lvlib"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="Set Cursor.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Cursor.vi"/>
				<Item Name="Set Cursor (Cursor ID).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Cursor (Cursor ID).vi"/>
				<Item Name="Set Cursor (Icon Pict).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Cursor (Icon Pict).vi"/>
				<Item Name="DialogType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogType.ctl"/>
				<Item Name="DialogTypeEnum.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogTypeEnum.ctl"/>
				<Item Name="General Error Handler Core CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler Core CORE.vi"/>
				<Item Name="Check Special Tags.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Check Special Tags.vi"/>
				<Item Name="TagReturnType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/TagReturnType.ctl"/>
				<Item Name="Set String Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set String Value.vi"/>
				<Item Name="GetRTHostConnectedProp.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetRTHostConnectedProp.vi"/>
				<Item Name="Error Code Database.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Code Database.vi"/>
				<Item Name="Format Message String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Format Message String.vi"/>
				<Item Name="Set Bold Text.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set Bold Text.vi"/>
				<Item Name="Find Tag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find Tag.vi"/>
				<Item Name="Details Display Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Details Display Dialog.vi"/>
				<Item Name="ErrWarn.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/ErrWarn.ctl"/>
				<Item Name="eventvkey.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/eventvkey.ctl"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Not Found Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Not Found Dialog.vi"/>
				<Item Name="Three Button Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog.vi"/>
				<Item Name="Three Button Dialog CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog CORE.vi"/>
				<Item Name="LVRectTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVRectTypeDef.ctl"/>
				<Item Name="Longest Line Length in Pixels.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Longest Line Length in Pixels.vi"/>
				<Item Name="Convert property node font to graphics font.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Convert property node font to graphics font.vi"/>
				<Item Name="Get String Text Bounds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Get String Text Bounds.vi"/>
				<Item Name="BuildHelpPath.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/BuildHelpPath.vi"/>
				<Item Name="GetHelpDir.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetHelpDir.vi"/>
				<Item Name="Simple Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Simple Error Handler.vi"/>
				<Item Name="Internecine Avoider.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/tcp.llb/Internecine Avoider.vi"/>
				<Item Name="TCP Listen List Operations.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/tcp.llb/TCP Listen List Operations.ctl"/>
				<Item Name="TCP Listen Internal List.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/tcp.llb/TCP Listen Internal List.vi"/>
				<Item Name="TCP Listen.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/tcp.llb/TCP Listen.vi"/>
				<Item Name="Merge Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Merge Errors.vi"/>
				<Item Name="VISA Configure Serial Port" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Configure Serial Port"/>
				<Item Name="VISA Configure Serial Port (Instr).vi" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Configure Serial Port (Instr).vi"/>
				<Item Name="VISA Configure Serial Port (Serial Instr).vi" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Configure Serial Port (Serial Instr).vi"/>
				<Item Name="Get File Extension.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Get File Extension.vi"/>
				<Item Name="Read JPEG File.vi" Type="VI" URL="/&lt;vilib&gt;/picture/jpeg.llb/Read JPEG File.vi"/>
				<Item Name="imagedata.ctl" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/imagedata.ctl"/>
				<Item Name="Check Path.vi" Type="VI" URL="/&lt;vilib&gt;/picture/jpeg.llb/Check Path.vi"/>
				<Item Name="Directory of Top Level VI.vi" Type="VI" URL="/&lt;vilib&gt;/picture/jpeg.llb/Directory of Top Level VI.vi"/>
				<Item Name="Read BMP File.vi" Type="VI" URL="/&lt;vilib&gt;/picture/bmp.llb/Read BMP File.vi"/>
				<Item Name="Read BMP File Data.vi" Type="VI" URL="/&lt;vilib&gt;/picture/bmp.llb/Read BMP File Data.vi"/>
				<Item Name="Read BMP Header Info.vi" Type="VI" URL="/&lt;vilib&gt;/picture/bmp.llb/Read BMP Header Info.vi"/>
				<Item Name="Calc Long Word Padded Width.vi" Type="VI" URL="/&lt;vilib&gt;/picture/bmp.llb/Calc Long Word Padded Width.vi"/>
				<Item Name="Flip and Pad for Picture Control.vi" Type="VI" URL="/&lt;vilib&gt;/picture/bmp.llb/Flip and Pad for Picture Control.vi"/>
				<Item Name="Read PNG File.vi" Type="VI" URL="/&lt;vilib&gt;/picture/png.llb/Read PNG File.vi"/>
				<Item Name="Create Mask By Alpha.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Create Mask By Alpha.vi"/>
				<Item Name="Bit-array To Byte-array.vi" Type="VI" URL="/&lt;vilib&gt;/picture/pictutil.llb/Bit-array To Byte-array.vi"/>
				<Item Name="Draw Flattened Pixmap.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Draw Flattened Pixmap.vi"/>
				<Item Name="FixBadRect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/pictutil.llb/FixBadRect.vi"/>
				<Item Name="Play Sound File.vi" Type="VI" URL="/&lt;vilib&gt;/sound2/lvsound2.llb/Play Sound File.vi"/>
				<Item Name="Sound Output Task ID.ctl" Type="VI" URL="/&lt;vilib&gt;/sound2/lvsound2.llb/Sound Output Task ID.ctl"/>
				<Item Name="Path To Command Line String.vi" Type="VI" URL="/&lt;vilib&gt;/AdvancedString/Path To Command Line String.vi"/>
				<Item Name="PathToUNIXPathString.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/CFURL.llb/PathToUNIXPathString.vi"/>
				<Item Name="_Get Sound Error From Return Value.vi" Type="VI" URL="/&lt;vilib&gt;/sound2/lvsound2.llb/_Get Sound Error From Return Value.vi"/>
				<Item Name="Sound Output Wait.vi" Type="VI" URL="/&lt;vilib&gt;/sound2/lvsound2.llb/Sound Output Wait.vi"/>
				<Item Name="Initialize Mouse.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/inputDevices.llb/Initialize Mouse.vi"/>
				<Item Name="errorList.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/inputDevices.llb/errorList.vi"/>
				<Item Name="ErrorDescriptions.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/inputDevices.llb/ErrorDescriptions.vi"/>
				<Item Name="Acquire Input Data.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/inputDevices.llb/Acquire Input Data.vi"/>
				<Item Name="joystickAcquire.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/inputDevices.llb/joystickAcquire.vi"/>
				<Item Name="keyboardAcquire.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/inputDevices.llb/keyboardAcquire.vi"/>
				<Item Name="mouseAcquire.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/inputDevices.llb/mouseAcquire.vi"/>
				<Item Name="Close Input Device.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/inputDevices.llb/Close Input Device.vi"/>
				<Item Name="closeJoystick.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/inputDevices.llb/closeJoystick.vi"/>
				<Item Name="closeKeyboard.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/inputDevices.llb/closeKeyboard.vi"/>
				<Item Name="closeMouse.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/inputDevices.llb/closeMouse.vi"/>
				<Item Name="Space Constant.vi" Type="VI" URL="/&lt;vilib&gt;/dlg_ctls.llb/Space Constant.vi"/>
				<Item Name="LVComboBoxStrsAndValuesArrayTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVComboBoxStrsAndValuesArrayTypeDef.ctl"/>
				<Item Name="Beep.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/system.llb/Beep.vi"/>
				<Item Name="NI_Database_API.lvlib" Type="Library" URL="/&lt;vilib&gt;/addons/database/NI_Database_API.lvlib"/>
				<Item Name="GOOP Object Repository Method.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/_goopsup.llb/GOOP Object Repository Method.ctl"/>
				<Item Name="GOOP Object Repository Statistics.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/_goopsup.llb/GOOP Object Repository Statistics.ctl"/>
				<Item Name="GOOP Object Repository.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/_goopsup.llb/GOOP Object Repository.vi"/>
				<Item Name="Check if File or Folder Exists.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Check if File or Folder Exists.vi"/>
				<Item Name="NI_PackedLibraryUtility.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/LVLibp/NI_PackedLibraryUtility.lvlib"/>
				<Item Name="NI_LVConfig.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/config.llb/NI_LVConfig.lvlib"/>
				<Item Name="8.6CompatibleGlobalVar.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/config.llb/8.6CompatibleGlobalVar.vi"/>
				<Item Name="LVPositionTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVPositionTypeDef.ctl"/>
				<Item Name="subFile Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/express/express input/FileDialogBlock.llb/subFile Dialog.vi"/>
				<Item Name="ex_CorrectErrorChain.vi" Type="VI" URL="/&lt;vilib&gt;/express/express shared/ex_CorrectErrorChain.vi"/>
				<Item Name="Registry RtKey.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry RtKey.ctl"/>
				<Item Name="Open Registry Key.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Open Registry Key.vi"/>
				<Item Name="Registry SAM.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry SAM.ctl"/>
				<Item Name="Registry refnum.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry refnum.ctl"/>
				<Item Name="Registry View.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry View.ctl"/>
				<Item Name="STR_ASCII-Unicode.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/STR_ASCII-Unicode.vi"/>
				<Item Name="Registry WinErr-LVErr.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry WinErr-LVErr.vi"/>
				<Item Name="Registry Handle Master.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry Handle Master.vi"/>
				<Item Name="Write Registry Value Simple.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Write Registry Value Simple.vi"/>
				<Item Name="Write Registry Value Simple STR.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Write Registry Value Simple STR.vi"/>
				<Item Name="Write Registry Value.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Write Registry Value.vi"/>
				<Item Name="Write Registry Value STR.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Write Registry Value STR.vi"/>
				<Item Name="Write Registry Value DWORD.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Write Registry Value DWORD.vi"/>
				<Item Name="Write Registry Value Simple U32.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Write Registry Value Simple U32.vi"/>
				<Item Name="Close Registry Key.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Close Registry Key.vi"/>
				<Item Name="Delete Registry Value.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Delete Registry Value.vi"/>
				<Item Name="Read Registry Value Simple.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value Simple.vi"/>
				<Item Name="Read Registry Value Simple STR.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value Simple STR.vi"/>
				<Item Name="Read Registry Value.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value.vi"/>
				<Item Name="Read Registry Value STR.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value STR.vi"/>
				<Item Name="Read Registry Value DWORD.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value DWORD.vi"/>
				<Item Name="Registry Simplify Data Type.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry Simplify Data Type.vi"/>
				<Item Name="Read Registry Value Simple U32.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value Simple U32.vi"/>
				<Item Name="Write to XML File.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/Write to XML File.vi"/>
				<Item Name="Write to XML File(string).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/Write to XML File(string).vi"/>
				<Item Name="Open_Create_Replace File.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/Open_Create_Replace File.vi"/>
				<Item Name="compatFileDialog.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatFileDialog.vi"/>
				<Item Name="compatOpenFileOperation.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatOpenFileOperation.vi"/>
				<Item Name="compatCalcOffset.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatCalcOffset.vi"/>
				<Item Name="Write to XML File(array).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/Write to XML File(array).vi"/>
				<Item Name="Read From XML File.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/Read From XML File.vi"/>
				<Item Name="Read From XML File(array).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/Read From XML File(array).vi"/>
				<Item Name="ParseXMLFragments.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/ParseXMLFragments.vi"/>
				<Item Name="FindFirstTag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/FindFirstTag.vi"/>
				<Item Name="FindEmptyElement.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/FindEmptyElement.vi"/>
				<Item Name="FindElement.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/FindElement.vi"/>
				<Item Name="FindMatchingCloseTag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/FindMatchingCloseTag.vi"/>
				<Item Name="FindCloseTagByName.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/FindCloseTagByName.vi"/>
				<Item Name="FindElementStartByName.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/FindElementStartByName.vi"/>
				<Item Name="Read From XML File(string).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/Read From XML File(string).vi"/>
				<Item Name="NI_MAPro.lvlib" Type="Library" URL="/&lt;vilib&gt;/measure/NI_MAPro.lvlib"/>
				<Item Name="NI_MABase.lvlib" Type="Library" URL="/&lt;vilib&gt;/measure/NI_MABase.lvlib"/>
			</Item>
			<Item Name="user.lib" Type="Folder">
				<Item Name="Sort Array__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort Array__ogtk.vi"/>
				<Item Name="Sort 1D Array (CDB)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 1D Array (CDB)__ogtk.vi"/>
				<Item Name="Reorder 1D Array (CDB)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 1D Array (CDB)__ogtk.vi"/>
				<Item Name="Sort 1D Array (CXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 1D Array (CXT)__ogtk.vi"/>
				<Item Name="Reorder 1D Array (CXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 1D Array (CXT)__ogtk.vi"/>
				<Item Name="Sort 1D Array (CSG)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 1D Array (CSG)__ogtk.vi"/>
				<Item Name="Reorder 1D Array (CSG)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 1D Array (CSG)__ogtk.vi"/>
				<Item Name="Sort 1D Array (DBL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 1D Array (DBL)__ogtk.vi"/>
				<Item Name="Sort 1D Array (EXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 1D Array (EXT)__ogtk.vi"/>
				<Item Name="Sort 1D Array (I16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 1D Array (I16)__ogtk.vi"/>
				<Item Name="Sort 1D Array (I32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 1D Array (I32)__ogtk.vi"/>
				<Item Name="Sort 1D Array (I8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 1D Array (I8)__ogtk.vi"/>
				<Item Name="Sort 1D Array (SGL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 1D Array (SGL)__ogtk.vi"/>
				<Item Name="Sort 1D Array (U16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 1D Array (U16)__ogtk.vi"/>
				<Item Name="Sort 1D Array (U32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 1D Array (U32)__ogtk.vi"/>
				<Item Name="Sort 1D Array (U8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 1D Array (U8)__ogtk.vi"/>
				<Item Name="Sort 2D Array (CDB)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 2D Array (CDB)__ogtk.vi"/>
				<Item Name="Sort 2D Array (CXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 2D Array (CXT)__ogtk.vi"/>
				<Item Name="Sort 2D Array (CSG)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 2D Array (CSG)__ogtk.vi"/>
				<Item Name="Sort 2D Array (DBL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 2D Array (DBL)__ogtk.vi"/>
				<Item Name="Sort 2D Array (EXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 2D Array (EXT)__ogtk.vi"/>
				<Item Name="Sort 2D Array (I16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 2D Array (I16)__ogtk.vi"/>
				<Item Name="Sort 2D Array (I32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 2D Array (I32)__ogtk.vi"/>
				<Item Name="Sort 2D Array (I8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 2D Array (I8)__ogtk.vi"/>
				<Item Name="Sort 2D Array (SGL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 2D Array (SGL)__ogtk.vi"/>
				<Item Name="Sort 2D Array (U16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 2D Array (U16)__ogtk.vi"/>
				<Item Name="Sort 2D Array (U32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 2D Array (U32)__ogtk.vi"/>
				<Item Name="Sort 2D Array (U8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 2D Array (U8)__ogtk.vi"/>
				<Item Name="Sort 1D Array (Path)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 1D Array (Path)__ogtk.vi"/>
				<Item Name="Sort 1D Array (String)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 1D Array (String)__ogtk.vi"/>
				<Item Name="Sort 2D Array (Path)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 2D Array (Path)__ogtk.vi"/>
				<Item Name="Sort 2D Array (String)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 2D Array (String)__ogtk.vi"/>
				<Item Name="Sort 1D Array (I64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 1D Array (I64)__ogtk.vi"/>
				<Item Name="Sort 1D Array (U64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 1D Array (U64)__ogtk.vi"/>
				<Item Name="Sort 2D Array (I64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 2D Array (I64)__ogtk.vi"/>
				<Item Name="Sort 2D Array (U64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 2D Array (U64)__ogtk.vi"/>
				<Item Name="Filter 1D Array__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array__ogtk.vi"/>
				<Item Name="Filter 1D Array (String)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array (String)__ogtk.vi"/>
				<Item Name="Remove Duplicates from 1D Array__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array__ogtk.vi"/>
				<Item Name="Remove Duplicates from 1D Array (Path)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array (Path)__ogtk.vi"/>
				<Item Name="Remove Duplicates from 1D Array (CDB)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array (CDB)__ogtk.vi"/>
				<Item Name="Remove Duplicates from 1D Array (CSG)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array (CSG)__ogtk.vi"/>
				<Item Name="Remove Duplicates from 1D Array (CXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array (CXT)__ogtk.vi"/>
				<Item Name="Remove Duplicates from 1D Array (DBL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array (DBL)__ogtk.vi"/>
				<Item Name="Remove Duplicates from 1D Array (EXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array (EXT)__ogtk.vi"/>
				<Item Name="Remove Duplicates from 1D Array (I16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array (I16)__ogtk.vi"/>
				<Item Name="Remove Duplicates from 1D Array (I32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array (I32)__ogtk.vi"/>
				<Item Name="Remove Duplicates from 1D Array (I8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array (I8)__ogtk.vi"/>
				<Item Name="Remove Duplicates from 1D Array (SGL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array (SGL)__ogtk.vi"/>
				<Item Name="Remove Duplicates from 1D Array (String)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array (String)__ogtk.vi"/>
				<Item Name="Remove Duplicates from 1D Array (U16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array (U16)__ogtk.vi"/>
				<Item Name="Remove Duplicates from 1D Array (U32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array (U32)__ogtk.vi"/>
				<Item Name="Remove Duplicates from 1D Array (U8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array (U8)__ogtk.vi"/>
				<Item Name="Remove Duplicates from 1D Array (Variant)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array (Variant)__ogtk.vi"/>
				<Item Name="Remove Duplicates from 1D Array (Boolean)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array (Boolean)__ogtk.vi"/>
				<Item Name="Remove Duplicates from 1D Array (I64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array (I64)__ogtk.vi"/>
				<Item Name="Remove Duplicates from 1D Array (U64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array (U64)__ogtk.vi"/>
				<Item Name="Remove Duplicates from 1D Array (LVObject)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array (LVObject)__ogtk.vi"/>
				<Item Name="Search Array__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Search Array__ogtk.vi"/>
				<Item Name="Search 1D Array (SGL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Search 1D Array (SGL)__ogtk.vi"/>
				<Item Name="Search 1D Array (DBL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Search 1D Array (DBL)__ogtk.vi"/>
				<Item Name="Search 1D Array (EXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Search 1D Array (EXT)__ogtk.vi"/>
				<Item Name="Search 1D Array (CSG)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Search 1D Array (CSG)__ogtk.vi"/>
				<Item Name="Search 1D Array (CDB)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Search 1D Array (CDB)__ogtk.vi"/>
				<Item Name="Search 1D Array (CXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Search 1D Array (CXT)__ogtk.vi"/>
				<Item Name="Search 1D Array (I8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Search 1D Array (I8)__ogtk.vi"/>
				<Item Name="Search 1D Array (I16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Search 1D Array (I16)__ogtk.vi"/>
				<Item Name="Search 1D Array (I32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Search 1D Array (I32)__ogtk.vi"/>
				<Item Name="Search 1D Array (U8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Search 1D Array (U8)__ogtk.vi"/>
				<Item Name="Search 1D Array (U16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Search 1D Array (U16)__ogtk.vi"/>
				<Item Name="Search 1D Array (U32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Search 1D Array (U32)__ogtk.vi"/>
				<Item Name="Search 1D Array (Boolean)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Search 1D Array (Boolean)__ogtk.vi"/>
				<Item Name="Search 1D Array (String)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Search 1D Array (String)__ogtk.vi"/>
				<Item Name="Search 1D Array (Path)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Search 1D Array (Path)__ogtk.vi"/>
				<Item Name="Search 1D Array (Variant)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Search 1D Array (Variant)__ogtk.vi"/>
				<Item Name="Search 1D Array (U64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Search 1D Array (U64)__ogtk.vi"/>
				<Item Name="Search 1D Array (I64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Search 1D Array (I64)__ogtk.vi"/>
				<Item Name="Search 1D Array (LVObject)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Search 1D Array (LVObject)__ogtk.vi"/>
				<Item Name="Delete Elements from Array__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from Array__ogtk.vi"/>
				<Item Name="Delete Elements from 1D Array (Boolean)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 1D Array (Boolean)__ogtk.vi"/>
				<Item Name="Reorder Array2__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder Array2__ogtk.vi"/>
				<Item Name="Reorder 1D Array2 (CDB)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 1D Array2 (CDB)__ogtk.vi"/>
				<Item Name="Reorder 1D Array2 (CXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 1D Array2 (CXT)__ogtk.vi"/>
				<Item Name="Reorder 1D Array2 (CSG)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 1D Array2 (CSG)__ogtk.vi"/>
				<Item Name="Reorder 1D Array2 (DBL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 1D Array2 (DBL)__ogtk.vi"/>
				<Item Name="Reorder 1D Array2 (EXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 1D Array2 (EXT)__ogtk.vi"/>
				<Item Name="Reorder 1D Array2 (I16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 1D Array2 (I16)__ogtk.vi"/>
				<Item Name="Reorder 1D Array2 (I32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 1D Array2 (I32)__ogtk.vi"/>
				<Item Name="Reorder 1D Array2 (I8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 1D Array2 (I8)__ogtk.vi"/>
				<Item Name="Reorder 1D Array2 (SGL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 1D Array2 (SGL)__ogtk.vi"/>
				<Item Name="Reorder 1D Array2 (U16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 1D Array2 (U16)__ogtk.vi"/>
				<Item Name="Reorder 1D Array2 (U32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 1D Array2 (U32)__ogtk.vi"/>
				<Item Name="Reorder 1D Array2 (U8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 1D Array2 (U8)__ogtk.vi"/>
				<Item Name="Reorder 1D Array2 (Boolean)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 1D Array2 (Boolean)__ogtk.vi"/>
				<Item Name="Reorder 1D Array2 (String)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 1D Array2 (String)__ogtk.vi"/>
				<Item Name="Reorder 1D Array2 (Path)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 1D Array2 (Path)__ogtk.vi"/>
				<Item Name="Reorder 1D Array2 (Variant)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 1D Array2 (Variant)__ogtk.vi"/>
				<Item Name="Reorder 2D Array2 (Boolean)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 2D Array2 (Boolean)__ogtk.vi"/>
				<Item Name="Reorder 2D Array2 (CDB)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 2D Array2 (CDB)__ogtk.vi"/>
				<Item Name="Reorder 2D Array2 (CSG)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 2D Array2 (CSG)__ogtk.vi"/>
				<Item Name="Reorder 2D Array2 (CXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 2D Array2 (CXT)__ogtk.vi"/>
				<Item Name="Reorder 2D Array2 (DBL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 2D Array2 (DBL)__ogtk.vi"/>
				<Item Name="Reorder 2D Array2 (EXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 2D Array2 (EXT)__ogtk.vi"/>
				<Item Name="Reorder 2D Array2 (I8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 2D Array2 (I8)__ogtk.vi"/>
				<Item Name="Reorder 2D Array2 (I16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 2D Array2 (I16)__ogtk.vi"/>
				<Item Name="Reorder 2D Array2 (I32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 2D Array2 (I32)__ogtk.vi"/>
				<Item Name="Reorder 2D Array2 (Path)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 2D Array2 (Path)__ogtk.vi"/>
				<Item Name="Reorder 2D Array2 (SGL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 2D Array2 (SGL)__ogtk.vi"/>
				<Item Name="Reorder 2D Array2 (String)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 2D Array2 (String)__ogtk.vi"/>
				<Item Name="Reorder 2D Array2 (U8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 2D Array2 (U8)__ogtk.vi"/>
				<Item Name="Reorder 2D Array2 (U16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 2D Array2 (U16)__ogtk.vi"/>
				<Item Name="Reorder 2D Array2 (U32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 2D Array2 (U32)__ogtk.vi"/>
				<Item Name="Reorder 2D Array2 (Variant)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 2D Array2 (Variant)__ogtk.vi"/>
				<Item Name="Reorder 1D Array2 (I64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 1D Array2 (I64)__ogtk.vi"/>
				<Item Name="Reorder 1D Array2 (U64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 1D Array2 (U64)__ogtk.vi"/>
				<Item Name="Reorder 2D Array2 (I64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 2D Array2 (I64)__ogtk.vi"/>
				<Item Name="Reorder 2D Array2 (U64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 2D Array2 (U64)__ogtk.vi"/>
				<Item Name="Reorder 1D Array2 (LVObject)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 1D Array2 (LVObject)__ogtk.vi"/>
				<Item Name="Reorder 2D Array2 (LVObject)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 2D Array2 (LVObject)__ogtk.vi"/>
				<Item Name="Delete Elements from 1D Array (CDB)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 1D Array (CDB)__ogtk.vi"/>
				<Item Name="Delete Elements from 1D Array (CSG)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 1D Array (CSG)__ogtk.vi"/>
				<Item Name="Delete Elements from 1D Array (DBL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 1D Array (DBL)__ogtk.vi"/>
				<Item Name="Delete Elements from 1D Array (CXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 1D Array (CXT)__ogtk.vi"/>
				<Item Name="Delete Elements from 1D Array (EXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 1D Array (EXT)__ogtk.vi"/>
				<Item Name="Delete Elements from 1D Array (I8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 1D Array (I8)__ogtk.vi"/>
				<Item Name="Delete Elements from 1D Array (I16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 1D Array (I16)__ogtk.vi"/>
				<Item Name="Delete Elements from 1D Array (I32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 1D Array (I32)__ogtk.vi"/>
				<Item Name="Delete Elements from 1D Array (Path)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 1D Array (Path)__ogtk.vi"/>
				<Item Name="Delete Elements from 1D Array (SGL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 1D Array (SGL)__ogtk.vi"/>
				<Item Name="Delete Elements from 1D Array (String)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 1D Array (String)__ogtk.vi"/>
				<Item Name="Delete Elements from 1D Array (U8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 1D Array (U8)__ogtk.vi"/>
				<Item Name="Delete Elements from 1D Array (U16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 1D Array (U16)__ogtk.vi"/>
				<Item Name="Delete Elements from 1D Array (U32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 1D Array (U32)__ogtk.vi"/>
				<Item Name="Delete Elements from 1D Array (Variant)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 1D Array (Variant)__ogtk.vi"/>
				<Item Name="Delete Elements from 2D Array (Boolean)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 2D Array (Boolean)__ogtk.vi"/>
				<Item Name="Delete Elements from 2D Array (CDB)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 2D Array (CDB)__ogtk.vi"/>
				<Item Name="Delete Elements from 2D Array (CSG)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 2D Array (CSG)__ogtk.vi"/>
				<Item Name="Delete Elements from 2D Array (CXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 2D Array (CXT)__ogtk.vi"/>
				<Item Name="Delete Elements from 2D Array (DBL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 2D Array (DBL)__ogtk.vi"/>
				<Item Name="Delete Elements from 2D Array (EXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 2D Array (EXT)__ogtk.vi"/>
				<Item Name="Delete Elements from 2D Array (I8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 2D Array (I8)__ogtk.vi"/>
				<Item Name="Delete Elements from 2D Array (I16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 2D Array (I16)__ogtk.vi"/>
				<Item Name="Delete Elements from 2D Array (I32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 2D Array (I32)__ogtk.vi"/>
				<Item Name="Delete Elements from 2D Array (Path)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 2D Array (Path)__ogtk.vi"/>
				<Item Name="Delete Elements from 2D Array (SGL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 2D Array (SGL)__ogtk.vi"/>
				<Item Name="Delete Elements from 2D Array (String)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 2D Array (String)__ogtk.vi"/>
				<Item Name="Delete Elements from 2D Array (U8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 2D Array (U8)__ogtk.vi"/>
				<Item Name="Delete Elements from 2D Array (U16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 2D Array (U16)__ogtk.vi"/>
				<Item Name="Delete Elements from 2D Array (U32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 2D Array (U32)__ogtk.vi"/>
				<Item Name="Delete Elements from 2D Array (Variant)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 2D Array (Variant)__ogtk.vi"/>
				<Item Name="Delete Elements from 1D Array (I64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 1D Array (I64)__ogtk.vi"/>
				<Item Name="Delete Elements from 1D Array (U64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 1D Array (U64)__ogtk.vi"/>
				<Item Name="Delete Elements from 2D Array (I64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 2D Array (I64)__ogtk.vi"/>
				<Item Name="Delete Elements from 2D Array (U64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 2D Array (U64)__ogtk.vi"/>
				<Item Name="Delete Elements from 1D Array (LVObject)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 1D Array (LVObject)__ogtk.vi"/>
				<Item Name="Delete Elements from 2D Array (LVObject)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 2D Array (LVObject)__ogtk.vi"/>
				<Item Name="Filter 1D Array (U8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array (U8)__ogtk.vi"/>
				<Item Name="Filter 1D Array (Path)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array (Path)__ogtk.vi"/>
				<Item Name="Filter 1D Array (U32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array (U32)__ogtk.vi"/>
				<Item Name="Filter 1D Array (U16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array (U16)__ogtk.vi"/>
				<Item Name="Filter 1D Array (I16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array (I16)__ogtk.vi"/>
				<Item Name="Filter 1D Array (I32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array (I32)__ogtk.vi"/>
				<Item Name="Filter 1D Array (I8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array (I8)__ogtk.vi"/>
				<Item Name="Filter 1D Array (Variant)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array (Variant)__ogtk.vi"/>
				<Item Name="Filter 1D Array (Boolean)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array (Boolean)__ogtk.vi"/>
				<Item Name="Filter 1D Array (CXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array (CXT)__ogtk.vi"/>
				<Item Name="Filter 1D Array (CDB)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array (CDB)__ogtk.vi"/>
				<Item Name="Filter 1D Array (CSG)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array (CSG)__ogtk.vi"/>
				<Item Name="Filter 1D Array (EXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array (EXT)__ogtk.vi"/>
				<Item Name="Filter 1D Array (DBL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array (DBL)__ogtk.vi"/>
				<Item Name="Filter 1D Array (SGL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array (SGL)__ogtk.vi"/>
				<Item Name="Filter 1D Array with Scalar (Boolean)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array with Scalar (Boolean)__ogtk.vi"/>
				<Item Name="Filter 1D Array with Scalar (CDB)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array with Scalar (CDB)__ogtk.vi"/>
				<Item Name="Filter 1D Array with Scalar (CSG)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array with Scalar (CSG)__ogtk.vi"/>
				<Item Name="Filter 1D Array with Scalar (CXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array with Scalar (CXT)__ogtk.vi"/>
				<Item Name="Filter 1D Array with Scalar (DBL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array with Scalar (DBL)__ogtk.vi"/>
				<Item Name="Filter 1D Array with Scalar (EXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array with Scalar (EXT)__ogtk.vi"/>
				<Item Name="Filter 1D Array with Scalar (I8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array with Scalar (I8)__ogtk.vi"/>
				<Item Name="Filter 1D Array with Scalar (I16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array with Scalar (I16)__ogtk.vi"/>
				<Item Name="Filter 1D Array with Scalar (I32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array with Scalar (I32)__ogtk.vi"/>
				<Item Name="Filter 1D Array with Scalar (Path)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array with Scalar (Path)__ogtk.vi"/>
				<Item Name="Filter 1D Array with Scalar (SGL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array with Scalar (SGL)__ogtk.vi"/>
				<Item Name="Filter 1D Array with Scalar (String)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array with Scalar (String)__ogtk.vi"/>
				<Item Name="Filter 1D Array with Scalar (U8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array with Scalar (U8)__ogtk.vi"/>
				<Item Name="Filter 1D Array with Scalar (U16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array with Scalar (U16)__ogtk.vi"/>
				<Item Name="Filter 1D Array with Scalar (U32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array with Scalar (U32)__ogtk.vi"/>
				<Item Name="Filter 1D Array with Scalar (Variant)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array with Scalar (Variant)__ogtk.vi"/>
				<Item Name="Filter 1D Array (I64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array (I64)__ogtk.vi"/>
				<Item Name="Filter 1D Array (U64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array (U64)__ogtk.vi"/>
				<Item Name="Filter 1D Array with Scalar (I64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array with Scalar (I64)__ogtk.vi"/>
				<Item Name="Filter 1D Array with Scalar (U64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array with Scalar (U64)__ogtk.vi"/>
				<Item Name="Filter 1D Array (LVObject)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array (LVObject)__ogtk.vi"/>
				<Item Name="Filter 1D Array with Scalar (LVObject)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array with Scalar (LVObject)__ogtk.vi"/>
				<Item Name="Build Error Cluster__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/error/error.llb/Build Error Cluster__ogtk.vi"/>
			</Item>
			<Item Name="Get MAC Address.vi" Type="VI" URL="../Auxiliary/Get MAC Address.vi"/>
			<Item Name="MD5Checksum.vi" Type="VI" URL="../Auxiliary/MD5Checksum.vi"/>
			<Item Name="Exp2_Postfix_point(SubVI).vi" Type="VI" URL="../Auxiliary/Exp2_Postfix_point(SubVI).vi"/>
			<Item Name="UTDB_filename_dialoge.vi" Type="VI" URL="../Auxiliary/UTDB_filename_dialoge.vi"/>
			<Item Name="System.Management" Type="Document" URL="System.Management">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="Postfix2expV2_point(SubVI).vi" Type="VI" URL="../Auxiliary/Postfix2expV2_point(SubVI).vi"/>
			<Item Name="DrLL_RS232_settings.ctl" Type="VI" URL="../DriverLowLevel/DrLL_RS232_settings.ctl"/>
			<Item Name="DrLL_Prologix_settings.ctl" Type="VI" URL="../DriverLowLevel/DrLL_Prologix_settings.ctl"/>
			<Item Name="DrLL_InterfaceTypeFilter.ctl" Type="VI" URL="../DriverLowLevel/DrLL_InterfaceTypeFilter.ctl"/>
			<Item Name="DrLL_array_parameters2str.vi" Type="VI" URL="../DriverLowLevel/DrLL_array_parameters2str.vi"/>
			<Item Name="DrLL_PhysicalInterfaceKind.ctl" Type="VI" URL="../DriverLowLevel/DrLL_PhysicalInterfaceKind.ctl"/>
			<Item Name="mscorlib" Type="VI" URL="mscorlib">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="UT_delimeter_data.vi" Type="VI" URL="../Auxiliary/UT_delimeter_data.vi"/>
			<Item Name="B7_53_Done.vi" Type="VI" URL="../Drivers/dpi4 map B7_53/B7_53_Done.vi"/>
			<Item Name="B7_53_Check_Error.vi" Type="VI" URL="../Drivers/dpi4 map B7_53/B7_53_Check_Error.vi"/>
			<Item Name="B7_53_StandartRangeSelect (SubVI).vi" Type="VI" URL="../Drivers/dpi4 map B7_53/B7_53_StandartRangeSelect (SubVI).vi"/>
			<Item Name="DrLL_IS_DLL.vi" Type="VI" URL="../DriverLowLevel/DrLL_IS_DLL.vi"/>
			<Item Name="DrLL_Simulate.vi" Type="VI" URL="../DriverLowLevel/DrLL_Simulate.vi"/>
			<Item Name="DrLL_Message.vi" Type="VI" URL="../DriverLowLevel/DrLL_Message.vi"/>
			<Item Name="SyntaxCheck (SubVI).vi" Type="VI" URL="../Auxiliary/SyntaxCheck (SubVI).vi"/>
			<Item Name="lvsound2.dll" Type="Document" URL="/&lt;resource&gt;/lvsound2.dll"/>
			<Item Name="B7_54_StandartRangeSelect (SubVI).vi" Type="VI" URL="../Drivers/dpi4 map B7_54/B7_54_StandartRangeSelect (SubVI).vi"/>
			<Item Name="B7_54_WriteAndCheck.vi" Type="VI" URL="../Drivers/dpi4 map B7_54/B7_54_WriteAndCheck.vi"/>
			<Item Name="B7_54_GPIB_Check_STB_4_Error.vi" Type="VI" URL="../Drivers/dpi4 map B7_54/B7_54_GPIB_Check_STB_4_Error.vi"/>
			<Item Name="B7_54_GPIB_Init.vi" Type="VI" URL="../Drivers/dpi4 map B7_54/B7_54_GPIB_Init.vi"/>
			<Item Name="B7_64_write.vi" Type="VI" URL="../Drivers/dpi4 map B7_64/B7_64_write.vi"/>
			<Item Name="B7_64_readstring.vi" Type="VI" URL="../Drivers/dpi4 map B7_64/B7_64_readstring.vi"/>
			<Item Name="B7_64M_ESR_check.vi" Type="VI" URL="../Drivers/dpi4 map B7_64M/B7_64M_ESR_check.vi"/>
			<Item Name="B764M_ESR_check.vi" Type="VI" URL="../Drivers/dpi4 map B7_64M/B764M_ESR_check.vi"/>
			<Item Name="B7_65_WriteAndCheck.vi" Type="VI" URL="../Drivers/dpi4 map B7_65/B7_65_WriteAndCheck.vi"/>
			<Item Name="B7_65_GPIB_Check_STB_4_Error.vi" Type="VI" URL="../Drivers/dpi4 map B7_65/B7_65_GPIB_Check_STB_4_Error.vi"/>
			<Item Name="B7_65_ErrorAnswer.vi" Type="VI" URL="../Drivers/dpi4 map B7_65/B7_65_ErrorAnswer.vi"/>
			<Item Name="B7_65_GPIB_Init.vi" Type="VI" URL="../Drivers/dpi4 map B7_65/B7_65_GPIB_Init.vi"/>
			<Item Name="B7_65_Check_Init_STB_4_Error.vi" Type="VI" URL="../Drivers/dpi4 map B7_65/B7_65_Check_Init_STB_4_Error.vi"/>
			<Item Name="B7_65_StandartRangeSelect (SubVI).vi" Type="VI" URL="../Drivers/dpi4 map B7_65/B7_65_StandartRangeSelect (SubVI).vi"/>
			<Item Name="B7_68_WriteWithCheck.vi" Type="VI" URL="../Drivers/dpi4 map B7_68/B7_68_WriteWithCheck.vi"/>
			<Item Name="B7_68_Query.vi" Type="VI" URL="../Drivers/dpi4 map B7_68/B7_68_Query.vi"/>
			<Item Name="B7_68_WriteSentenceWithDelay.vi" Type="VI" URL="../Drivers/dpi4 map B7_68/B7_68_WriteSentenceWithDelay.vi"/>
			<Item Name="B7_68_Read.vi" Type="VI" URL="../Drivers/dpi4 map B7_68/B7_68_Read.vi"/>
			<Item Name="B7_68_address_init.vi" Type="VI" URL="../Drivers/dpi4 map B7_68/B7_68_address_init.vi"/>
			<Item Name="B7_68_StandartRangeSelect (SubVI).vi" Type="VI" URL="../Drivers/dpi4 map B7_68/B7_68_StandartRangeSelect (SubVI).vi"/>
			<Item Name="B7_68_READ_math.vi" Type="VI" URL="../Drivers/dpi4 map B7_68/B7_68_READ_math.vi"/>
			<Item Name="B7_72_CheckMode.vi" Type="VI" URL="../Drivers/dpi4 map B7_72/B7_72_CheckMode.vi"/>
			<Item Name="B7_73_WriteAndCheck.vi" Type="VI" URL="../Drivers/dpi4 map B7_73/B7_73_WriteAndCheck.vi"/>
			<Item Name="B7_73_GPIB_Check_STB_4_Error.vi" Type="VI" URL="../Drivers/dpi4 map B7_73/B7_73_GPIB_Check_STB_4_Error.vi"/>
			<Item Name="B7_73_ErrorAnswer.vi" Type="VI" URL="../Drivers/dpi4 map B7_73/B7_73_ErrorAnswer.vi"/>
			<Item Name="B7_73_GPIB_Init.vi" Type="VI" URL="../Drivers/dpi4 map B7_73/B7_73_GPIB_Init.vi"/>
			<Item Name="B7_73_Check_Init_STB_4_Error.vi" Type="VI" URL="../Drivers/dpi4 map B7_73/B7_73_Check_Init_STB_4_Error.vi"/>
			<Item Name="B7_73_StandartRangeSelect (SubVI).vi" Type="VI" URL="../Drivers/dpi4 map B7_73/B7_73_StandartRangeSelect (SubVI).vi"/>
			<Item Name="B7_74_ESR_check.vi" Type="VI" URL="../Drivers/dpi4 map B7_74/B7_74_ESR_check.vi"/>
			<Item Name="Functions.ctl" Type="VI" URL="../Drivers/dpi4 map B7_78/Functions.ctl"/>
			<Item Name="B7_78_ESR_check.vi" Type="VI" URL="../Drivers/dpi4 map B7_78/B7_78_ESR_check.vi"/>
			<Item Name="B7_80_readstring.vi" Type="VI" URL="../Drivers/dpi4 map B7_80/B7_80_readstring.vi"/>
			<Item Name="B7_80_write.vi" Type="VI" URL="../Drivers/dpi4 map B7_80/B7_80_write.vi"/>
			<Item Name="B7_82_CheckMode.vi" Type="VI" URL="../Drivers/dpi4 map B7_82/B7_82_CheckMode.vi"/>
			<Item Name="B7_84_write.vi" Type="VI" URL="../Drivers/dpi4 map B7_84/B7_84_write.vi"/>
			<Item Name="B7_84_readstring.vi" Type="VI" URL="../Drivers/dpi4 map B7_84/B7_84_readstring.vi"/>
			<Item Name="B7_89_Read.vi" Type="VI" URL="../Drivers/dpi4 map B7_89/B7_89_Read.vi"/>
			<Item Name="B7_72_82_89_StandartRangeSelect (SubVI).vi" Type="VI" URL="../Drivers/dpi4 map B7_89/B7_72_82_89_StandartRangeSelect (SubVI).vi"/>
			<Item Name="B7_89_FormatValue.vi" Type="VI" URL="../Drivers/dpi4 map B7_89/B7_89_FormatValue.vi"/>
			<Item Name="9100_OUTstate_OFF.vi" Type="VI" URL="../Drivers/dpi4 map Fluke9100/9100_OUTstate_OFF.vi"/>
			<Item Name="F9100_ESR_check.vi" Type="VI" URL="../Drivers/dpi4 map Fluke9100/F9100_ESR_check.vi"/>
			<Item Name="9100_ESR_OUTstate_ON.vi" Type="VI" URL="../Drivers/dpi4 map Fluke9100/9100_ESR_OUTstate_ON.vi"/>
			<Item Name="9100_OUT_select_HI10_IDC.vi" Type="VI" URL="../Drivers/dpi4 map Fluke9100/9100_OUT_select_HI10_IDC.vi"/>
			<Item Name="9100_OUT_select_NORMAL_IDC.vi" Type="VI" URL="../Drivers/dpi4 map Fluke9100/9100_OUT_select_NORMAL_IDC.vi"/>
			<Item Name="9100_OUT_select_HI50_IDC.vi" Type="VI" URL="../Drivers/dpi4 map Fluke9100/9100_OUT_select_HI50_IDC.vi"/>
			<Item Name="9100_OUT_select_HI50_IAC.vi" Type="VI" URL="../Drivers/dpi4 map Fluke9100/9100_OUT_select_HI50_IAC.vi"/>
			<Item Name="9100_OUT_select_HIGH_IAC.vi" Type="VI" URL="../Drivers/dpi4 map Fluke9100/9100_OUT_select_HIGH_IAC.vi"/>
			<Item Name="9100_OUT_select_HI10_IAC.vi" Type="VI" URL="../Drivers/dpi4 map Fluke9100/9100_OUT_select_HI10_IAC.vi"/>
			<Item Name="9100_QC_register_check.vi" Type="VI" URL="../Drivers/dpi4 map Fluke9100/9100_QC_register_check.vi"/>
			<Item Name="InterfaceType.ctl" Type="VI" URL="../InterfaceLowLevel/InterfaceType.ctl"/>
			<Item Name="9500_ESR_check.vi" Type="VI" URL="../Drivers/dpi4 map Fluke9500/9500_ESR_check.vi"/>
			<Item Name="9500_Out_off.vi" Type="VI" URL="../Drivers/dpi4 map Fluke9500/9500_Out_off.vi"/>
			<Item Name="9500_Out_on.vi" Type="VI" URL="../Drivers/dpi4 map Fluke9500/9500_Out_on.vi"/>
			<Item Name="9500_Set_Channel_Impedance.vi" Type="VI" URL="../Drivers/dpi4 map Fluke9500/9500_Set_Channel_Impedance.vi"/>
			<Item Name="Fluke5790_ESR_check.vi" Type="VI" URL="../Drivers/dpi4 map Fluke5790/Fluke5790_ESR_check.vi"/>
			<Item Name="F5790A_ErrorDescription.vi" Type="VI" URL="../Drivers/dpi4 map Fluke5790/F5790A_ErrorDescription.vi"/>
			<Item Name="F6100_ESR_check.vi" Type="VI" URL="../Drivers/dpi4 map Fluke6100/F6100_ESR_check.vi"/>
			<Item Name="Fluke 6100_Volt_Select_Range.vi" Type="VI" URL="../Drivers/dpi4 map Fluke6100/Fluke 6100_Volt_Select_Range.vi"/>
			<Item Name="Fluke 6100_Curr_Select_Range.vi" Type="VI" URL="../Drivers/dpi4 map Fluke6100/Fluke 6100_Curr_Select_Range.vi"/>
			<Item Name="Out_Off_F6100.vi" Type="VI" URL="../Drivers/dpi4 map Fluke6100/Out_Off_F6100.vi"/>
			<Item Name="Out_On_6100.vi" Type="VI" URL="../Drivers/dpi4 map Fluke6100/Out_On_6100.vi"/>
			<Item Name="Fluke 6100_Volt_DC_Select_Range.vi" Type="VI" URL="../Drivers/dpi4 map Fluke6100/Fluke 6100_Volt_DC_Select_Range.vi"/>
			<Item Name="Fluke 6100_Curr_DC_Select_Range.vi" Type="VI" URL="../Drivers/dpi4 map Fluke6100/Fluke 6100_Curr_DC_Select_Range.vi"/>
			<Item Name="Fluke5000_PQ_ASK.vi" Type="VI" URL="../Drivers/dpi4 map Fluke5000/Fluke5000_PQ_ASK.vi"/>
			<Item Name="F5xxx_ESR_check.vi" Type="VI" URL="../Drivers/dpi4 map Fluke5000/F5xxx_ESR_check.vi"/>
			<Item Name="Out_Off_F5xxx.vi" Type="VI" URL="../Drivers/dpi4 map Fluke5000/Out_Off_F5xxx.vi"/>
			<Item Name="Out_On_F5xxx.vi" Type="VI" URL="../Drivers/dpi4 map Fluke5000/Out_On_F5xxx.vi"/>
			<Item Name="F5000_Find_V.vi" Type="VI" URL="../Drivers/dpi4 map Fluke5000/F5000_Find_V.vi"/>
			<Item Name="F5000_Adjust.vi" Type="VI" URL="../Drivers/dpi4 map Fluke5000/F5000_Adjust.vi"/>
			<Item Name="lvinput.dll" Type="Document" URL="/&lt;resource&gt;/lvinput.dll"/>
			<Item Name="F5xxx_ZCOMP.vi" Type="VI" URL="../Drivers/dpi4 map Fluke5000/F5xxx_ZCOMP.vi"/>
			<Item Name="FLUKE884X_Models.ctl" Type="VI" URL="../Drivers/dpi4 map Fluke884XA/FLUKE884X_Models.ctl"/>
			<Item Name="Fluke854XA_ESR_check.vi" Type="VI" URL="../Drivers/dpi4 map Fluke884XA/Fluke854XA_ESR_check.vi"/>
			<Item Name="Fluke854XA_read.vi" Type="VI" URL="../Drivers/dpi4 map Fluke884XA/Fluke854XA_read.vi"/>
			<Item Name="B1_18_query.vi" Type="VI" URL="../Drivers/dpi4 map B1_18/B1_18_query.vi"/>
			<Item Name="B1_18_write.vi" Type="VI" URL="../Drivers/dpi4 map B1_18/B1_18_write.vi"/>
			<Item Name="B1_18_IsDONE.vi" Type="VI" URL="../Drivers/dpi4 map B1_18/B1_18_IsDONE.vi"/>
			<Item Name="B1_18_CheckError.vi" Type="VI" URL="../Drivers/dpi4 map B1_18/B1_18_CheckError.vi"/>
			<Item Name="B1_18_ErrorDescription.vi" Type="VI" URL="../Drivers/dpi4 map B1_18/B1_18_ErrorDescription.vi"/>
			<Item Name="B1_28_IsDONE.vi" Type="VI" URL="../Drivers/dpi4 map B1_28/B1_28_IsDONE.vi"/>
			<Item Name="B1_28_CheckError.vi" Type="VI" URL="../Drivers/dpi4 map B1_28/B1_28_CheckError.vi"/>
			<Item Name="B1_28_query.vi" Type="VI" URL="../Drivers/dpi4 map B1_28/B1_28_query.vi"/>
			<Item Name="B1_28_ErrorDescription.vi" Type="VI" URL="../Drivers/dpi4 map B1_28/B1_28_ErrorDescription.vi"/>
			<Item Name="B1_28_CheckAndSett.vi" Type="VI" URL="../Drivers/dpi4 map B1_28/B1_28_CheckAndSett.vi"/>
			<Item Name="B1_28_GO.vi" Type="VI" URL="../Drivers/dpi4 map B1_28/B1_28_GO.vi"/>
			<Item Name="B1_28_STOP.vi" Type="VI" URL="../Drivers/dpi4 map B1_28/B1_28_STOP.vi"/>
			<Item Name="B1_28_WriteAndCheck.vi" Type="VI" URL="../Drivers/dpi4 map B1_28/B1_28_WriteAndCheck.vi"/>
			<Item Name="B1_28_CorrectFreqValue.vi" Type="VI" URL="../Drivers/dpi4 map B1_28/B1_28_CorrectFreqValue.vi"/>
			<Item Name="B1_28_R_sel.vi" Type="VI" URL="../Drivers/dpi4 map B1_28/B1_28_R_sel.vi"/>
			<Item Name="R&amp;S_CMU200_Write.vi" Type="VI" URL="../Drivers/dpi4 map CMU200/R&amp;S_CMU200_Write.vi"/>
			<Item Name="R&amp;S_CMU200_Read.vi" Type="VI" URL="../Drivers/dpi4 map CMU200/R&amp;S_CMU200_Read.vi"/>
			<Item Name="DrLL_ESR_check_CMU200.vi" Type="VI" URL="../Drivers/dpi4 map CMU200/DrLL_ESR_check_CMU200.vi"/>
			<Item Name="STD_SELECT.vi" Type="VI" URL="../Drivers/dpi4 map CMU200/STD_SELECT.vi"/>
			<Item Name="R&amp;S_CMU200_QUERY.vi" Type="VI" URL="../Drivers/dpi4 map CMU200/R&amp;S_CMU200_QUERY.vi"/>
			<Item Name="R&amp;S_CMU200_GPRS_EGPRS_SET_COMMUNICATION.vi" Type="VI" URL="../Drivers/dpi4 map CMU200/R&amp;S_CMU200_GPRS_EGPRS_SET_COMMUNICATION.vi"/>
			<Item Name="R&amp;S_CMU200_Message.vi" Type="VI" URL="../Drivers/dpi4 map CMU200/R&amp;S_CMU200_Message.vi"/>
			<Item Name="GSM_SET_END_CALL_NEW.vi" Type="VI" URL="../Drivers/dpi4 map CMU200/GSM_SET_END_CALL_NEW.vi"/>
			<Item Name="Main stm.ctl" Type="VI" URL="../Drivers/dpi4 map CMU200/STM/Main stm.ctl"/>
			<Item Name="QueueManger.vi" Type="VI" URL="../Drivers/dpi4 map CMU200/STM/QueueManger.vi"/>
			<Item Name="UMTS_SET_END_CALL_NEW.vi" Type="VI" URL="../Drivers/dpi4 map CMU200/UMTS_SET_END_CALL_NEW.vi"/>
			<Item Name="R&amp;SCMU200_PARS_CMD.vi" Type="VI" URL="../Drivers/dpi4 map CMU200/R&amp;SCMU200_PARS_CMD.vi"/>
			<Item Name="R&amp;S_CMU200_GSM_SET_COMMUNICATION.vi" Type="VI" URL="../Drivers/dpi4 map CMU200/R&amp;S_CMU200_GSM_SET_COMMUNICATION.vi"/>
			<Item Name="R&amp;S_Dialog.vi" Type="VI" URL="../Drivers/dpi4 map CMU200/R&amp;S_Dialog.vi"/>
			<Item Name="GSM_SET_COMMUNICATION_NEW.vi" Type="VI" URL="../Drivers/dpi4 map CMU200/GSM_SET_COMMUNICATION_NEW.vi"/>
			<Item Name="R&amp;S_CMU200_UMTS_SET_COMMUNICATION.vi" Type="VI" URL="../Drivers/dpi4 map CMU200/R&amp;S_CMU200_UMTS_SET_COMMUNICATION.vi"/>
			<Item Name="UMTS_SET_COMMUNICATION_NEW.vi" Type="VI" URL="../Drivers/dpi4 map CMU200/UMTS_SET_COMMUNICATION_NEW.vi"/>
			<Item Name="R&amp;S_CMU200_GET_ARRAY.vi" Type="VI" URL="../Drivers/dpi4 map CMU200/R&amp;S_CMU200_GET_ARRAY.vi"/>
			<Item Name="R&amp;S_CMU200_GSM_RACH.vi" Type="VI" URL="../Drivers/dpi4 map CMU200/R&amp;S_CMU200_GSM_RACH.vi"/>
			<Item Name="PWR_RACH.vi" Type="VI" URL="../Drivers/dpi4 map CMU200/PWR_RACH.vi"/>
			<Item Name="R&amp;S_CMU200_UMTS_RACH.vi" Type="VI" URL="../Drivers/dpi4 map CMU200/R&amp;S_CMU200_UMTS_RACH.vi"/>
			<Item Name="R&amp;S_CUM200_REQUEST_VALUE.vi" Type="VI" URL="../Drivers/dpi4 map CMU200/R&amp;S_CUM200_REQUEST_VALUE.vi"/>
			<Item Name="UMTS_RACH.vi" Type="VI" URL="../Drivers/dpi4 map CMU200/UMTS_RACH.vi"/>
			<Item Name="TestDriverDialog.vi" Type="VI" URL="../Drivers/dpi4 map TestDriver/TestDriverDialog.vi"/>
			<Item Name="Out_Off_30xx.vi" Type="VI" URL="../Drivers/dpi4 map Transmille3000/Out_Off_30xx.vi"/>
			<Item Name="Tr30xx_ESR_check.vi" Type="VI" URL="../Drivers/dpi4 map Transmille3000/Tr30xx_ESR_check.vi"/>
			<Item Name="Tr_30xx_ErrorDescription.vi" Type="VI" URL="../Drivers/dpi4 map Transmille3000/Tr_30xx_ErrorDescription.vi"/>
			<Item Name="Out_On_30xx.vi" Type="VI" URL="../Drivers/dpi4 map Transmille3000/Out_On_30xx.vi"/>
			<Item Name="TransmilleRangeSelect (SubVI).vi" Type="VI" URL="../Drivers/dpi4 map Transmille3000/TransmilleRangeSelect (SubVI).vi"/>
			<Item Name="TransmilleStandartRangeSelect (SubVI).vi" Type="VI" URL="../Drivers/dpi4 map Transmille3000/TransmilleStandartRangeSelect (SubVI).vi"/>
			<Item Name="Tr_30xx_Write&amp;ErrorCheck.vi" Type="VI" URL="../Drivers/dpi4 map Transmille3000/Tr_30xx_Write&amp;ErrorCheck.vi"/>
			<Item Name="DynamWaypoint.ctl" Type="VI" URL="../Drivers/dpi4 map SMBV100A/DynamWaypoint.ctl"/>
			<Item Name="DynamWaypoints.ctl" Type="VI" URL="../Drivers/dpi4 map SMBV100A/DynamWaypoints.ctl"/>
			<Item Name="R&amp;S_SMBV100A_Error_check.vi" Type="VI" URL="../Drivers/dpi4 map SMBV100A/R&amp;S_SMBV100A_Error_check.vi"/>
			<Item Name="DrLL_ESR_check_R&amp;S_SMBV100A.vi" Type="VI" URL="../Drivers/dpi4 map SMBV100A/DrLL_ESR_check_R&amp;S_SMBV100A.vi"/>
			<Item Name="R&amp;S_SMBV100A_FASTQUERY.vi" Type="VI" URL="../Drivers/dpi4 map SMBV100A/R&amp;S_SMBV100A_FASTQUERY.vi"/>
			<Item Name="R&amp;S_SMBV100A_RequestWaypoints.vi" Type="VI" URL="../Drivers/dpi4 map SMBV100A/R&amp;S_SMBV100A_RequestWaypoints.vi"/>
			<Item Name="R&amp;S_SMBV100A_get_scenario.vi" Type="VI" URL="../Drivers/dpi4 map SMBV100A/R&amp;S_SMBV100A_get_scenario.vi"/>
			<Item Name="ScenarioInfo.ctl" Type="VI" URL="../Drivers/dpi4 map SMBV100A/ScenarioInfo.ctl"/>
			<Item Name="ScenarioLineInfo.ctl" Type="VI" URL="../Drivers/dpi4 map SMBV100A/ScenarioLineInfo.ctl"/>
			<Item Name="CalculateLineScenario.vi" Type="VI" URL="../Drivers/dpi4 map SMBV100A/CalculateLineScenario.vi"/>
			<Item Name="CalculateAcrScenario.vi" Type="VI" URL="../Drivers/dpi4 map SMBV100A/CalculateAcrScenario.vi"/>
			<Item Name="R&amp;S_SMBV100A_DMS_TO_DEC.vi" Type="VI" URL="../Drivers/dpi4 map SMBV100A/R&amp;S_SMBV100A_DMS_TO_DEC.vi"/>
			<Item Name="R&amp;S_SMBV100A_Scenario_Get_Speed.vi" Type="VI" URL="../Drivers/dpi4 map SMBV100A/R&amp;S_SMBV100A_Scenario_Get_Speed.vi"/>
			<Item Name="SMBV100A_ADD_ACCELERATION.vi" Type="VI" URL="../Drivers/dpi4 map SMBV100A/SMBV100A_ADD_ACCELERATION.vi"/>
			<Item Name="CMBV100_Import2UnitessNMEA.vi" Type="VI" URL="../Drivers/dpi4 map SMBV100A/CMBV100_Import2UnitessNMEA.vi"/>
			<Item Name="NMEA0183_init.vi" Type="VI" URL="../Drivers/dpi4 map SMBV100A/NMEA0183_init.vi"/>
			<Item Name="NMEA0183_close.vi" Type="VI" URL="../Drivers/dpi4 map SMBV100A/NMEA0183_close.vi"/>
			<Item Name="R&amp;S_SMBV100A_arrayStringToDOUBLE.vi" Type="VI" URL="../Drivers/dpi4 map SMBV100A/R&amp;S_SMBV100A_arrayStringToDOUBLE.vi"/>
			<Item Name="SMBV100A_PARS_SAT_TIME_AND_NUMBER.vi" Type="VI" URL="../Drivers/dpi4 map SMBV100A/SMBV100A_PARS_SAT_TIME_AND_NUMBER.vi"/>
			<Item Name="SMBV100A_ASKCONFIGURATION.vi" Type="VI" URL="../Drivers/dpi4 map SMBV100A/SMBV100A_ASKCONFIGURATION.vi"/>
			<Item Name="SMBV100A_CONVERT_DATASAT_2D_to_N1D.vi" Type="VI" URL="../Drivers/dpi4 map SMBV100A/SMBV100A_CONVERT_DATASAT_2D_to_N1D.vi"/>
			<Item Name="SMBV100A_N_in_SKY_to_SAT_in_DEVICE.vi" Type="VI" URL="../Drivers/dpi4 map SMBV100A/SMBV100A_N_in_SKY_to_SAT_in_DEVICE.vi"/>
			<Item Name="SMBV100A_N_ASKCONFIGURATION.vi" Type="VI" URL="../Drivers/dpi4 map SMBV100A/SMBV100A_N_ASKCONFIGURATION.vi"/>
			<Item Name="USWITCH_Models.ctl" Type="VI" URL="../Drivers/dpi4 map Uswitch/USWITCH_Models.ctl"/>
			<Item Name="USWITCH_Error_check.vi" Type="VI" URL="../Drivers/dpi4 map Uswitch/USWITCH_Error_check.vi"/>
			<Item Name="USWITCHWrite.vi" Type="VI" URL="../Drivers/dpi4 map Uswitch/USWITCHWrite.vi"/>
			<Item Name="PACE5000_ParsingError.vi" Type="VI" URL="../Drivers/dpi4 map PACE5000/PACE5000_ParsingError.vi"/>
			<Item Name="PACE5000_AUTO_CONTROL.vi" Type="VI" URL="../Drivers/dpi4 map PACE5000/PACE5000_AUTO_CONTROL.vi"/>
			<Item Name="PACE5000_ParsingStringPreas.vi" Type="VI" URL="../Drivers/dpi4 map PACE5000/PACE5000_ParsingStringPreas.vi"/>
			<Item Name="PACE5000_ESR_check.vi" Type="VI" URL="../Drivers/dpi4 map PACE5000/PACE5000_ESR_check.vi"/>
			<Item Name="AgilentModels.ctl" Type="VI" URL="../Drivers/dpi4 map Agilent_740XA/AgilentModels.ctl"/>
			<Item Name="Agilent740X_SweepDone.vi" Type="VI" URL="../Drivers/dpi4 map Agilent_740XA/Agilent740X_SweepDone.vi"/>
			<Item Name="NEX1_NS265_ErrorCheck.vi" Type="VI" URL="../Drivers/dpi4 map NEX1_NS265/NEX1_NS265_ErrorCheck.vi"/>
			<Item Name="Start_Interface_Setup.vi" Type="VI" URL="../Drivers/dpi4 map CE602/Start_Interface_Setup.vi"/>
			<Item Name="CE602_ESR_check.vi" Type="VI" URL="../Drivers/dpi4 map CE602/CE602_ESR_check.vi"/>
			<Item Name="EX124_Read.vi" Type="VI" URL="../Drivers/dpi4 map EX124/EX124_Read.vi"/>
			<Item Name="AgilentN67xx_ESR_check.vi" Type="VI" URL="../Drivers/dpi4 map AgilentN67xx/AgilentN67xx_ESR_check.vi"/>
			<Item Name="Keithley_2002_ERR_check.vi" Type="VI" URL="../Drivers/dpi4 map Keitley_2002/Keithley_2002_ERR_check.vi"/>
			<Item Name="Keithley_7001_ERR_check.vi" Type="VI" URL="../Drivers/dpi4 map Keitley_7001/Keithley_7001_ERR_check.vi"/>
			<Item Name="Status parse.vi" Type="VI" URL="../Drivers/dpi4 map Keitley_7001/Status parse.vi"/>
			<Item Name="GENser Checksum Append.vi" Type="VI" URL="../Drivers/dpi4 map TDK_Lambda/_GENser.llb/GENser Checksum Append.vi"/>
			<Item Name="TDK_ESR_check.vi" Type="VI" URL="../Drivers/dpi4 map TDK_Lambda/TDK_ESR_check.vi"/>
			<Item Name="GENser Checksum Verify.vi" Type="VI" URL="../Drivers/dpi4 map TDK_Lambda/_GENser.llb/GENser Checksum Verify.vi"/>
			<Item Name="GENser Convert Errors.vi" Type="VI" URL="../Drivers/dpi4 map TDK_Lambda/_GENser.llb/GENser Convert Errors.vi"/>
			<Item Name="InstekPCS71000_ERR_check.vi" Type="VI" URL="../Drivers/dpi4 map InstekPCS71000/InstekPCS71000_ERR_check.vi"/>
			<Item Name="AKIP_1202_ERR_check.vi" Type="VI" URL="../Drivers/dpi4 map AKIP_1202/AKIP_1202_ERR_check.vi"/>
			<Item Name="AKIP_1310_ERR_check.vi" Type="VI" URL="../Drivers/dpi4 map AKIP_1310/AKIP_1310_ERR_check.vi"/>
			<Item Name="DrLL_FormatStringValueWithDot.vi" Type="VI" URL="../DriverLowLevel/DrLL_FormatStringValueWithDot.vi"/>
			<Item Name="IKSU2000_DriverSettings.ctl" Type="VI" URL="../Drivers/dpi4 map IKSU2000/IKSU2000_DriverSettings.ctl"/>
			<Item Name="IKSU2000_MODE.ctl" Type="VI" URL="../Drivers/dpi4 map IKSU2000/SUB_COMMAND/IKSU2000_MODE.ctl"/>
			<Item Name="IKSU2000_INIT.vi" Type="VI" URL="../Drivers/dpi4 map IKSU2000/Function/IKSU2000_INIT.vi"/>
			<Item Name="IKSU2000_command_init.vi" Type="VI" URL="../Drivers/dpi4 map IKSU2000/IKSU2000_command_init.vi"/>
			<Item Name="IKSU2000_command_stak.vi" Type="VI" URL="../Drivers/dpi4 map IKSU2000/IKSU2000_command_stak.vi"/>
			<Item Name="IKSU2000_PUT_START.vi" Type="VI" URL="../Drivers/dpi4 map IKSU2000/SUB_COMMAND/IKSU2000_PUT_START.vi"/>
			<Item Name="IKSU2000_PUT_MODE.vi" Type="VI" URL="../Drivers/dpi4 map IKSU2000/SUB_COMMAND/IKSU2000_PUT_MODE.vi"/>
			<Item Name="IKSU2000_QUERY.vi" Type="VI" URL="../Drivers/dpi4 map IKSU2000/SUB_COMMAND/IKSU2000_QUERY.vi"/>
			<Item Name="IKSU2000_PUT_NUMBER.vi" Type="VI" URL="../Drivers/dpi4 map IKSU2000/SUB_COMMAND/IKSU2000_PUT_NUMBER.vi"/>
			<Item Name="IKSU2000_PURSE_NUMBER(SubVI).vi" Type="VI" URL="../Drivers/dpi4 map IKSU2000/SUB_COMMAND/IKSU2000_PURSE_NUMBER(SubVI).vi"/>
			<Item Name="IKSU2000_PUT_STOP.vi" Type="VI" URL="../Drivers/dpi4 map IKSU2000/SUB_COMMAND/IKSU2000_PUT_STOP.vi"/>
			<Item Name="IKSU2000_READ_ONE_number.vi" Type="VI" URL="../Drivers/dpi4 map IKSU2000/SUB_COMMAND/IKSU2000_READ_ONE_number.vi"/>
			<Item Name="IKSU2000_PUT_READ.vi" Type="VI" URL="../Drivers/dpi4 map IKSU2000/SUB_COMMAND/IKSU2000_PUT_READ.vi"/>
			<Item Name="IKSU2000_READ_ANSWER.vi" Type="VI" URL="../Drivers/dpi4 map IKSU2000/SUB_COMMAND/IKSU2000_READ_ANSWER.vi"/>
			<Item Name="IKSU2000_READ_stak.vi" Type="VI" URL="../Drivers/dpi4 map IKSU2000/SUB_COMMAND/IKSU2000_READ_stak.vi"/>
			<Item Name="KM300_COMMANDS.ctl" Type="VI" URL="../Drivers/dpi4 map KM300/Function/KM300_COMMANDS.ctl"/>
			<Item Name="KM300_Load_Last_Setting.vi" Type="VI" URL="../Drivers/dpi4 map KM300/KM300_Load_Last_Setting.vi"/>
			<Item Name="KM300_Adress_Control Build.vi" Type="VI" URL="../Drivers/dpi4 map KM300/KM300_Adress_Control Build.vi"/>
			<Item Name="KM300_INIT.vi" Type="VI" URL="../Drivers/dpi4 map KM300/KM300_INIT.vi"/>
			<Item Name="KM300_COMMAND.vi" Type="VI" URL="../Drivers/dpi4 map KM300/Function/KM300_COMMAND.vi"/>
			<Item Name="KM300_COMP_BYTE.vi" Type="VI" URL="../Drivers/dpi4 map KM300/Function/KM300_COMP_BYTE.vi"/>
			<Item Name="KM300_X_CRC_POLYNOM_TO_WRITE.vi" Type="VI" URL="../Drivers/dpi4 map KM300/KM300_X_CRC_POLYNOM_TO_WRITE.vi"/>
			<Item Name="KM300_CALC_PARITY_BIT.vi" Type="VI" URL="../Drivers/dpi4 map KM300/KM300_CALC_PARITY_BIT.vi"/>
			<Item Name="KM300_QUERY.vi" Type="VI" URL="../Drivers/dpi4 map KM300/Function/KM300_QUERY.vi"/>
			<Item Name="KM300_HEXstring_READ_BUFFER.vi" Type="VI" URL="../Drivers/dpi4 map KM300/Function/KM300_HEXstring_READ_BUFFER.vi"/>
			<Item Name="KM300_Save_Last_Setting.vi" Type="VI" URL="../Drivers/dpi4 map KM300/KM300_Save_Last_Setting.vi"/>
			<Item Name="KM300_GLOBAL_WRITE.vi" Type="VI" URL="../Drivers/dpi4 map KM300/KM300_GLOBAL_WRITE.vi"/>
			<Item Name="KM300_SwitchOFF.vi" Type="VI" URL="../Drivers/dpi4 map KM300/Function/KM300_SwitchOFF.vi"/>
			<Item Name="KM300_Range.vi" Type="VI" URL="../Drivers/dpi4 map KM300/Function/KM300_Range.vi"/>
			<Item Name="KM300_RANGE_SELECT.vi" Type="VI" URL="../Drivers/dpi4 map KM300/KM300_RANGE_SELECT.vi"/>
			<Item Name="KM300_Value_Create.vi" Type="VI" URL="../Drivers/dpi4 map KM300/Function/KM300_Value_Create.vi"/>
			<Item Name="KM300_Value_Maska(SubVI).vi" Type="VI" URL="../Drivers/dpi4 map KM300/Function/KM300_Value_Maska(SubVI).vi"/>
			<Item Name="KM300_Value_check_value_and_PLUS_MINUS(SubVI).vi" Type="VI" URL="../Drivers/dpi4 map KM300/Function/KM300_Value_check_value_and_PLUS_MINUS(SubVI).vi"/>
			<Item Name="KM300_check_AC.vi" Type="VI" URL="../Drivers/dpi4 map KM300/Function/KM300_check_AC.vi"/>
			<Item Name="KM300_CREATE_HEX(SubVI).vi" Type="VI" URL="../Drivers/dpi4 map KM300/Function/KM300_CREATE_HEX(SubVI).vi"/>
			<Item Name="KM300_Value_Create_1(SubVI).vi" Type="VI" URL="../Drivers/dpi4 map KM300/Function/KM300_Value_Create_1(SubVI).vi"/>
			<Item Name="KM300_FREQ_toHEX.vi" Type="VI" URL="../Drivers/dpi4 map KM300/Function/KM300_FREQ_toHEX.vi"/>
			<Item Name="DjLL_Number_to_2str.vi" Type="VI" URL="../Drivers/dpi4 map KM300/driver_jr_hex/DjLL_Number_to_2str.vi"/>
			<Item Name="KM300_COMPLETE_PACKAGE.vi" Type="VI" URL="../Drivers/dpi4 map KM300/KM300_COMPLETE_PACKAGE.vi"/>
			<Item Name="KM300_ADJUST_new.vi" Type="VI" URL="../Drivers/dpi4 map KM300/adjust/KM300_ADJUST_new.vi"/>
			<Item Name="KM300_Buttom_clr.ctl" Type="VI" URL="../Drivers/dpi4 map KM300/adjust/KM300_Buttom_clr.ctl"/>
			<Item Name="KM300_main_stm.ctl" Type="VI" URL="../Drivers/dpi4 map KM300/adjust/STM/KM300_main_stm.ctl"/>
			<Item Name="KM300_main_stm.vi" Type="VI" URL="../Drivers/dpi4 map KM300/adjust/STM/KM300_main_stm.vi"/>
			<Item Name="KM300_Take_Coef(SubVI).vi" Type="VI" URL="../Drivers/dpi4 map KM300/Function/KM300_Take_Coef(SubVI).vi"/>
			<Item Name="KM300_Parse_Read.vi" Type="VI" URL="../Drivers/dpi4 map KM300/Function/KM300_Parse_Read.vi"/>
			<Item Name="KM300_CRC_CHECK.vi" Type="VI" URL="../Drivers/dpi4 map KM300/KM300_CRC_CHECK.vi"/>
			<Item Name="KM300_X_CRC_POLYNOM_FROM_READ.vi" Type="VI" URL="../Drivers/dpi4 map KM300/KM300_X_CRC_POLYNOM_FROM_READ.vi"/>
			<Item Name="CO3001_DriverSettings.ctl" Type="VI" URL="../Drivers/dpi4 map CO3001/Function/CO3001_DriverSettings.ctl"/>
			<Item Name="C3001_DATA.ctl" Type="VI" URL="../Drivers/dpi4 map CO3001/Function/C3001_DATA.ctl"/>
			<Item Name="CO3001_command_for_function.ctl" Type="VI" URL="../Drivers/dpi4 map CO3001/Function/CO3001_command_for_function.ctl"/>
			<Item Name="CO3001_Load_Last_Setting.vi" Type="VI" URL="../Drivers/dpi4 map CO3001/CO3001_Load_Last_Setting.vi"/>
			<Item Name="CO3001_Adress_Control Build.vi" Type="VI" URL="../Drivers/dpi4 map CO3001/CO3001_Adress_Control Build.vi"/>
			<Item Name="CO3001_INIT.vi" Type="VI" URL="../Drivers/dpi4 map CO3001/CO3001_INIT.vi"/>
			<Item Name="CO3001_create_writepocket.vi" Type="VI" URL="../Drivers/dpi4 map CO3001/CO3001_create_writepocket.vi"/>
			<Item Name="CO3001_DATA_TRANSPORT_IN.vi" Type="VI" URL="../Drivers/dpi4 map CO3001/Function/CO3001_DATA_TRANSPORT_IN.vi"/>
			<Item Name="DjLL_Num_to_format_Str.vi" Type="VI" URL="../Drivers/dpi4 map CO3001/driver_jr_hex/DjLL_Num_to_format_Str.vi"/>
			<Item Name="CO3001_5int_to_5hex_str.vi" Type="VI" URL="../Drivers/dpi4 map CO3001/Function/5_to_5_OR_12_to_12/CO3001_5int_to_5hex_str.vi"/>
			<Item Name="DjLL_TR_STR__to_BIN_STR.vi" Type="VI" URL="../Drivers/dpi4 map CO3001/driver_jr_hex/DjLL_TR_STR__to_BIN_STR.vi"/>
			<Item Name="DjLL_Num_to_BIN_Str.vi" Type="VI" URL="../Drivers/dpi4 map CO3001/driver_jr_hex/DjLL_Num_to_BIN_Str.vi"/>
			<Item Name="DjLL_STR__TO__HEX_STR.vi" Type="VI" URL="../Drivers/dpi4 map CO3001/driver_jr_hex/DjLL_STR__TO__HEX_STR.vi"/>
			<Item Name="CO3001_5_bool_to_5_str.vi" Type="VI" URL="../Drivers/dpi4 map CO3001/Function/5_to_5_OR_12_to_12/CO3001_5_bool_to_5_str.vi"/>
			<Item Name="CO3001_X_CRC_POLYNOM_TO_WRITE.vi" Type="VI" URL="../Drivers/dpi4 map CO3001/CO3001_X_CRC_POLYNOM_TO_WRITE.vi"/>
			<Item Name="CO3001_CALC_PARITY_BIT.vi" Type="VI" URL="../Drivers/dpi4 map CO3001/CO3001_CALC_PARITY_BIT.vi"/>
			<Item Name="CO3001_QUERY.vi" Type="VI" URL="../Drivers/dpi4 map CO3001/Function/CO3001_QUERY.vi"/>
			<Item Name="CO3001_DriverSettings_Status.ctl" Type="VI" URL="../Drivers/dpi4 map CO3001/Function/CO3001_DriverSettings_Status.ctl"/>
			<Item Name="CO3001_show_hex_pocket.ctl" Type="VI" URL="../Drivers/dpi4 map CO3001/Function/CO3001_show_hex_pocket.ctl"/>
			<Item Name="CO3001_show_maska.ctl" Type="VI" URL="../Drivers/dpi4 map CO3001/Function/CO3001_show_maska.ctl"/>
			<Item Name="CO3001_mode.ctl" Type="VI" URL="../Drivers/dpi4 map CO3001/Function/CO3001_mode.ctl"/>
			<Item Name="CO3001_show_True_value.ctl" Type="VI" URL="../Drivers/dpi4 map CO3001/Function/CO3001_show_True_value.ctl"/>
			<Item Name="CO3001_show_for_adjust.ctl" Type="VI" URL="../Drivers/dpi4 map CO3001/Function/CO3001_show_for_adjust.ctl"/>
			<Item Name="CO3001_show_information.ctl" Type="VI" URL="../Drivers/dpi4 map CO3001/Function/CO3001_show_information.ctl"/>
			<Item Name="CO3001_HEXstring_READ_BUFFER.vi" Type="VI" URL="../Drivers/dpi4 map CO3001/Function/CO3001_HEXstring_READ_BUFFER.vi"/>
			<Item Name="CO3001_Save_Last_Setting.vi" Type="VI" URL="../Drivers/dpi4 map CO3001/CO3001_Save_Last_Setting.vi"/>
			<Item Name="CO3001_PARSE_VALUE.vi" Type="VI" URL="../Drivers/dpi4 map CO3001/CO3001_PARSE_VALUE.vi"/>
			<Item Name="KM300C_DriverSettings.ctl" Type="VI" URL="../Drivers/dpi4 map KM300C/Function/KM300C_DriverSettings.ctl"/>
			<Item Name="KM300C_Load_Last_Setting.vi" Type="VI" URL="../Drivers/dpi4 map KM300C/KM300C_Load_Last_Setting.vi"/>
			<Item Name="KM300C_Adress_Control Build.vi" Type="VI" URL="../Drivers/dpi4 map KM300C/KM300C_Adress_Control Build.vi"/>
			<Item Name="KM300C_INIT.vi" Type="VI" URL="../Drivers/dpi4 map KM300C/KM300C_INIT.vi"/>
			<Item Name="KM300C_COMMAND.vi" Type="VI" URL="../Drivers/dpi4 map KM300C/Function/KM300C_COMMAND.vi"/>
			<Item Name="KM300C_X_CRC_POLYNOM_TO_WRITE.vi" Type="VI" URL="../Drivers/dpi4 map KM300C/KM300C_X_CRC_POLYNOM_TO_WRITE.vi"/>
			<Item Name="KM300C_CALC_PARITY_BIT.vi" Type="VI" URL="../Drivers/dpi4 map KM300C/KM300C_CALC_PARITY_BIT.vi"/>
			<Item Name="KM300C_QUERY.vi" Type="VI" URL="../Drivers/dpi4 map KM300C/Function/KM300C_QUERY.vi"/>
			<Item Name="KM300C_HEXstring_READ_BUFFER.vi" Type="VI" URL="../Drivers/dpi4 map KM300C/Function/KM300C_HEXstring_READ_BUFFER.vi"/>
			<Item Name="KM300C_Save_Last_Setting.vi" Type="VI" URL="../Drivers/dpi4 map KM300C/KM300C_Save_Last_Setting.vi"/>
			<Item Name="KM300C_Range.vi" Type="VI" URL="../Drivers/dpi4 map KM300C/Function/KM300C_Range.vi"/>
			<Item Name="KM300C_SaveRange.vi" Type="VI" URL="../Drivers/dpi4 map KM300C/Function/KM300C_SaveRange.vi"/>
			<Item Name="RESISTANCESTORE_Models_TAB.ctl" Type="VI" URL="../Drivers/dpi4 map ResistanceStore/RESISTANCESTORE_Models_TAB.ctl"/>
			<Item Name="RESISTANCESTORE_Load_Last_Setting.vi" Type="VI" URL="../Drivers/dpi4 map ResistanceStore/RESISTANCESTORE_Load_Last_Setting.vi"/>
			<Item Name="RESISTANCESTORE_Save_Last_Setting.vi" Type="VI" URL="../Drivers/dpi4 map ResistanceStore/RESISTANCESTORE_Save_Last_Setting.vi"/>
			<Item Name="RESISTANCESTORE_IMG_NEW.vi" Type="VI" URL="../Drivers/dpi4 map ResistanceStore/RESISTANCESTORE_IMG_NEW.vi"/>
			<Item Name="RESISTANCESTORE_TAB.ctl" Type="VI" URL="../Drivers/dpi4 map ResistanceStore/RESISTANCESTORE_TAB.ctl"/>
			<Item Name="RESISTANCESTORE_WAITTIME.ctl" Type="VI" URL="../Drivers/dpi4 map ResistanceStore/RESISTANCESTORE_WAITTIME.ctl"/>
			<Item Name="RESISTANCESTORE_mini_dicider.vi" Type="VI" URL="../Drivers/dpi4 map ResistanceStore/RESISTANCESTORE_mini_dicider.vi"/>
			<Item Name="man_status_manometr.vi" Type="VI" URL="../Drivers/dpi4 map PressureStore/man_status_manometr.vi"/>
			<Item Name="manovetr.ctl" Type="VI" URL="../Drivers/dpi4 map PressureStore/manovetr.ctl"/>
			<Item Name="ACOUSTICSTORE_TABLE_SHOW.vi" Type="VI" URL="../Drivers/dpi4 map Driver AcousticStore/ACOUSTICSTORE_TABLE_SHOW.vi"/>
			<Item Name="ACOUSTICSTORE_TABLE.ctl" Type="VI" URL="../Drivers/dpi4 map Driver AcousticStore/ACOUSTICSTORE_TABLE.ctl"/>
			<Item Name="ACOUSTICSTORE_TABLE_WRITE.vi" Type="VI" URL="../Drivers/dpi4 map Driver AcousticStore/ACOUSTICSTORE_TABLE_WRITE.vi"/>
			<Item Name="ACOUSTICSTORE_Range_Select.vi" Type="VI" URL="../Drivers/dpi4 map Driver AcousticStore/ACOUSTICSTORE_Range_Select.vi"/>
			<Item Name="ACOUSTICSTORE_StandartRangeSelect (SubVI).vi" Type="VI" URL="../Drivers/dpi4 map Driver AcousticStore/ACOUSTICSTORE_StandartRangeSelect (SubVI).vi"/>
			<Item Name="ACOUSTICSTORE_TABLE_LENGHT_ELEMENT.vi" Type="VI" URL="../Drivers/dpi4 map Driver AcousticStore/ACOUSTICSTORE_TABLE_LENGHT_ELEMENT.vi"/>
			<Item Name="ACOUSTICSTORE_TABLE_SCAN_FULL.vi" Type="VI" URL="../Drivers/dpi4 map Driver AcousticStore/ACOUSTICSTORE_TABLE_SCAN_FULL.vi"/>
			<Item Name="COIL_DB_PARAM.ctl" Type="VI" URL="../Drivers/dpi4 map COILSTORE/COIL_DB_PARAM.ctl"/>
			<Item Name="COILSTORE_Load_Last_Setting.vi" Type="VI" URL="../Drivers/dpi4 map COILSTORE/COILSTORE_Load_Last_Setting.vi"/>
			<Item Name="COILSTORE_Save_Last_Setting.vi" Type="VI" URL="../Drivers/dpi4 map COILSTORE/COILSTORE_Save_Last_Setting.vi"/>
			<Item Name="coilDB.vi" Type="VI" URL="../Drivers/dpi4 map COILSTORE/coilDB.vi"/>
			<Item Name="COIL_main_stm.ctl" Type="VI" URL="../Drivers/dpi4 map COILSTORE/STM/COIL_main_stm.ctl"/>
			<Item Name="COIL_main_stm.vi" Type="VI" URL="../Drivers/dpi4 map COILSTORE/STM/COIL_main_stm.vi"/>
			<Item Name="coilDB_PastTestResult(SubVI).vi" Type="VI" URL="../Drivers/dpi4 map COILSTORE/coilDB_PastTestResult(SubVI).vi"/>
			<Item Name="Instek_DMM_Models.ctl" Type="VI" URL="../Drivers/dpi4 map InstekDMM/Instek_DMM_Models.ctl"/>
			<Item Name="InstekQuery.vi" Type="VI" URL="../Drivers/dpi4 map InstekDMM/InstekQuery.vi"/>
			<Item Name="Instek_DMM_Error_check.vi" Type="VI" URL="../Drivers/dpi4 map InstekDMM/Instek_DMM_Error_check.vi"/>
			<Item Name="Instek_DMM_ESR_check.vi" Type="VI" URL="../Drivers/dpi4 map InstekDMM/Instek_DMM_ESR_check.vi"/>
			<Item Name="InstekWrite.vi" Type="VI" URL="../Drivers/dpi4 map InstekDMM/InstekWrite.vi"/>
			<Item Name="AgilentDMM_Models.ctl" Type="VI" URL="../Drivers/dpi4 map AgilentDMM/AgilentDMM_Models.ctl"/>
			<Item Name="AgilentDMM_Functions.ctl" Type="VI" URL="../Drivers/dpi4 map AgilentDMM/AgilentDMM_Functions.ctl"/>
			<Item Name="AgilentDMM_ESR_check.vi" Type="VI" URL="../Drivers/dpi4 map AgilentDMM/AgilentDMM_ESR_check.vi"/>
			<Item Name="Agilent3458A_ESR_check.vi" Type="VI" URL="../Drivers/dpi4 map Agilent 3458A/Agilent3458A_ESR_check.vi"/>
			<Item Name="RigolDMM_Models.ctl" Type="VI" URL="../Drivers/dpi4 map RigolDMM/RigolDMM_Models.ctl"/>
			<Item Name="RigolDMMQuery.vi" Type="VI" URL="../Drivers/dpi4 map RigolDMM/RigolDMMQuery.vi"/>
			<Item Name="RigolDMM_Error_check.vi" Type="VI" URL="../Drivers/dpi4 map RigolDMM/RigolDMM_Error_check.vi"/>
			<Item Name="RigolDMM_ESR_check.vi" Type="VI" URL="../Drivers/dpi4 map RigolDMM/RigolDMM_ESR_check.vi"/>
			<Item Name="RigolDMMWrite.vi" Type="VI" URL="../Drivers/dpi4 map RigolDMM/RigolDMMWrite.vi"/>
			<Item Name="RigolDMM_3068_Choicefunc.vi" Type="VI" URL="../Drivers/dpi4 map RigolDMM/RigolDMM_3068_Choicefunc.vi"/>
			<Item Name="RigolDMM_RangeSelect.vi" Type="VI" URL="../Drivers/dpi4 map RigolDMM/RigolDMM_RangeSelect.vi"/>
			<Item Name="TektronixDMM_Models.ctl" Type="VI" URL="../Drivers/dpi4 map TektronixDMM/TektronixDMM_Models.ctl"/>
			<Item Name="TektronixDMM_DISPLAY.ctl" Type="VI" URL="../Drivers/dpi4 map TektronixDMM/TektronixDMM_DISPLAY.ctl"/>
			<Item Name="TektronixDMM_ESR_check.vi" Type="VI" URL="../Drivers/dpi4 map TektronixDMM/TektronixDMM_ESR_check.vi"/>
			<Item Name="Transmille_MODEL.ctl" Type="VI" URL="../Drivers/dpi4 map Transmille8000/Transmille_MODEL.ctl"/>
			<Item Name="Transmille_CONF.ctl" Type="VI" URL="../Drivers/dpi4 map Transmille8000/Transmille_CONF.ctl"/>
			<Item Name="Tr_80xx_Write&amp;ErrorCheck.vi" Type="VI" URL="../Drivers/dpi4 map Transmille8000/Tr_80xx_Write&amp;ErrorCheck.vi"/>
			<Item Name="Tr80xx_ESR_check.vi" Type="VI" URL="../Drivers/dpi4 map Transmille8000/Tr80xx_ESR_check.vi"/>
			<Item Name="Tr_80xx_ErrorDescription.vi" Type="VI" URL="../Drivers/dpi4 map Transmille8000/Tr_80xx_ErrorDescription.vi"/>
			<Item Name="TransmilleDMM_RangeSelect.vi" Type="VI" URL="../Drivers/dpi4 map Transmille8000/TransmilleDMM_RangeSelect.vi"/>
			<Item Name="Out_Off_80xx.vi" Type="VI" URL="../Drivers/dpi4 map Transmille8000/Out_Off_80xx.vi"/>
			<Item Name="AgilentU3400_Models.ctl" Type="VI" URL="../Drivers/dpi4 map AgilentU3400/AgilentU3400_Models.ctl"/>
			<Item Name="AgilentU3400_ESR_check.vi" Type="VI" URL="../Drivers/dpi4 map AgilentU3400/AgilentU3400_ESR_check.vi"/>
			<Item Name="Instek_DMM70000_Models.ctl" Type="VI" URL="../Drivers/dpi4 map InstekDMM7/Instek_DMM70000_Models.ctl"/>
			<Item Name="Instek70000Query.vi" Type="VI" URL="../Drivers/dpi4 map InstekDMM7/Instek70000Query.vi"/>
			<Item Name="Instek70000Write.vi" Type="VI" URL="../Drivers/dpi4 map InstekDMM7/Instek70000Write.vi"/>
			<Item Name="Instek_DMM70000_Error_check.vi" Type="VI" URL="../Drivers/dpi4 map InstekDMM7/Instek_DMM70000_Error_check.vi"/>
			<Item Name="Instek_DMM70000_ESR_check.vi" Type="VI" URL="../Drivers/dpi4 map InstekDMM7/Instek_DMM70000_ESR_check.vi"/>
			<Item Name="KeithleyDMM2000_Models.ctl" Type="VI" URL="../Drivers/dpi4 map KeithleyDMM2000/KeithleyDMM2000_Models.ctl"/>
			<Item Name="KeithleyDMM2000_Functions.ctl" Type="VI" URL="../Drivers/dpi4 map KeithleyDMM2000/KeithleyDMM2000_Functions.ctl"/>
			<Item Name="KeithleyDMM2000_ESR_check.vi" Type="VI" URL="../Drivers/dpi4 map KeithleyDMM2000/KeithleyDMM2000_ESR_check.vi"/>
			<Item Name="R&amp;S_DMM_Models.ctl" Type="VI" URL="../Drivers/dpi4 map R&amp;S HMC DMM/R&amp;S_DMM_Models.ctl"/>
			<Item Name="R&amp;SQuery.vi" Type="VI" URL="../Drivers/dpi4 map R&amp;S HMC DMM/R&amp;SQuery.vi"/>
			<Item Name="R&amp;S_DMM_ESR_check.vi" Type="VI" URL="../Drivers/dpi4 map R&amp;S HMC DMM/R&amp;S_DMM_ESR_check.vi"/>
			<Item Name="en33_DATA.ctl" Type="VI" URL="../Drivers/dpi4 map Energomonitor_3_3/data/en33_DATA.ctl"/>
			<Item Name="en33_DATA_mass.ctl" Type="VI" URL="../Drivers/dpi4 map Energomonitor_3_3/data/en33_DATA_mass.ctl"/>
			<Item Name="ENERGOMONITOR_3_3_calculate_CRC.vi" Type="VI" URL="../Drivers/dpi4 map Energomonitor_3_3/ENERGOMONITOR_3_3_calculate_CRC.vi"/>
			<Item Name="DjLL_CRC_CCITT(XMODEM)(16bit).vi" Type="VI" URL="../Drivers/dpi4 map Energomonitor_3_3/driver_jr_hex/DjLL_CRC_CCITT(XMODEM)(16bit).vi"/>
			<Item Name="DjLL_1_2 HEX to 2 HEX.vi" Type="VI" URL="../Drivers/dpi4 map Energomonitor_3_3/driver_jr_hex/DjLL_1_2 HEX to 2 HEX.vi"/>
			<Item Name="DjLL_HEXstring_READ_BUFFER.vi" Type="VI" URL="../Drivers/dpi4 map Energomonitor_3_3/driver_jr_hex/DjLL_HEXstring_READ_BUFFER.vi"/>
			<Item Name="ENERGOMONITOR_3_3_CHECK_CRC_AND_BUFF.vi" Type="VI" URL="../Drivers/dpi4 map Energomonitor_3_3/ENERGOMONITOR_3_3_CHECK_CRC_AND_BUFF.vi"/>
			<Item Name="data_mode_2.vi" Type="VI" URL="../Drivers/dpi4 map Energomonitor_3_3/data/data_mode_2.vi"/>
			<Item Name="ENERGOMONITOR_3_3_analysis_setup.vi" Type="VI" URL="../Drivers/dpi4 map Energomonitor_3_3/small_vies/ENERGOMONITOR_3_3_analysis_setup.vi"/>
			<Item Name="ENERGOMONITOR_3_3_parsing_parameter.vi" Type="VI" URL="../Drivers/dpi4 map Energomonitor_3_3/small_vies/ENERGOMONITOR_3_3_parsing_parameter.vi"/>
			<Item Name="data_mode_3.vi" Type="VI" URL="../Drivers/dpi4 map Energomonitor_3_3/data/data_mode_3.vi"/>
			<Item Name="ENERGOMONITOR_3_3_analysis_parameters_1-46.vi" Type="VI" URL="../Drivers/dpi4 map Energomonitor_3_3/small_vies/ENERGOMONITOR_3_3_analysis_parameters_1-46.vi"/>
			<Item Name="ENERGOMONITOR_3_3_convert_one_word_drobn.vi" Type="VI" URL="../Drivers/dpi4 map Energomonitor_3_3/small_vies/ENERGOMONITOR_3_3_convert_one_word_drobn.vi"/>
			<Item Name="ENERGOMONITOR_3_3_convert_two_words.vi" Type="VI" URL="../Drivers/dpi4 map Energomonitor_3_3/small_vies/ENERGOMONITOR_3_3_convert_two_words.vi"/>
			<Item Name="ENERGOMETR_3_3_BINstring_to_DECnumber.vi" Type="VI" URL="../Drivers/dpi4 map Energomonitor_3_3/small_vies/ENERGOMETR_3_3_BINstring_to_DECnumber.vi"/>
			<Item Name="ENERGOMONITOR_3_3_convert_quart_words_with_sign.vi" Type="VI" URL="../Drivers/dpi4 map Energomonitor_3_3/small_vies/ENERGOMONITOR_3_3_convert_quart_words_with_sign.vi"/>
			<Item Name="data_mode_3_v2_5_26.vi" Type="VI" URL="../Drivers/dpi4 map Energomonitor_3_3/data/data_mode_3_v2_5_26.vi"/>
			<Item Name="Fluke43B_ESR_check.vi" Type="VI" URL="../Drivers/dpi4 map Fluke43B/Fluke43B_ESR_check.vi"/>
			<Item Name="Volt_Cur_Freq.ctl" Type="VI" URL="../Drivers/dpi4 map Fluke43X/Volt_Cur_Freq.ctl"/>
			<Item Name="Power_Energy.ctl" Type="VI" URL="../Drivers/dpi4 map Fluke43X/Power_Energy.ctl"/>
			<Item Name="Fliker.ctl" Type="VI" URL="../Drivers/dpi4 map Fluke43X/Fliker.ctl"/>
			<Item Name="Harmonics.ctl" Type="VI" URL="../Drivers/dpi4 map Fluke43X/Harmonics.ctl"/>
			<Item Name="DISBALANS.ctl" Type="VI" URL="../Drivers/dpi4 map Fluke43X/DISBALANS.ctl"/>
			<Item Name="Model.ctl" Type="VI" URL="../Drivers/dpi4 map Fluke43X/Model.ctl"/>
			<Item Name="Volt_Cur_Freq_43X-II.ctl" Type="VI" URL="../Drivers/dpi4 map Fluke43X/Volt_Cur_Freq_43X-II.ctl"/>
			<Item Name="Power_Energy_43X-II.ctl" Type="VI" URL="../Drivers/dpi4 map Fluke43X/Power_Energy_43X-II.ctl"/>
			<Item Name="Harmonics_43X-II.ctl" Type="VI" URL="../Drivers/dpi4 map Fluke43X/Harmonics_43X-II.ctl"/>
			<Item Name="Fluke43_ESR_check.vi" Type="VI" URL="../Drivers/dpi4 map Fluke43X/Fluke43_ESR_check.vi"/>
			<Item Name="Fluke345_PARS_ARRAY.vi" Type="VI" URL="../Drivers/dpi4 map Fluke43X/Fluke345_PARS_ARRAY.vi"/>
			<Item Name="Fluke43X-II_PARS_ARRAY.vi" Type="VI" URL="../Drivers/dpi4 map Fluke43X/Fluke43X-II_PARS_ARRAY.vi"/>
			<Item Name="Fluke345_POWER_ENERGY.vi" Type="VI" URL="../Drivers/dpi4 map Fluke43X/Fluke345_POWER_ENERGY.vi"/>
			<Item Name="Fluke43X-II_POWER_ENERGY.vi" Type="VI" URL="../Drivers/dpi4 map Fluke43X/Fluke43X-II_POWER_ENERGY.vi"/>
			<Item Name="Fluke345_HARM.vi" Type="VI" URL="../Drivers/dpi4 map Fluke43X/Fluke345_HARM.vi"/>
			<Item Name="Fluke43X-II_HARM.vi" Type="VI" URL="../Drivers/dpi4 map Fluke43X/Fluke43X-II_HARM.vi"/>
			<Item Name="Fluke345_FLIKER.vi" Type="VI" URL="../Drivers/dpi4 map Fluke43X/Fluke345_FLIKER.vi"/>
			<Item Name="Fluke345_DISBALANS.vi" Type="VI" URL="../Drivers/dpi4 map Fluke43X/Fluke345_DISBALANS.vi"/>
			<Item Name="resurs_command.ctl" Type="VI" URL="../Drivers/dpi4 map ResursK2/resurs_command.ctl"/>
			<Item Name="RESURS_K2_command.vi" Type="VI" URL="../Drivers/dpi4 map ResursK2/RESURS_K2_command.vi"/>
			<Item Name="ResursK2ClusterIntoArray.vi" Type="VI" URL="../Drivers/dpi4 map ResursK2/help function kostia/ResursK2ClusterIntoArray.vi"/>
			<Item Name="ReplaceElementsOfArrayFromTo.vi" Type="VI" URL="../Drivers/dpi4 map ResursK2/help function kostia/ReplaceElementsOfArrayFromTo.vi"/>
			<Item Name="RESURS_K2_ERR_PARS.vi" Type="VI" URL="../Drivers/dpi4 map ResursK2/RESURS_K2_ERR_PARS.vi"/>
			<Item Name="RESURS_K2_command_frame_choise_port.vi" Type="VI" URL="../Drivers/dpi4 map ResursK2/RESURS_K2_command_frame_choise_port.vi"/>
			<Item Name="Default_Values_For_ResursK2.vi" Type="VI" URL="../Drivers/dpi4 map ResursK2/help function kostia/Default_Values_For_ResursK2.vi"/>
			<Item Name="УК1_DrLL_ModBus_Query_Holding_Register.vi" Type="VI" URL="../Drivers/dpi4 map UK1/УК1_DrLL_ModBus_Query_Holding_Register.vi"/>
			<Item Name="УК1_HoldigHexRegToDec(BIG ENDIAN INT16_32).vi" Type="VI" URL="../Drivers/dpi4 map UK1/УК1_HoldigHexRegToDec(BIG ENDIAN INT16_32).vi"/>
			<Item Name="УК1_DjLL_HEXstring_TO_WRITEstring.vi" Type="VI" URL="../Drivers/dpi4 map UK1/УК1_DjLL_HEXstring_TO_WRITEstring.vi"/>
			<Item Name="УК1_DrLL_ModBus_Write_Multi_Holding_Register.vi" Type="VI" URL="../Drivers/dpi4 map UK1/УК1_DrLL_ModBus_Write_Multi_Holding_Register.vi"/>
			<Item Name="УК1_DrLL_ModBus_Write_Multi_Holding_Register_LittleEndian.vi" Type="VI" URL="../Drivers/dpi4 map UK1/УК1_DrLL_ModBus_Write_Multi_Holding_Register_LittleEndian.vi"/>
			<Item Name="УК1_DrLL_ModBus_Write_Holding_Register.vi" Type="VI" URL="../Drivers/dpi4 map UK1/УК1_DrLL_ModBus_Write_Holding_Register.vi"/>
			<Item Name="Octava101a_DATA.ctl" Type="VI" URL="../Drivers/dpi4 map OCTAVA101A/Octava101a_DATA.ctl"/>
			<Item Name="octava101a_write.vi" Type="VI" URL="../Drivers/dpi4 map OCTAVA101A/octava101a_write.vi"/>
			<Item Name="octava101a_READ_BUFFER.vi" Type="VI" URL="../Drivers/dpi4 map OCTAVA101A/octava101a_READ_BUFFER.vi"/>
			<Item Name="octava101a_CHECK_CRC_AND_BUFF.vi" Type="VI" URL="../Drivers/dpi4 map OCTAVA101A/octava101a_CHECK_CRC_AND_BUFF.vi"/>
			<Item Name="octava101a_calc_first_string_DATA.vi" Type="VI" URL="../Drivers/dpi4 map OCTAVA101A/octava101a_calc_first_string_DATA.vi"/>
			<Item Name="octava101a_1_255.vi" Type="VI" URL="../Drivers/dpi4 map OCTAVA101A/octava101a_1_255.vi"/>
			<Item Name="main excel.vi" Type="VI" URL="../Drivers/dpi4 map OCTAVA101A/work with excel/main excel.vi"/>
			<Item Name="Чтение Excel.vi" Type="VI" URL="../Drivers/dpi4 map OCTAVA101A/work with excel/Чтение Excel.vi"/>
			<Item Name="Row Col To Range Format.vi" Type="VI" URL="../Drivers/dpi4 map OCTAVA101A/work with excel/Row Col To Range Format.vi"/>
			<Item Name="octava101a_BIT_CHANGE.vi" Type="VI" URL="../Drivers/dpi4 map OCTAVA101A/octava101a_BIT_CHANGE.vi"/>
			<Item Name="octava101a_StandartRangeSelect (SubVI).vi" Type="VI" URL="../Drivers/dpi4 map OCTAVA101A/octava101a_StandartRangeSelect (SubVI).vi"/>
			<Item Name="BETAGAUGE_PI_Check_Error.vi" Type="VI" URL="../Drivers/dpi4 map Betagauge/BETAGAUGE_PI_Check_Error.vi"/>
			<Item Name="BETAGAUGE_check_mode.vi" Type="VI" URL="../Drivers/dpi4 map Betagauge/BETAGAUGE_check_mode.vi"/>
			<Item Name="METRAN517_Check_Error.vi" Type="VI" URL="../Drivers/dpi4 map Metran517/METRAN517_Check_Error.vi"/>
			<Item Name="Out_Off_N4_101.vi" Type="VI" URL="../Drivers/dpi4 map N4_101/Out_Off_N4_101.vi"/>
			<Item Name="N4_101_DelayAndWrite.vi" Type="VI" URL="../Drivers/dpi4 map N4_101/N4_101_DelayAndWrite.vi"/>
			<Item Name="N4_101_StandartRangeSelect (SubVI).vi" Type="VI" URL="../Drivers/dpi4 map N4_101/N4_101_StandartRangeSelect (SubVI).vi"/>
			<Item Name="N4_101_write_mode.vi" Type="VI" URL="../Drivers/dpi4 map N4_101/N4_101_write_mode.vi"/>
			<Item Name="N4_101_Query.vi" Type="VI" URL="../Drivers/dpi4 map N4_101/N4_101_Query.vi"/>
			<Item Name="N4_101_WriteAndCheck.vi" Type="VI" URL="../Drivers/dpi4 map N4_101/N4_101_WriteAndCheck.vi"/>
			<Item Name="N4_101_check_mode.vi" Type="VI" URL="../Drivers/dpi4 map N4_101/N4_101_check_mode.vi"/>
			<Item Name="N4_101_write_range.vi" Type="VI" URL="../Drivers/dpi4 map N4_101/N4_101_write_range.vi"/>
			<Item Name="N4_101_write_P.vi" Type="VI" URL="../Drivers/dpi4 map N4_101/N4_101_write_P.vi"/>
			<Item Name="Out_On_N4_101.vi" Type="VI" URL="../Drivers/dpi4 map N4_101/Out_On_N4_101.vi"/>
			<Item Name="N4_101_Adjust.vi" Type="VI" URL="../Drivers/dpi4 map N4_101/N4_101_Adjust.vi"/>
			<Item Name="Out_Off_N4_201.vi" Type="VI" URL="../Drivers/dpi4 map N4_201/Out_Off_N4_201.vi"/>
			<Item Name="N4_201_DelayAndWrite.vi" Type="VI" URL="../Drivers/dpi4 map N4_201/N4_201_DelayAndWrite.vi"/>
			<Item Name="N4_201_StandartRangeSelect (SubVI).vi" Type="VI" URL="../Drivers/dpi4 map N4_201/N4_201_StandartRangeSelect (SubVI).vi"/>
			<Item Name="N4_201_write_mode.vi" Type="VI" URL="../Drivers/dpi4 map N4_201/N4_201_write_mode.vi"/>
			<Item Name="N4_201_Query.vi" Type="VI" URL="../Drivers/dpi4 map N4_201/N4_201_Query.vi"/>
			<Item Name="N4_201_WriteAndCheck.vi" Type="VI" URL="../Drivers/dpi4 map N4_201/N4_201_WriteAndCheck.vi"/>
			<Item Name="N4_201_check_mode.vi" Type="VI" URL="../Drivers/dpi4 map N4_201/N4_201_check_mode.vi"/>
			<Item Name="N4_201_write_range.vi" Type="VI" URL="../Drivers/dpi4 map N4_201/N4_201_write_range.vi"/>
			<Item Name="N4_201_write_P.vi" Type="VI" URL="../Drivers/dpi4 map N4_201/N4_201_write_P.vi"/>
			<Item Name="Out_On_N4_201.vi" Type="VI" URL="../Drivers/dpi4 map N4_201/Out_On_N4_201.vi"/>
			<Item Name="N4_201_Adjust.vi" Type="VI" URL="../Drivers/dpi4 map N4_201/N4_201_Adjust.vi"/>
			<Item Name="E7_20_READ_BUFFER.vi" Type="VI" URL="../Drivers/dpi4 map E7_20/E7_20_READ_BUFFER.vi"/>
			<Item Name="E7_20_write.vi" Type="VI" URL="../Drivers/dpi4 map E7_20/E7_20_write.vi"/>
			<Item Name="E7_20_readstring.vi" Type="VI" URL="../Drivers/dpi4 map E7_20/E7_20_readstring.vi"/>
			<Item Name="E7_28_write.vi" Type="VI" URL="../Drivers/dpi4 map E7_28/E7_28_write.vi"/>
			<Item Name="E7_28_READ_BUFFER_2.vi" Type="VI" URL="../Drivers/dpi4 map E7_28/E7_28_READ_BUFFER_2.vi"/>
			<Item Name="E7_28_check.vi" Type="VI" URL="../Drivers/dpi4 map E7_28/E7_28_check.vi"/>
			<Item Name="E7_28_readstring_2.vi" Type="VI" URL="../Drivers/dpi4 map E7_28/E7_28_readstring_2.vi"/>
			<Item Name="E7_30_write.vi" Type="VI" URL="../Drivers/dpi4 map E7_30/E7_30_write.vi"/>
			<Item Name="E7_30_READ_BUFFER_2.vi" Type="VI" URL="../Drivers/dpi4 map E7_30/E7_30_READ_BUFFER_2.vi"/>
			<Item Name="E7_30_check.vi" Type="VI" URL="../Drivers/dpi4 map E7_30/E7_30_check.vi"/>
			<Item Name="E7_30_readstring_2.vi" Type="VI" URL="../Drivers/dpi4 map E7_30/E7_30_readstring_2.vi"/>
			<Item Name="E7_30_DATA.ctl" Type="VI" URL="../Drivers/dpi4 map E7_30/E7_30_DATA.ctl"/>
			<Item Name="Out_Off_N4_6.vi" Type="VI" URL="../Drivers/dpi4 map N4_6/Out_Off_N4_6.vi"/>
			<Item Name="N4_6_WriteSentenceWithDelay.vi" Type="VI" URL="../Drivers/dpi4 map N4_6/N4_6_WriteSentenceWithDelay.vi"/>
			<Item Name="N4_6_write.vi" Type="VI" URL="../Drivers/dpi4 map N4_6/N4_6_write.vi"/>
			<Item Name="Out_On_N4_6.vi" Type="VI" URL="../Drivers/dpi4 map N4_6/Out_On_N4_6.vi"/>
			<Item Name="N4_6_WriteAndCheck.vi" Type="VI" URL="../Drivers/dpi4 map N4_6/N4_6_WriteAndCheck.vi"/>
			<Item Name="N4_6_check_mode.vi" Type="VI" URL="../Drivers/dpi4 map N4_6/N4_6_check_mode.vi"/>
			<Item Name="N4_6_Query.vi" Type="VI" URL="../Drivers/dpi4 map N4_6/N4_6_Query.vi"/>
			<Item Name="N4_6_F_param.vi" Type="VI" URL="../Drivers/dpi4 map N4_6/N4_6_F_param.vi"/>
			<Item Name="N4_6_Query_DC.vi" Type="VI" URL="../Drivers/dpi4 map N4_6/N4_6_Query_DC.vi"/>
			<Item Name="N4_6_Query_Mode.vi" Type="VI" URL="../Drivers/dpi4 map N4_6/N4_6_Query_Mode.vi"/>
			<Item Name="N4_6_FormatStringValue.vi" Type="VI" URL="../Drivers/dpi4 map N4_6/N4_6_FormatStringValue.vi"/>
			<Item Name="N4_6_Range_select.vi" Type="VI" URL="../Drivers/dpi4 map N4_6/N4_6_Range_select.vi"/>
			<Item Name="N4_6_StandartRangeSelect (SubVI).vi" Type="VI" URL="../Drivers/dpi4 map N4_6/N4_6_StandartRangeSelect (SubVI).vi"/>
			<Item Name="N4_6_Query_R.vi" Type="VI" URL="../Drivers/dpi4 map N4_6/N4_6_Query_R.vi"/>
			<Item Name="N4_6_write_R.vi" Type="VI" URL="../Drivers/dpi4 map N4_6/N4_6_write_R.vi"/>
			<Item Name="N4_6_FREQ_FormatStringValue.vi" Type="VI" URL="../Drivers/dpi4 map N4_6/N4_6_FREQ_FormatStringValue.vi"/>
			<Item Name="N4_6_ACWriteAndCheck.vi" Type="VI" URL="../Drivers/dpi4 map N4_6/N4_6_ACWriteAndCheck.vi"/>
			<Item Name="N4_6_ESR_check.vi" Type="VI" URL="../Drivers/dpi4 map N4_6/N4_6_ESR_check.vi"/>
			<Item Name="N4_6_Read.vi" Type="VI" URL="../Drivers/dpi4 map N4_6/N4_6_Read.vi"/>
			<Item Name="N4_11_INIT.vi" Type="VI" URL="../Drivers/dpi4 map N4_11/Function/N4_11_INIT.vi"/>
			<Item Name="N4_11_DriverSettings.ctl" Type="VI" URL="../Drivers/dpi4 map N4_11/Function/N4_11_DriverSettings.ctl"/>
			<Item Name="N4_11_COMMAND.vi" Type="VI" URL="../Drivers/dpi4 map N4_11/Function/N4_11_COMMAND.vi"/>
			<Item Name="N4_11_READ.vi" Type="VI" URL="../Drivers/dpi4 map N4_11/Function/N4_11_READ.vi"/>
			<Item Name="Out_Off_N4_12.vi" Type="VI" URL="../Drivers/dpi4 map N4_12/Out_Off_N4_12.vi"/>
			<Item Name="N4_12_WriteSentenceWithDelay.vi" Type="VI" URL="../Drivers/dpi4 map N4_12/N4_12_WriteSentenceWithDelay.vi"/>
			<Item Name="N4_12_write.vi" Type="VI" URL="../Drivers/dpi4 map N4_12/N4_12_write.vi"/>
			<Item Name="Out_On_N4_12.vi" Type="VI" URL="../Drivers/dpi4 map N4_12/Out_On_N4_12.vi"/>
			<Item Name="N4_12_WriteAndCheck.vi" Type="VI" URL="../Drivers/dpi4 map N4_12/N4_12_WriteAndCheck.vi"/>
			<Item Name="N4_12_check_mode.vi" Type="VI" URL="../Drivers/dpi4 map N4_12/N4_12_check_mode.vi"/>
			<Item Name="N4_12_Query.vi" Type="VI" URL="../Drivers/dpi4 map N4_12/N4_12_Query.vi"/>
			<Item Name="N4_12_Query_DC.vi" Type="VI" URL="../Drivers/dpi4 map N4_12/N4_12_Query_DC.vi"/>
			<Item Name="N4_12_Query_Mode.vi" Type="VI" URL="../Drivers/dpi4 map N4_12/N4_12_Query_Mode.vi"/>
			<Item Name="N4_12_FormatStringValue.vi" Type="VI" URL="../Drivers/dpi4 map N4_12/N4_12_FormatStringValue.vi"/>
			<Item Name="N4_12_Range_select.vi" Type="VI" URL="../Drivers/dpi4 map N4_12/N4_12_Range_select.vi"/>
			<Item Name="Out_Off_N4_17.vi" Type="VI" URL="../Drivers/dpi4 map N4_17/Out_Off_N4_17.vi"/>
			<Item Name="N4_17_write.vi" Type="VI" URL="../Drivers/dpi4 map N4_17/N4_17_write.vi"/>
			<Item Name="Out_On_N4_17.vi" Type="VI" URL="../Drivers/dpi4 map N4_17/Out_On_N4_17.vi"/>
			<Item Name="N4_17_WriteAndCheck.vi" Type="VI" URL="../Drivers/dpi4 map N4_17/N4_17_WriteAndCheck.vi"/>
			<Item Name="N4_17_check_mode.vi" Type="VI" URL="../Drivers/dpi4 map N4_17/N4_17_check_mode.vi"/>
			<Item Name="N4_17_Query.vi" Type="VI" URL="../Drivers/dpi4 map N4_17/N4_17_Query.vi"/>
			<Item Name="N4_17_F_param.vi" Type="VI" URL="../Drivers/dpi4 map N4_17/N4_17_F_param.vi"/>
			<Item Name="N4_17_Query_DC.vi" Type="VI" URL="../Drivers/dpi4 map N4_17/N4_17_Query_DC.vi"/>
			<Item Name="N4_17_Query_Mode.vi" Type="VI" URL="../Drivers/dpi4 map N4_17/N4_17_Query_Mode.vi"/>
			<Item Name="N4_17_FormatStringValue.vi" Type="VI" URL="../Drivers/dpi4 map N4_17/N4_17_FormatStringValue.vi"/>
			<Item Name="N4_17_Range_select.vi" Type="VI" URL="../Drivers/dpi4 map N4_17/N4_17_Range_select.vi"/>
			<Item Name="N4_17_Query_R.vi" Type="VI" URL="../Drivers/dpi4 map N4_17/N4_17_Query_R.vi"/>
			<Item Name="N4_17_write_R.vi" Type="VI" URL="../Drivers/dpi4 map N4_17/N4_17_write_R.vi"/>
			<Item Name="N4_17_FREQ_FormatStringValue.vi" Type="VI" URL="../Drivers/dpi4 map N4_17/N4_17_FREQ_FormatStringValue.vi"/>
			<Item Name="N4_17_ACWriteAndCheck.vi" Type="VI" URL="../Drivers/dpi4 map N4_17/N4_17_ACWriteAndCheck.vi"/>
			<Item Name="AnritsuSGmodels.ctl" Type="VI" URL="../Drivers/dpi4 map AnritsuSG/AnritsuSGmodels.ctl"/>
			<Item Name="AnritsuSG_ErrorCheck.vi" Type="VI" URL="../Drivers/dpi4 map AnritsuSG/AnritsuSG_ErrorCheck.vi"/>
			<Item Name="Out_Off_AnritsuMG.vi" Type="VI" URL="../Drivers/dpi4 map AnritsuSG/Out_Off_AnritsuMG.vi"/>
			<Item Name="AnritsuSAmodels.ctl" Type="VI" URL="../Drivers/dpi4 map AnritsuSA/AnritsuSAmodels.ctl"/>
			<Item Name="AnritsuSA_ErrorCheck.vi" Type="VI" URL="../Drivers/dpi4 map AnritsuSA/AnritsuSA_ErrorCheck.vi"/>
			<Item Name="AnritsuMS2720T_CheckError.vi" Type="VI" URL="../Drivers/dpi4 map AnritsuSA/AnritsuMS2720T_CheckError.vi"/>
			<Item Name="AnritsuMS2720T_Reset2SPA.vi" Type="VI" URL="../Drivers/dpi4 map AnritsuSA/AnritsuMS2720T_Reset2SPA.vi"/>
			<Item Name="AnritsuMS2720T_SweepDone.vi" Type="VI" URL="../Drivers/dpi4 map AnritsuSA/AnritsuMS2720T_SweepDone.vi"/>
			<Item Name="AnritsuSA_Query.vi" Type="VI" URL="../Drivers/dpi4 map AnritsuSA/AnritsuSA_Query.vi"/>
			<Item Name="Interface_Write.vi" Type="VI" URL="../InterfaceLowLevel/Interface_Write.vi"/>
			<Item Name="Interface_Read.vi" Type="VI" URL="../InterfaceLowLevel/Interface_Read.vi"/>
			<Item Name="Interface_Set_EOS.vi" Type="VI" URL="../InterfaceLowLevel/Interface_Set_EOS.vi"/>
			<Item Name="Interface_Set_TMO.vi" Type="VI" URL="../InterfaceLowLevel/Interface_Set_TMO.vi"/>
			<Item Name="Driver_Select_Interface.vi" Type="VI" URL="../InterfaceLowLevel/Driver_Select_Interface.vi"/>
			<Item Name="Interface_Set_GPIB_address.vi" Type="VI" URL="../InterfaceLowLevel/Interface_Set_GPIB_address.vi"/>
			<Item Name="Interface_ESR_check.vi" Type="VI" URL="../InterfaceLowLevel/Interface_ESR_check.vi"/>
			<Item Name="MT8815B_QUERY.vi" Type="VI" URL="../Drivers/dpi4 map MT8815B/MT8815B_QUERY.vi"/>
			<Item Name="MT8815_Message.vi" Type="VI" URL="../Drivers/dpi4 map MT8815B/MT8815_Message.vi"/>
			<Item Name="MT8815_SET_CALL.vi" Type="VI" URL="../Drivers/dpi4 map MT8815B/MT8815_SET_CALL.vi"/>
			<Item Name="MT88XX_DIALOG.vi" Type="VI" URL="../Drivers/dpi4 map MT8815B/MT88XX_DIALOG.vi"/>
			<Item Name="MT88XX_SET_RACH.vi" Type="VI" URL="../Drivers/dpi4 map MT8815B/MT88XX_SET_RACH.vi"/>
			<Item Name="MT8815B_MEAS_DONE.vi" Type="VI" URL="../Drivers/dpi4 map MT8815B/MT8815B_MEAS_DONE.vi"/>
			<Item Name="Max Value String(SubVI).vi" Type="VI" URL="../Drivers/dpi4 map MT8815B/Max Value String(SubVI).vi"/>
			<Item Name="G3_122_write.vi" Type="VI" URL="../Drivers/dpi4 map G3_122/G3_122_write.vi"/>
			<Item Name="G3_122_checkSTB.vi" Type="VI" URL="../Drivers/dpi4 map G3_122/G3_122_checkSTB.vi"/>
			<Item Name="G5_82_write.vi" Type="VI" URL="../Drivers/dpi4 map G5_82/G5_82_write.vi"/>
			<Item Name="RG4_04_write.vi" Type="VI" URL="../Drivers/dpi4 map RG4_04/RG4_04_write.vi"/>
			<Item Name="RG4_04_checkSTB.vi" Type="VI" URL="../Drivers/dpi4 map RG4_04/RG4_04_checkSTB.vi"/>
			<Item Name="Stanford DS360_ErrorCheck.vi" Type="VI" URL="../Drivers/dpi4 map StanfordDS360/Stanford DS360_ErrorCheck.vi"/>
			<Item Name="Stanford DS360_query_with_check.vi" Type="VI" URL="../Drivers/dpi4 map StanfordDS360/Stanford DS360_query_with_check.vi"/>
			<Item Name="G4_input_freq.vi" Type="VI" URL="../Drivers/dpi4 map G3_221/G4_input_freq.vi"/>
			<Item Name="System" Type="VI" URL="System">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="user32.dll" Type="Document" URL="user32.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="Advapi32.dll" Type="Document" URL="Advapi32.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="kernel32.dll" Type="Document" URL="kernel32.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="AgilentRF_Models.ctl" Type="VI" URL="../Drivers/dpi4 map AgilentRF/AgilentRF_Models.ctl"/>
			<Item Name="AgilentRF_ESR_check.vi" Type="VI" URL="../Drivers/dpi4 map AgilentRF/AgilentRF_ESR_check.vi"/>
			<Item Name="Instek_DMM8212_Func.ctl" Type="VI" URL="../Drivers/dpi4 map Instek8212/Instek_DMM8212_Func.ctl"/>
			<Item Name="Instek8212Query.vi" Type="VI" URL="../Drivers/dpi4 map Instek8212/Instek8212Query.vi"/>
			<Item Name="InstekDMM8212_RangeSelect.vi" Type="VI" URL="../Drivers/dpi4 map Instek8212/InstekDMM8212_RangeSelect.vi"/>
			<Item Name="Instek8212Write.vi" Type="VI" URL="../Drivers/dpi4 map Instek8212/Instek8212Write.vi"/>
			<Item Name="B7_72_82_StandartRangeSelect (SubVI).vi" Type="VI" URL="../Drivers/dpi4 map B7_82/B7_72_82_StandartRangeSelect (SubVI).vi"/>
			<Item Name="N4_17_StandartRangeSelect (SubVI).vi" Type="VI" URL="../Drivers/dpi4 map N4_17/N4_17_StandartRangeSelect (SubVI).vi"/>
			<Item Name="ENERGOMETR_3_3_HEXstring_to_BINstring.vi" Type="VI" URL="../Drivers/dpi4 map Energomonitor_3_3/small_vies/ENERGOMETR_3_3_HEXstring_to_BINstring.vi"/>
			<Item Name="Models.ctl" Type="VI" URL="../Drivers/dpi4 map Fluke5000/Models.ctl"/>
			<Item Name="XP2i_Check_Error.vi" Type="VI" URL="../Drivers/dpi4 map CrystalXP2/XP2i_Check_Error.vi"/>
			<Item Name="Fluke8508A_ESR_check.vi" Type="VI" URL="../Drivers/dpi4 map Fluke8508A/Fluke8508A_ESR_check.vi"/>
			<Item Name="N4_17_WriteSentenceWithDelay.vi" Type="VI" URL="../Drivers/dpi4 map N4_17/N4_17_WriteSentenceWithDelay.vi"/>
			<Item Name="InstekDMM7_RangeSelect.vi" Type="VI" URL="../Drivers/dpi4 map InstekDMM7/InstekDMM7_RangeSelect.vi"/>
			<Item Name="DjLL_HEXstring_TO_WRITEstring.vi" Type="VI" URL="../Drivers/dpi4 map Energomonitor_3_3/driver_jr_hex/DjLL_HEXstring_TO_WRITEstring.vi"/>
			<Item Name="DjLL_HEX_TO_HEXstring.vi" Type="VI" URL="../Drivers/dpi4 map Energomonitor_3_3/driver_jr_hex/DjLL_HEX_TO_HEXstring.vi"/>
			<Item Name="StrN+E2Num.vi" Type="VI" URL="../Drivers/dpi4 map Instek_GPM_8212/StrN+E2Num.vi"/>
			<Item Name="InstekDMM7_CONF.ctl" Type="VI" URL="../Drivers/dpi4 map InstekDMM7/InstekDMM7_CONF.ctl"/>
			<Item Name="N4_12_FREQ_FormatStringValue.vi" Type="VI" URL="../Drivers/dpi4 map N4_12/N4_12_FREQ_FormatStringValue.vi"/>
			<Item Name="KM300_show_for_adjust.ctl" Type="VI" URL="../Drivers/dpi4 map KM300/Function/KM300_show_for_adjust.ctl"/>
			<Item Name="KM300_mode.ctl" Type="VI" URL="../Drivers/dpi4 map KM300/Function/KM300_mode.ctl"/>
			<Item Name="KM300_show_True_value.ctl" Type="VI" URL="../Drivers/dpi4 map KM300/Function/KM300_show_True_value.ctl"/>
			<Item Name="KM300_show_maska.ctl" Type="VI" URL="../Drivers/dpi4 map KM300/Function/KM300_show_maska.ctl"/>
			<Item Name="KM300_show_hex_pocket.ctl" Type="VI" URL="../Drivers/dpi4 map KM300/Function/KM300_show_hex_pocket.ctl"/>
			<Item Name="KM300_DriverSettings_Status.ctl" Type="VI" URL="../Drivers/dpi4 map KM300/Function/KM300_DriverSettings_Status.ctl"/>
			<Item Name="KM300_show_information.ctl" Type="VI" URL="../Drivers/dpi4 map KM300/Function/KM300_show_information.ctl"/>
			<Item Name="KM300_DriverSettings.ctl" Type="VI" URL="../Drivers/dpi4 map KM300/Function/KM300_DriverSettings.ctl"/>
			<Item Name="E7_28_DATA.ctl" Type="VI" URL="../Drivers/dpi4 map E7_28/E7_28_DATA.ctl"/>
			<Item Name="Calibration_Message.vi" Type="VI" URL="../Drivers/dpi4 map Fluke8508A/Calibration_Message.vi"/>
			<Item Name="Fluke12x_Init.vi" Type="VI" URL="../Drivers/dpi4 map Fluke12x/Fluke12x_Init.vi"/>
			<Item Name="Initialize.vi" Type="VI" URL="../Drivers/dpi4 map Fluke12x/LowLevel/Initialize.vi"/>
			<Item Name="Identification.vi" Type="VI" URL="../Drivers/dpi4 map Fluke12x/LowLevel/Identification.vi"/>
			<Item Name="Fluke12x_SendCommand.vi" Type="VI" URL="../Drivers/dpi4 map Fluke12x/LowLevel/Fluke12x_SendCommand.vi"/>
			<Item Name="Get Bytes Until CR WTimeout.vi" Type="VI" URL="../Drivers/dpi4 map Fluke12x/LowLevel/Get Bytes Until CR WTimeout.vi"/>
			<Item Name="Reset.vi" Type="VI" URL="../Drivers/dpi4 map Fluke12x/LowLevel/Reset.vi"/>
			<Item Name="Utility Default Instrument Setup.vi" Type="VI" URL="../Drivers/dpi4 map Fluke12x/LowLevel/Utility Default Instrument Setup.vi"/>
			<Item Name="Utility Clean Up Initialize.vi" Type="VI" URL="../Drivers/dpi4 map Fluke12x/LowLevel/Utility Clean Up Initialize.vi"/>
			<Item Name="Change Baud.vi" Type="VI" URL="../Drivers/dpi4 map Fluke12x/LowLevel/Change Baud.vi"/>
			<Item Name="Program Communication.vi" Type="VI" URL="../Drivers/dpi4 map Fluke12x/LowLevel/Program Communication.vi"/>
			<Item Name="Fluke12x_SelectInterface.vi" Type="VI" URL="../Drivers/dpi4 map Fluke12x/Fluke12x_SelectInterface.vi"/>
			<Item Name="Fluke12x_Query Setup.vi" Type="VI" URL="../Drivers/dpi4 map Fluke12x/LowLevel/Fluke12x_Query Setup.vi"/>
			<Item Name="Get N Bytes WTimeout.vi" Type="VI" URL="../Drivers/dpi4 map Fluke12x/LowLevel/Get N Bytes WTimeout.vi"/>
			<Item Name="Fluke12x_GetSetupNode.vi" Type="VI" URL="../Drivers/dpi4 map Fluke12x/LowLevel/Fluke12x_GetSetupNode.vi"/>
			<Item Name="Fluke12x_ErrorQuery.vi" Type="VI" URL="../Drivers/dpi4 map Fluke12x/Fluke12x_ErrorQuery.vi"/>
			<Item Name="Error Query.vi" Type="VI" URL="../Drivers/dpi4 map Fluke12x/LowLevel/Error Query.vi"/>
			<Item Name="Error Message.vi" Type="VI" URL="../Drivers/dpi4 map Fluke12x/LowLevel/Error Message.vi"/>
			<Item Name="Auto Setup.vi" Type="VI" URL="../Drivers/dpi4 map Fluke12x/LowLevel/Auto Setup.vi"/>
			<Item Name="Fluke12x_ValCompare.vi" Type="VI" URL="../Drivers/dpi4 map Fluke12x/Fluke12x_ValCompare.vi"/>
			<Item Name="Fluke12x_Setmanual.vi" Type="VI" URL="../Drivers/dpi4 map Fluke12x/Fluke12x_Setmanual.vi"/>
			<Item Name="Fluke12x_SearchNodes_New.vi" Type="VI" URL="../Drivers/dpi4 map Fluke12x/Fluke12x_SearchNodes_New.vi"/>
			<Item Name="Fluke12x_SearchNodes.vi" Type="VI" URL="../Drivers/dpi4 map Fluke12x/Fluke12x_SearchNodes.vi"/>
			<Item Name="Fluke12x_ChangeSetupNodes.vi" Type="VI" URL="../Drivers/dpi4 map Fluke12x/Fluke12x_ChangeSetupNodes.vi"/>
			<Item Name="Program Setup.vi" Type="VI" URL="../Drivers/dpi4 map Fluke12x/LowLevel/Program Setup.vi"/>
			<Item Name="Fluke12x_GetMeas.vi" Type="VI" URL="../Drivers/dpi4 map Fluke12x/LowLevel/Fluke12x_GetMeas.vi"/>
			<Item Name="Query Waveform.vi" Type="VI" URL="../Drivers/dpi4 map Fluke12x/LowLevel/Query Waveform.vi"/>
			<Item Name="Get Trace Admin.vi" Type="VI" URL="../Drivers/dpi4 map Fluke12x/LowLevel/Get Trace Admin.vi"/>
			<Item Name="Get Trace.vi" Type="VI" URL="../Drivers/dpi4 map Fluke12x/LowLevel/Get Trace.vi"/>
			<Item Name="Data Conversion.vi" Type="VI" URL="../Drivers/dpi4 map Fluke12x/LowLevel/Data Conversion.vi"/>
			<Item Name="Scale Waveform.vi" Type="VI" URL="../Drivers/dpi4 map Fluke12x/LowLevel/Scale Waveform.vi"/>
			<Item Name="lvanlys.dll" Type="Document" URL="/&lt;resource&gt;/lvanlys.dll"/>
			<Item Name="Fluke12x_format_output_value.vi" Type="VI" URL="../Drivers/dpi4 map Fluke12x/Fluke12x_format_output_value.vi"/>
			<Item Name="Fluke12x_Close.vi" Type="VI" URL="../Drivers/dpi4 map Fluke12x/Fluke12x_Close.vi"/>
			<Item Name="Close.vi" Type="VI" URL="../Drivers/dpi4 map Fluke12x/LowLevel/Close.vi"/>
		</Item>
		<Item Name="Build Specifications" Type="Build">
			<Item Name="TestDriver" Type="DLL">
				<Property Name="App_copyErrors" Type="Bool">true</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{EC12B477-F92F-4453-B6A7-5D6021C2D872}</Property>
				<Property Name="App_INI_GUID" Type="Str">{97F22AEE-83FA-455D-9CF3-F0C7E1F68590}</Property>
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{C4B39F6A-923C-4B14-80E5-C6E47BBA7A37}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">TestDriver</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">../DriverCompiled_DLL</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToProject</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{EE5DB2C5-E412-491D-8DAE-C35EDD675A0F}</Property>
				<Property Name="Bld_version.build" Type="Int">16</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">TestDriver.dll</Property>
				<Property Name="Destination[0].path" Type="Path">../DriverCompiled_DLL/TestDriver.dll</Property>
				<Property Name="Destination[0].path.type" Type="Str">relativeToProject</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">../DriverCompiled_DLL/data</Property>
				<Property Name="Destination[1].path.type" Type="Str">relativeToProject</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Dll_delayOSMsg" Type="Bool">true</Property>
				<Property Name="Dll_headerGUID" Type="Str">{4813B638-56DB-4B15-9AE9-B3C0C627D816}</Property>
				<Property Name="Dll_libGUID" Type="Str">{A3083D64-7152-4DF6-A49C-7859C661D7B9}</Property>
				<Property Name="Source[0].itemID" Type="Str">{E6AFF473-8AC5-4436-AD62-6934B713F366}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/Drivers/RF/MT8815.vi</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="Source[2].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[2].ExportedVI.VIProtoInfo[0]VIProtoDir" Type="Int">1</Property>
				<Property Name="Source[2].ExportedVI.VIProtoInfo[0]VIProtoInputIdx" Type="Int">-1</Property>
				<Property Name="Source[2].ExportedVI.VIProtoInfo[0]VIProtoLenInput" Type="Int">-1</Property>
				<Property Name="Source[2].ExportedVI.VIProtoInfo[0]VIProtoLenOutput" Type="Int">-1</Property>
				<Property Name="Source[2].ExportedVI.VIProtoInfo[0]VIProtoName" Type="Str">return value</Property>
				<Property Name="Source[2].ExportedVI.VIProtoInfo[0]VIProtoOutputIdx" Type="Int">-1</Property>
				<Property Name="Source[2].ExportedVI.VIProtoInfo[0]VIProtoPassBy" Type="Int">0</Property>
				<Property Name="Source[2].ExportedVI.VIProtoInfo[1]VIProtoDir" Type="Int">0</Property>
				<Property Name="Source[2].ExportedVI.VIProtoInfo[1]VIProtoInputIdx" Type="Int">7</Property>
				<Property Name="Source[2].ExportedVI.VIProtoInfo[1]VIProtoLenInput" Type="Int">-1</Property>
				<Property Name="Source[2].ExportedVI.VIProtoInfo[1]VIProtoLenOutput" Type="Int">-1</Property>
				<Property Name="Source[2].ExportedVI.VIProtoInfo[1]VIProtoName" Type="Str">Mode</Property>
				<Property Name="Source[2].ExportedVI.VIProtoInfo[1]VIProtoOutputIdx" Type="Int">-1</Property>
				<Property Name="Source[2].ExportedVI.VIProtoInfo[1]VIProtoPassBy" Type="Int">1</Property>
				<Property Name="Source[2].ExportedVI.VIProtoInfo[2]VIProtoDir" Type="Int">0</Property>
				<Property Name="Source[2].ExportedVI.VIProtoInfo[2]VIProtoInputIdx" Type="Int">6</Property>
				<Property Name="Source[2].ExportedVI.VIProtoInfo[2]VIProtoLenInput" Type="Int">-1</Property>
				<Property Name="Source[2].ExportedVI.VIProtoInfo[2]VIProtoLenOutput" Type="Int">-1</Property>
				<Property Name="Source[2].ExportedVI.VIProtoInfo[2]VIProtoName" Type="Str">DrvDataIn</Property>
				<Property Name="Source[2].ExportedVI.VIProtoInfo[2]VIProtoOutputIdx" Type="Int">-1</Property>
				<Property Name="Source[2].ExportedVI.VIProtoInfo[2]VIProtoPassBy" Type="Int">1</Property>
				<Property Name="Source[2].ExportedVI.VIProtoInfo[3]VIProtoDir" Type="Int">0</Property>
				<Property Name="Source[2].ExportedVI.VIProtoInfo[3]VIProtoInputIdx" Type="Int">5</Property>
				<Property Name="Source[2].ExportedVI.VIProtoInfo[3]VIProtoLenInput" Type="Int">-1</Property>
				<Property Name="Source[2].ExportedVI.VIProtoInfo[3]VIProtoLenOutput" Type="Int">-1</Property>
				<Property Name="Source[2].ExportedVI.VIProtoInfo[3]VIProtoName" Type="Str">Command</Property>
				<Property Name="Source[2].ExportedVI.VIProtoInfo[3]VIProtoOutputIdx" Type="Int">-1</Property>
				<Property Name="Source[2].ExportedVI.VIProtoInfo[3]VIProtoPassBy" Type="Int">1</Property>
				<Property Name="Source[2].ExportedVI.VIProtoInfo[4]VIProtoDir" Type="Int">0</Property>
				<Property Name="Source[2].ExportedVI.VIProtoInfo[4]VIProtoInputIdx" Type="Int">4</Property>
				<Property Name="Source[2].ExportedVI.VIProtoInfo[4]VIProtoLenInput" Type="Int">-1</Property>
				<Property Name="Source[2].ExportedVI.VIProtoInfo[4]VIProtoLenOutput" Type="Int">-1</Property>
				<Property Name="Source[2].ExportedVI.VIProtoInfo[4]VIProtoName" Type="Str">errorIn</Property>
				<Property Name="Source[2].ExportedVI.VIProtoInfo[4]VIProtoOutputIdx" Type="Int">-1</Property>
				<Property Name="Source[2].ExportedVI.VIProtoInfo[4]VIProtoPassBy" Type="Int">1</Property>
				<Property Name="Source[2].ExportedVI.VIProtoInfo[5]VIProtoDir" Type="Int">1</Property>
				<Property Name="Source[2].ExportedVI.VIProtoInfo[5]VIProtoInputIdx" Type="Int">-1</Property>
				<Property Name="Source[2].ExportedVI.VIProtoInfo[5]VIProtoLenInput" Type="Int">8</Property>
				<Property Name="Source[2].ExportedVI.VIProtoInfo[5]VIProtoLenOutput" Type="Int">-1</Property>
				<Property Name="Source[2].ExportedVI.VIProtoInfo[5]VIProtoName" Type="Str">Response</Property>
				<Property Name="Source[2].ExportedVI.VIProtoInfo[5]VIProtoOutputIdx" Type="Int">3</Property>
				<Property Name="Source[2].ExportedVI.VIProtoInfo[5]VIProtoPassBy" Type="Int">1</Property>
				<Property Name="Source[2].ExportedVI.VIProtoInfo[6]VIProtoDir" Type="Int">1</Property>
				<Property Name="Source[2].ExportedVI.VIProtoInfo[6]VIProtoInputIdx" Type="Int">-1</Property>
				<Property Name="Source[2].ExportedVI.VIProtoInfo[6]VIProtoLenInput" Type="Int">-1</Property>
				<Property Name="Source[2].ExportedVI.VIProtoInfo[6]VIProtoLenOutput" Type="Int">-1</Property>
				<Property Name="Source[2].ExportedVI.VIProtoInfo[6]VIProtoName" Type="Str">DrvDataOut</Property>
				<Property Name="Source[2].ExportedVI.VIProtoInfo[6]VIProtoOutputIdx" Type="Int">2</Property>
				<Property Name="Source[2].ExportedVI.VIProtoInfo[6]VIProtoPassBy" Type="Int">1</Property>
				<Property Name="Source[2].ExportedVI.VIProtoInfo[7]VIProtoDir" Type="Int">1</Property>
				<Property Name="Source[2].ExportedVI.VIProtoInfo[7]VIProtoInputIdx" Type="Int">-1</Property>
				<Property Name="Source[2].ExportedVI.VIProtoInfo[7]VIProtoLenInput" Type="Int">-1</Property>
				<Property Name="Source[2].ExportedVI.VIProtoInfo[7]VIProtoLenOutput" Type="Int">-1</Property>
				<Property Name="Source[2].ExportedVI.VIProtoInfo[7]VIProtoName" Type="Str">errorOut</Property>
				<Property Name="Source[2].ExportedVI.VIProtoInfo[7]VIProtoOutputIdx" Type="Int">0</Property>
				<Property Name="Source[2].ExportedVI.VIProtoInfo[7]VIProtoPassBy" Type="Int">1</Property>
				<Property Name="Source[2].ExportedVI.VIProtoInfo[8]CallingConv" Type="Int">1</Property>
				<Property Name="Source[2].ExportedVI.VIProtoInfo[8]Name" Type="Str">driver</Property>
				<Property Name="Source[2].ExportedVI.VIProtoInfo[8]VIProtoDir" Type="Int">3</Property>
				<Property Name="Source[2].ExportedVI.VIProtoInfo[8]VIProtoInputIdx" Type="Int">-1</Property>
				<Property Name="Source[2].ExportedVI.VIProtoInfo[8]VIProtoLenInput" Type="Int">-1</Property>
				<Property Name="Source[2].ExportedVI.VIProtoInfo[8]VIProtoLenOutput" Type="Int">-1</Property>
				<Property Name="Source[2].ExportedVI.VIProtoInfo[8]VIProtoName" Type="Str">len</Property>
				<Property Name="Source[2].ExportedVI.VIProtoInfo[8]VIProtoOutputIdx" Type="Int">-1</Property>
				<Property Name="Source[2].ExportedVI.VIProtoInfo[8]VIProtoPassBy" Type="Int">1</Property>
				<Property Name="Source[2].ExportedVI.VIProtoInfoCPTM" Type="Bin">%A#!"!!!!!Q!$%!B"H.U982V=Q!!#U!$!!2D&lt;W2F!!!11$$`````"H.P&gt;8*D:1!!&amp;E"1!!-!!!!"!!)*:8*S&lt;X)A&lt;X6U!!1!!!!11&amp;-+2(*W2'&amp;U95^V&gt;!!!%E!Q`````QB3:8.Q&lt;WZT:1!!&amp;E"1!!-!!!!"!!)):8*S&lt;X)A;7Y!!""!-0````](1W^N&lt;7&amp;O:!!/1&amp;-*2(*W2'&amp;U95FO!%1!]1!!!!!!!!!"$E2S;8:F=EVP:'5O9X2M!#V!&amp;A!'!W&gt;F&gt;!.T:81&amp;9WRP=W5%37ZJ&gt;!6$;'6D;Q**2!!%47^E:1!!0!$Q!!A!!Q!%!!5!"A!(!!A!#1!+!Q!!9!!!#1!!!!!!!!!*!!!!#1!!!!I!!!%+!!!!#A!!!!A!!!!!!1!,</Property>
				<Property Name="Source[2].ExportedVI.VIProtoInfoVIProtoItemCount" Type="Int">9</Property>
				<Property Name="Source[2].itemID" Type="Ref">/My Computer/Drivers/TestDriver.vi</Property>
				<Property Name="Source[2].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[2].type" Type="Str">ExportedVI</Property>
				<Property Name="SourceCount" Type="Int">3</Property>
				<Property Name="TgtF_companyName" Type="Str">dpi4.com</Property>
				<Property Name="TgtF_fileDescription" Type="Str">dpi4 Test Driver</Property>
				<Property Name="TgtF_internalName" Type="Str">dpi4 Test Driver</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © dpi4.com</Property>
				<Property Name="TgtF_productName" Type="Str">dpi4 Test Driver</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{796D822A-D75F-4E41-A819-63C6855BCCA9}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">TestDriver.dll</Property>
			</Item>
		</Item>
	</Item>
</Project>
