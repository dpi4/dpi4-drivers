#include "extcode.h"
#include <ole2.h>
#pragma pack(push)
#pragma pack(1)

#ifdef __cplusplus
extern "C" {
#endif
typedef struct {
	LVBoolean status;
	int32_t code;
	LStrHandle source;
	} TD1;


/*!
 * driver
 */
void __cdecl driver(uint16_t Mode, VARIANT *DrvDataIn, char Command[], 
	TD1 *errorIn, char Response[], VARIANT *DrvDataOut, TD1 *errorOut, 
	int32_t len);

MgErr __cdecl LVDLLStatus(char *errStr, int errStrLen, void *module);

#ifdef __cplusplus
} // extern "C"
#endif

#pragma pack(pop)

